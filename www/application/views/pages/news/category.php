<section class="top-post-area">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 p-0">
        <div class="hero-nav-area">
          <h1 class="text-white">News Category</h1>
          <p class="text-white link-nav">
            <a href="/">Home </a>
            <span class="lnr lnr-arrow-right"></span>
            <a href="/categories">Category</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

  <section class="tech-news-area section-gap-top-60">
    <div class="container">
      <div class="row">
        <?php foreach ($categories as $category) {?>
          <div class="col-lg-3 col-md-5 col-sm-6">
                <div class="bottom mt-10">
                  <div >
                    <a style="height: 5em; width: 11em; text-align: center;" href="category/<?=$category['id']?>" class="primary-btn"><?=$category['name']?></a>
                  </div>
                </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>


  <section class="int-news-area section-gap-top" >
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="row">
            <div class="col-lg-12">
              <div class="section-title">
                <h2 class="heading">Latest News</h2>
              </div>
            </div>
          </div>

          <div class="row">
            <?php if (isset($news[0])) { ?>
              <div class="col-lg-5 col-md-8">
                <div class="single-post single-int mb-40">
                  <div class="thumb">
                    <a href="<?=URL::base() . 'new/'.$news[0]['id']?>">
                      <div class="relative">
                        <img class="f-img w-100 img-fluid mx-auto" src="<?=URL::base() . $news[0]['images']?>" alt="">
                      </div>
                    </a>
                  </div>
                  <div class="">
                    <div class="bottom pt-30 d-flex justify-content-between align-items-center flex-wrap">
                      <div>
                        <a href="category/<?=$news[0]['type']['id']?>" class="primary-btn"><?=$news[0]['type']['name']?></a>
                        <a href="#"><span><?=$news[0]['date']?></span></a>
                      </div>
                    </div>
                    <a href="<?=URL::base() . 'new/'.$news[0]['id']?>">
                      <h4><?=$news[0]['title']?></h4>
                    </a>
                    <p>
                      <?=$news[0]['anons']?>
                    </p>
                  </div>
                </div>
              </div>
            <?php } ?>
            <div class="col-lg-4 col-md-12">
              <div class="row">
                <?php if (isset($news[1])) { ?>
                  <div class="col-lg-12 col-md-6 col-sm-6">
                    <div class="single-post single-int mb-40">
                      <div class="thumb">
                        <a href="<?=URL::base() . 'new/'.$news[1]['id']?>">
                          <div class="relative">
                            <img class="f-img w-100 img-fluid mx-auto" src="<?=URL::base() . $news[1]['images']?>" alt="">
                          </div>
                        </a>
                      </div>
                      <div class="">
                        <div class="bottom mt-10">
                          <div>
                            <a href="category/<?=$news[0]['type']['id']?>" class="primary-btn"><?=$news[1]['type']['name']?></a>
                            <a href="#"><span><?=$news[1]['date']?></span></a>
                          </div>
                        </div>
                        <a href="<?=URL::base() . 'new/'.$news[1]['id']?>">
                          <h4 class="mt-15"><?=$news[1]['title']?></h4>
                        </a>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                <?php if (isset($news[2])) { ?>
                  <div class="col-lg-12 col-md-6 col-sm-6">
                    <div class="single-post single-int mb-40">
                      <div class="thumb">
                        <a href="<?=URL::base() . 'new/'.$news[2]['id']?>">
                          <div class="relative">
                            <img class="f-img w-100 img-fluid mx-auto" src="<?=URL::base() . $news[2]['images']?>" alt="">
                          </div>
                        </a>
                      </div>
                      <div class="">
                        <div class="bottom mt-10">
                          <div>
                            <a href="category/<?=$news[2]['type']['id']?>" class="primary-btn"><?=$news[2]['type']['name']?></a>
                            <a href="#"><span><?=$news[2]['date']?></span></a>
                          </div>
                        </div>
                        <a href="<?=URL::base() . 'new/'.$news[2]['id']?>">
                          <h4><?=$news[2]['title']?></h4>
                        </a>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>




