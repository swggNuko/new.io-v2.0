<!-- Start Main Area -->
<div class="category-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-14 col-md-12">
        <!-- Start Technology News Area -->
        <section class="">
          <div class="row">
            <?php foreach ($news as $new) { ?>
            <div class="col-lg-4 col-md-5 col-sm-6">
              <div class="single-post single-int mb-40">
                <a href="<?=URL::base() . 'new/'.$new['id']?>">
                  <div class="thumb">
                    <div class="relative">
                      <img class="f-img img-fluid mx-auto" src="<?=URL::base() .$new['images']?>" alt="">
                    </div>
                  </div>
                </a>
                <div class="">
                  <div class="bottom mt-10">
                    <div>
                      <a href="/category/<?=$new['type']['id']?>" class="primary-btn"><?=$new['type']['name']?></a>
                      <a href="#"><span><?=$new['date']?></span></a>
                    </div>
                  </div>
                  <a href="<?=URL::base() . 'new/'.$new['id']?>">
                    <h4><?=$new['title']?></h4>
                  </a>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
        </section>
        <!-- End Technology News Area -->
      </div>

    <?php if ($numbering['startP'] > 0) { ?>
      <div class="demo" style="text-align: center">
        <nav class="pagination pagination_type2 pagination_type3">
          <ol class="pagination__list">
            <li class="pagination__group"><a href="/<?=$name?>/<?=$numbering['predP']?>" id="page-prev" data-url="/<?=$name?>/<?=$numbering['predP']?>" class="pagination__item pagination__control pagination__control_prev">prev</a></li>

          <?php
          for($i = $numbering['startP']; $i <= $numbering['endP']; $i++) {
            $linkClass='';
            if($page == $i || ($page == '' && $i == 1)) $linkClass='pagination__item_active';
            ?>
            <li class="pagination__group"><a href="/<?=$name?>/<?=$i?>" class="pagination__item <?=$linkClass?>"><?=$i?></a></li>
            <?php
          }
          ?>
            <li class="pagination__group"><a href="/<?=$name?>/<?=$numbering['sledP']?>" id="page-next" data-url="/<?=$name?>/<?=$numbering['sledP']?>" class="pagination__item pagination__control pagination__control_next">next</a></li>
          </ol>
        </nav>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<!-- End Main Area -->