<!-- Start post-content Area -->
<section class="post-content-area single-post-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 posts-list">
        <div class="single-post-blog row">
          <div class="col-lg-12">
            <div class="feature-img">
              <img class="img-fluid" src="<?=URL::base() . $new['images']?>" alt="">
            </div>
          </div>
          <div class="col-lg-12">
            <h1 class="mt-20 mb-20"><?=$new['title']?></h1>

            <div class="row tags">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <div>
                  <a href="category/<?=$new['type']['id']?>" class="primary-btn mr-10"><?=$new['type']['name']?></a>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-right">
                <div class="d-flex justify-content-end">
                  <div class="mr-15">
                    <h5 class="text-white">Mark wiens</h5>
                    <p><?=$new['date']?></p>
                  </div>
                </div>
              </div>
            </div>

            <p class="excert">
              <?=$new['text']?>
            </p>
          </div>
        </div>
        <div class="navigation-area">
          <div class="row">
            <?php if (isset($new['prevNew'])) { ?>
            <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
              <div class="thumb">
                <a href="<?=URL::base() . 'new/'.$new['prevNew']['id']?>"><img class="img-fluid" src="<?=URL::base() . $new['prevNew']['imagesMini']?>" alt=""></a>
              </div>
              <div class="arrow">
                <a href="<?=URL::base() . 'new/'.$new['prevNew']['id']?>"><span class="lnr text-white lnr-arrow-left"></span></a>
              </div>
              <div class="detials">
                <p>Prev Post</p>
                <a href="<?=URL::base() . 'new/'.$new['prevNew']['id']?>">
                  <h4><?=$new['prevNew']['title']?></h4>
                </a>
              </div>
            </div>
            <?php } ?>
            <?php if (isset($new['nextNew'])) { ?>
            <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
              <div class="detials">
                <p>Next Post</p>
                <a href="<?=URL::base() . 'new/'.$new['nextNew']['id']?>">
                  <h4><?=$new['nextNew']['title']?></h4>
                </a>
              </div>
              <div class="arrow">
                <a href="<?=URL::base() . 'new/'.$new['nextNew']['id']?>"><span class="lnr text-white lnr-arrow-right"></span></a>
              </div>
              <div class="thumb">
                <a href="<?=URL::base() . 'new/'.$new['nextNew']['id']?>"><img class="img-fluid" src="<?=URL::base() . $new['nextNew']['imagesMini']?>" alt=""></a>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End post-content Area -->