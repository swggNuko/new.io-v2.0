<!-- Start Editors Pick Area -->
<section class="editors-area">
  <div class="container-fluid">
    <div class="row">
      <style>
        .example1 {
          height: 50px;
          overflow: hidden;
          position: relative;
        }
        .example1 span {
          font-size: 3em;
          color: #FF1857;
          position: absolute;
          font-family: "Amador GothicC";
          width: 100%;
          height: 100%;
          margin: 0;
          line-height: 50px;
          text-align: center;
          /* Starting position */
          -moz-transform:translateX(100%);
          -webkit-transform:translateX(100%);
          transform:translateX(100%);
          /* Apply animation to this element */
          -moz-animation: example1 15s linear infinite;
          -webkit-animation: example1 15s linear infinite;
          animation: example1 15s linear infinite;
        }
        /* Move it (define the animation) */
        @-moz-keyframes example1 {
          0%   { -moz-transform: translateX(90%); }
          100% { -moz-transform: translateX(-90%); }
        }
        @-webkit-keyframes example1 {
          0%   { -webkit-transform: translateX(90%); }
          100% { -webkit-transform: translateX(-90%); }
        }
        @keyframes example1 {
          0%   {
            -moz-transform: translateX(90%); /* Firefox bug fix */
            -webkit-transform: translateX(90%); /* Firefox bug fix */
            transform: translateX(90%);
          }
          100% {
            -moz-transform: translateX(-90%); /* Firefox bug fix */
            -webkit-transform: translateX(-90%); /* Firefox bug fix */
            transform: translateX(-90%);
          }
        }
      </style>
      <div class="col-lg-12">
        <div class="example1">
          <span>Attention! This site is currently in a development state </span>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- End Editors Pick Area -->

  
  <!-- Start Technology News Area -->
<?php if (!empty($technews)) {?>
	<section class="tech-news-area section-gap-top-60">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h2 class="heading">Technology News</h2>
					</div>
				</div>
			</div>

			<div class="row">
        <?php foreach ($technews as $new) {?>
				<div class="col-lg-3 col-md-5 col-sm-6">
					<div class="single-post single-int mb-40">
						<div class="thumb">
              <a href="<?=URL::base() . 'new/'.$new['id']?>">
                <div class="relative">
                  <img class="f-img img-fluid mx-auto" src="<?=URL::base() . $new['images']?>" alt="">
                </div>
              </a>
						</div>
						<div class="">
							<div class="bottom mt-10">
								<div>
									<a href="/category/<?=$new['type']['id']?>" class="primary-btn"><?=$new['type']['name']?></a>
									<a href="#"><span><?=$new['date']?></span></a>
								</div>
							</div>
							<a href="<?=URL::base() . 'new/'.$new['id']?>">
								<h4><?=$new['title']?></h4>
							</a>
						</div>
					</div>
				</div>
        <?php } ?>
			</div>
		</div>
	</section>
  <!-- End Technology News Area -->
<?php } ?>


<!-- Start Latest News Area -->
<section class="int-news-area section-gap-top" >
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="row">
          <div class="col-lg-12">
            <div class="section-title">
              <h2 class="heading">Latest News</h2>
            </div>
          </div>
        </div>

        <div class="row">
          <?php if (isset($news[0])) { ?>
            <div class="col-lg-7 col-md-8">
              <div class="single-post single-int mb-40">
                <div class="thumb">
                  <a href="<?=URL::base() . 'new/'.$news[0]['id']?>">
                    <div class="relative">
                      <img class="f-img w-100 img-fluid mx-auto" src="<?=URL::base() . $news[0]['images']?>" alt="">
                    </div>
                  </a>
                </div>
                <div class="">
                  <div class="bottom pt-30 d-flex justify-content-between align-items-center flex-wrap">
                    <div>
                      <a href="/category/<?=$news[0]['type']['id']?>" class="primary-btn"><?=$news[0]['type']['name']?></a>
                      <a href="#"><span><?=$news[0]['date']?></span></a>
                    </div>
                  </div>
                  <a href="<?=URL::base() . 'new/'.$news[0]['id']?>">
                    <h4><?=$news[0]['title']?></h4>
                  </a>
                  <p>
                    <?=$news[0]['anons']?>
                  </p>
                </div>
              </div>
            </div>
          <?php } ?>
          <div class="col-lg-5 col-md-12">
            <div class="row">
              <?php if (isset($news[1])) { ?>
                <div class="col-lg-12 col-md-6 col-sm-6">
                  <div class="single-post single-int mb-40">
                    <div class="thumb">
                      <a href="<?=URL::base() . 'new/'.$news[1]['id']?>">
                        <div class="relative">
                          <img class="f-img w-100 img-fluid mx-auto" src="<?=URL::base() . $news[1]['images']?>" alt="">
                        </div>
                      </a>
                    </div>
                    <div class="">
                      <div class="bottom mt-10">
                        <div>
                          <a href="/category/<?=$news[1]['type']['id']?>" class="primary-btn"><?=$news[1]['type']['name']?></a>
                          <a href="#"><span><?=$news[1]['date']?></span></a>
                        </div>
                      </div>
                      <a href="<?=URL::base() . 'new/'.$news[1]['id']?>">
                        <h4 class="mt-15"><?=$news[1]['title']?></h4>
                      </a>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <?php if (isset($news[2])) { ?>
                <div class="col-lg-12 col-md-6 col-sm-6">
                  <div class="single-post single-int mb-40">
                    <div class="thumb">
                      <a href="<?=URL::base() . 'new/'.$news[2]['id']?>">
                        <div class="relative">
                          <img class="f-img w-100 img-fluid mx-auto" src="<?=URL::base() . $news[2]['images']?>" alt="">
                        </div>
                      </a>
                    </div>
                    <div class="">
                      <div class="bottom mt-10">
                        <div>
                          <a href="/category/<?=$news[1]['type']['id']?>" class="primary-btn"><?=$news[2]['type']['name']?></a>
                          <a href="#"><span><?=$news[2]['date']?></span></a>
                        </div>
                      </div>
                      <a href="<?=URL::base() . 'new/'.$news[2]['id']?>">
                        <h4><?=$news[2]['title']?></h4>
                      </a>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<!-- End International News Area -->

  
  <!-- Start Popular News feed Area -->
	<section class="popular-news-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h2 class="heading">Popular News Feed</h2>
					</div>
				</div>
			</div>

			<div class="row">

			</div>
		</div>
	</section>
	<!-- End Popular News feed Area -->