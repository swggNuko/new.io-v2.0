<div class="about" data-id="<?=$page['id']?>">
  <h1 <?=($editable ? 'contenteditable="true"' : '')?>><?=$page['title']?></h1>
  <div class="st">
    <div class="article <?=(!empty($page['content_right']) ? "medium-shortcode-area" : "")?>" <?=(empty($page['content_right']) ? "style=\"width: 100%\"" : "")?> <?=($editable ? 'data-block="center" contenteditable="true"' : '')?>>
      <?=$page['content']?>
    </div>
    <?php
      if (!empty($page['content_right'])) {
    ?>
    <div class="news" <?=($editable ? 'data-block="right" contenteditable="true"' : '')?>>
      <?=$page['content_right']?>
    </div>
    <?php
      }
    ?>
    <?php
      if (!empty($page['content_footer'])) {
    ?>
    <div class="article" <?=($editable ? 'data-block="bottom" contenteditable="true"' : '')?> style="width: 100%">
      <?=$page['content_footer']?>
    </div>
    <?php
      }
    ?>
  </div>
</div>