<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?=$title?></title>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
<meta charset="utf-8" http-equiv="description" name="description" content="<?=$description?>" />

<?php foreach($styles as $style): ?>
	<link href="<?php echo URL::base(); ?>public/css/<?php echo $style; ?>.css" rel="stylesheet" />
<?php endforeach; ?>
<?php if (isset($map_lib) && $map_lib): ?>
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<?php endif; ?>

</head>

<body>
<?php
  if (isset($show_media)) {
    echo $show_media;
  }
?>

<!-- Start Banner Area -->
<section class="banner-area">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-md-12">
        <div class="row">
          <div class="col-lg-12 col-md-4 col-sm-6">
            <div class="single-post mb-03">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Banner Area -->

<!-- Start Header Area -->
<header>
  <div class="main-menu" id="main-menu">
    <div class="container">
      <div class="row align-items-center justify-content-between">
        <div>
          <a class="navbar-brand" href="/">
            <img src="<?php echo URL::base(); ?>public/images/logo.png" alt="">
          </a>
        </div>
        <nav id="nav-menu-container" class="ml-auto">
          <ul class="nav-menu">
            <li <?= $_SERVER['REQUEST_URI'] == '/' ? 'class="menu-active"' : ''?>><a href="/">Home</a></li>
            <li <?= $_SERVER['REQUEST_URI'] == '/archive' ? 'class="menu-active"' : ''?>><a href="/archive">Archive</a></li>
            <li <?= $_SERVER['REQUEST_URI'] == '/categories' ? 'class="menu-active"' : ''?>><a href="/categories">Category</a></li>
            <?php if ($admin) { ?><li><a href="/admin">Panel</a></li><?php } ?>
          </ul>
        </nav>
        <div class="navbar-right ml-auto">
        </div>
      </div>
    </div>
  </div>
</header>
<!-- End Header Area -->
  
  
  <?php echo $content; ?>
  
  <footer class="footer-area section-gap-top">
		<div class="container">
			<div class="row pb-10">
				<div class="col-lg-4 col-md-6">
					<div class="single-footer-widget">
						<div class="mb-40">
							<img src="img/logo.png" alt="">
						</div>
						<p>
							Technology and gadgets Adapter (MPA) is our favorite iPhone solution, since it lets you use the headphones
							you’re most comfortable with. It has an iPhone-compatible jack at one end and a microphone module with an
							Answer/End/Pause button and a female 3.5mm audio jack for connectingheadphones
						</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-footer-widget">
						<h6 class="heading">Quick Links</h6>
						<div class="row">
							<ul class="col footer-nav">
								<li><a href="#">Sitemaps</a></li>
								<li><a href="#">Categories</a></li>
								<li><a href="#">Archives</a></li>
								<li><a href="#">Advertise</a></li>
								<li><a href="#">Ad Choice</a></li>
							</ul>
							<ul class="col footer-nav">
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Help Center</a></li>
								<li><a href="#">Newsletters</a></li>
								<li><a href="#">Feedback</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright-text">
			<div class="container">
				<div class="row footer-bottom d-flex justify-content-between">
					<p class="col-lg-8 col-sm-6 footer-text m-0 text-white"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
					<div class="col-lg-4 col-sm-6 footer-social">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-youtube-play"></i></a>
						<a href="#"><i class="fa fa-pinterest"></i></a>
						<a href="#"><i class="fa fa-rss"></i></a>
					</div>
				</div>
			</div>
		</div>
	</footer>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	 crossorigin="anonymous"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>

<?php foreach ($libs as $lib): ?>
  <script src="<?php echo URL::base(); ?>public/js/libs/<?php echo $lib; ?>.js"></script>
<?php endforeach; ?>
<?php foreach($scripts as $script): ?>
  <script src="<?php echo URL::base(); ?>public/js/<?php echo $script; ?>.js"></script>
<?php endforeach; ?>
</body>
</html>