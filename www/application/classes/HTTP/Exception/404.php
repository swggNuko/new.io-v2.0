<?php defined('SYSPATH') OR die('No direct script access.');

class HTTP_Exception_404 extends Kohana_HTTP_Exception_404
{
  public function get_response() {
    // Lets log the Exception, Just in case it's important!
    Kohana_Exception::log($this);

    if (Kohana::$environment >= Kohana::DEVELOPMENT) {
      // Show the normal Kohana error page.
      return parent::get_response();
    } else {
      $template = Controller_Common::CreateTemplate();
      $view = View::factory('errors/404')
        ->bind('url', $url);
      $template->content = $view;
			$template->timetableClasses = 'timetable-white';
      
      $exceptionModel = new Model_Exception_404();
      
      $url = $_SERVER['SERVER_NAME'].$_SERVER['PATH_INFO'];
      
      $response = Response::factory()
        ->status(404)
        ->body($template->render());
      
      return $response;
    }
  }
  

}
