<?php defined('SYSPATH') or die('No direct script access.');

class Model_Api_Pages extends Model_Api_Editable
{
  protected $table_ = 'pages';
  protected $access_ = 'pages';
  protected $edit_return_ = true;
  protected $fields_ = array(
    'header' => 'title',
    'center' => 'content',
    'right' => 'content_right',
    'bottom' => 'content_footer'
    );
  
  public function handler($type, $data)
  {
    if (empty($type) || empty($data)) return false;
    
    switch($type)
    {
      case 'edit':
        $result = $this->edit($data);
        break;
      default:
        $result = false;
        break;
    }
    
    return $result;
  }
  
  protected function edit_return($id, $field, $content)
  {
    if ($field == 'title') {
      return $content;
    }
    
    $ShortcodeHandler = new Model_Shortcodes_Handler();
    $position = $field == 'content_right' ? 'vertical' : 'horizontal';
    View::set_global('editable', true);
    
    return $ShortcodeHandler->handlerShortcodes(
      $content,
      $position,
      $this->table_,
      $id
    );
  }
}
?>