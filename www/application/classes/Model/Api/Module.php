<?php defined('SYSPATH') or die('No direct script access.');

abstract class Model_Api_Module extends Kohana_Model
{
  public abstract function handler($type, $data);
}
?>