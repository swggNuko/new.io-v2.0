<?php defined('SYSPATH') or die('No direct script access.');

class Model_Api_News extends Model_Api_Editable
{
  protected $table_ = 'news';
  protected $access_ = 'news';
  protected $edit_return_ = true;
  protected $fields_ = array(
    'header' => 'title',
    'text' => 'text',
    'anons' => 'anons'
    );
  
  public function handler($type, $data)
  {
    if (empty($type) || empty($data)) return false;
    
    switch($type)
    {
      case 'edit':
        $result = $this->edit($data);
        break;
      default:
        $result = false;
        break;
    }
    
    return $result;
  }
  
  protected function edit_return($id, $field, $content)
  {
    if ($field != 'text') {
      return $content;
    }

    $ShortcodeHandler = new Model_Shortcodes_Handler();
    $position = 'horizontal';
    View::set_global('editable', true);
    
    return $ShortcodeHandler->handlerShortcodes(
      $content,
      $position,
      $this->table_,
      $id
    );
  }
}
?>