<?php defined('SYSPATH') or die('No direct script access.');

class Model_Exception_404 extends Kohana_Model
{
  public function getRandomExhibit() {
    $results = DB::select('id', 'name')
      ->from('exhibits')
      ->execute()
      ->as_array();

    return $results[array_rand($results)];
  }
}

?>