<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Page extends Kohana_Model
{
  protected $table_ = 'pages';
  
  public function getPage($url)
  {
    $page = DB::select()
            ->from($this->table_)
            ->where('url', '=', $url)
            ->execute()
            ->as_array();
						
    if(empty($page)) {
        return false;
    }	
			
    return $this->changeParams($page[0]);
  }
  
  private function changeParams($page)
  {
    $ShortcodesHandler = new Model_Shortcodes_Handler();
    if (!empty($page['content'])) {
      $page['content'] = $ShortcodesHandler->handlerShortcodes(
        stripslashes($page['content']),
        'horizontal',
        $this->table_,
        $page['id']
      );
    }
    if (!empty($page['content_right'])) {
      $page['content_right'] = $ShortcodesHandler->handlerShortcodes(
        stripslashes($page['content_right']),
        'vertical',
        $this->table_,
        $page['id']
      );
    }
    if (!empty($page['content_footer'])) {
      $page['content_footer'] = $ShortcodesHandler->handlerShortcodes(
        stripslashes($page['content_footer']),
        'horizontal', 
        $this->table_,
        $page['id']
      );
    }
    return $page;
  }
}

?>