<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_News extends Model_Common
{
  protected $name = 'news';

  public function getNew($id) 
  {
    $new = DB::select()
          ->from($this->options['table'])
          ->where('id', '=', $id)
          ->execute()
          ->as_array();

    if(empty($new)) {
      return false;
    }

    $nextNew = DB::select()
      ->from($this->options['table'])
      ->where('id', '=', $id+1)
      ->execute()
      ->as_array();

    $prevNew = DB::select()
      ->from($this->options['table'])
      ->where('id', '=', $id-1)
      ->execute()
      ->as_array();


    if (!empty($nextNew)) {
      $new[0]['nextNew'] = $this->changeParams($nextNew)[0];
    }

    if (!empty($prevNew)) {
      $new[0]['prevNew'] = $this->changeParams($prevNew)[0];
    }

    $new = $this->changeParams($new);

    return $new[0];  
  }

  public function getLast3News()
  {
    $news = DB::select()
          ->from($this->options['table'])
          ->where('hidden', '=', '0')
          ->order_by('date','DESC')
          ->limit(3)
          ->offset(0)
          ->execute()
          ->as_array();

    if(empty($news)) {
      return false;
    }

    return $this->changeParams($news);
  }

  public function getLastTechNews($q)
  {
    $news = DB::select()
      ->from($this->options['table'])
      ->where('hidden', '=', '0')
      ->and_where('type_v', '=', 2)
      ->order_by('date','DESC')
      ->limit($q)
      ->offset(0)
      ->execute()
      ->as_array();

    if(empty($news)) {
      return array();
    }

    return $this->changeParams($news);
  }

  public function getCategories()
  {
    $categories = DB::select()
      ->from('type_v')
      ->where('hidden', '=', '0')
      ->order_by('name','DESC')
      ->execute()
      ->as_array();

    if(empty($categories)) {
      return false;
    }

    return $categories;
  }

  public function getNews($page) 
  {
    $news = DB::select()
          ->from($this->options['table'])
          ->where('hidden', '=', '0')
          ->order_by('date','DESC')
          ->limit(9)
          ->offset(($page - 1) * $this->options['inc'])
          ->execute()
          ->as_array();

    if(empty($news)) {
      return false;
    }

    return $this->changeParams($news);  
  }

  public function getNewsCategory($page, $category)
  {
    $news = DB::select()
      ->from($this->options['table'])
      ->where('hidden', '=', '0')
      ->and_where('type_v', '=', $category)
      ->order_by('date','DESC')
      ->limit(9)
      ->offset(($page - 1) * $this->options['inc'])
      ->execute()
      ->as_array();

    if(empty($news)) {
      return false;
    }

    return $this->changeParams($news);
  }

  public function getPrev($id) 
  {
    $prevId = DB::query(Database::SELECT, 'SELECT MIN(`id`) FROM `' 
      . $this->options['table'] .'` WHERE `id` > ' 
      . $id . ' AND `hidden` = "' . 0 .'"')
              ->execute()
              ->as_array();

    $prevId = $prevId[0]['MIN(`id`)'];

    return (!empty($prevId) ? $prevId : $id);
  }

  public function getNext($id) 
  {
    $nextId = DB::query(Database::SELECT, 'SELECT MAX(`id`) FROM `' 
      . $this->options['table'] .'` WHERE `id` < ' 
      . $id . ' AND `hidden` = "' . 0 .'"')
              ->execute()
              ->as_array();

    $nextId = $nextId[0]['MAX(`id`)'];

    return (!empty($nextId) ? $nextId : $id);
  }

  public function changeParams($news) 
  {
    $Files = new Model_Files();  
    $DateHandler = new Model_Service_DateHandler();
    $TypeHandler = new Model_Service_TypeHandler();

    for($i = 0; $i < count($news); $i++) {

      if(!empty($news[$i]['images'])) {
        $news[$i]['imagesMini'] = $Files->getImage($news[$i]['images'], 'F350-350');
        $news[$i]['images'] = $Files->getImage($news[$i]['images'], 'R975-975');

      } else {
        $news[$i]['imagesMini'] = 'public/images/default.jpg';
        $news[$i]['images'] = 'public/images/default.jpg';
      }

      $news[$i]['type'] = $TypeHandler->getNewType($news[$i]['type_v']);

      $news[$i]['date'] = $DateHandler->getNormalDateNews($news[$i]['date']);

    }

    return $news;
  }
}

?>