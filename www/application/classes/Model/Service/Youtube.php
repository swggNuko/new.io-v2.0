<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_Youtube extends Kohana_Model
{
  private $apikey = "AIzaSyD5sBRh67Oljktyz7sS1gDxIqmiZrGZwVs";
  private $thumbnailPriority = array(
    'standard',
    'high',
    'medium',
    'default'
  );
  
  public function getVideoIdByUrl($url)
  {
    $query = parse_url($url, PHP_URL_QUERY);
    
    if (empty($query)) {
      return false;
    }
    
    parse_str($query, $query);
    return $query['v'];
  }
  
  public function getVideoInfo($id)
  {
    if (empty($id) || !is_numeric($id)) {
      return false;
    }
    
    $curl = curl_init("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=$id&key=$this->apikey");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
    curl_close($curl);
    
    if ($data = json_decode($result, true))
    {
      if (!isset($data['items'])) {
        return 0;
      }
      $item = $data['items'][0];
      return array(
        'id' => $item['id'],
        'title' => $item['snippet']['title'],
        'description' => $item['snippet']['description'],
        'thumbnail' => $this->getVideoThumbnail($item['snippet']['thumbnails'])
      );
      
    }
    return false;
  }
  
  public function getVideoThumbnail($item)
  {
    foreach ($this->thumbnailPriority as $type)
    {
      if (isset($item[$type]))
      {
        return $item[$type]['url'];
      }
    }
    return 0;
  }
}
?>