﻿<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_DateHandler extends Kohana_Model
{		
		
		public function getNormalDateDMY($unix, $with_year = true) 
		{
			$date = getdate($unix);
			
			switch($date['mon']) {
				case 1: 
					$date['mon'] = 'января';
					break;
				case 2: 
					$date['mon'] = 'февраля';
					break;
				case 3: 
					$date['mon'] = 'марта';
					break;
				case 4: 
					$date['mon'] = 'апреля';
					break;
				case 5: 
					$date['mon'] = 'мая';
					break;
				case 6: 
					$date['mon'] = 'июня';
					break;
				case 7: 
					$date['mon'] = 'июля';
					break;
				case 8: 
					$date['mon'] = 'августа';
					break;
				case 9: 
					$date['mon'] = 'сентября';
					break;
				case 10: 
					$date['mon'] = 'октября';
					break;
				case 11: 
					$date['mon'] = 'ноября';
					break;
				case 12: 
					$date['mon'] = 'декабря';
					break;
				default:
					$date['mon'] = 'undefined';
					break;
			}
			
			return $date['mday'] . ' ' . $date['mon'] . ' ' . ($with_year ? $date['year'] : '');
		}
		
		public function getNormalDateNews($unix) 
		{
			$date = getdate($unix);
			
			return $date['month'] . ' ' . $date['mday'] . ', ' . $date['year'];
			
		}
  
  public function getOnlyTime($unix)
  {
    return date("H:i", $unix);
  }
}

?>