<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_Auth extends Kohana_Model
{
  private $cookie_ = "access";
  private $post_ = "hash";
  private $postTable_ = "table";
  private $loggedIn_ = "logged_in";
  private $canEdit_ = "can_edit";
  
  public function checkAdminAccess()
  {
    if (!isset($_COOKIE[$this->cookie_])) {
      return false;
    }
    $post = array(
      $this->post_ => $_COOKIE[$this->cookie_]
    );
    $curl = curl_init($_SERVER["SERVER_NAME"].URL::base()."admin/api/isLoggedIn.php");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
    $result = curl_exec($curl);
    curl_close($curl);
    
    if (($result = json_decode($result, true)) && isset($result[$this->loggedIn_])) {
      return $result[$this->loggedIn_];
    }
    
    return false;
  }
  
  public function canUserEdit($table, $only_access = true)
  {
    if (!isset($_COOKIE[$this->cookie_]) || empty($table)) {
      return false;
    }
    $post = array(
      $this->post_ => $_COOKIE[$this->cookie_],
      $this->postTable_ => $table
    );
    $curl = curl_init($_SERVER["SERVER_NAME"].URL::base()."admin/api/canUserEdit.php");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
    $result = curl_exec($curl);
    curl_close($curl);

    if (($result = json_decode($result, true)) && isset($result[$this->canEdit_])) {
      return $only_access ? $result[$this->canEdit_] : $result;
    }
    
    return false;
  }
}
?>