<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_Email extends Model_Common
{
  protected $name = 'service.email';
  
  public function sendMail($to, $subject, $message)
  { 
    Email::send(null, $subject, $message, $this->options['from'], $to, 'text/html');
  }
}
?>