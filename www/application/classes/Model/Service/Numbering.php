<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_Numbering extends Kohana_Model
{	
  public $inc = 10; //Number records on page

  public function getPagesRange($page, $table, $params = array(), $params2 = array()) 
  {	
    if(empty($table)) {
      return false;
    }

    $records = DB::select()
      ->from($table);

    if(!empty($params)) {
      $records = $records
        ->where($params[0], $params[1], $params[2]);
    }

    if(!empty($params2)) {
      $records = $records
        ->and_where($params2[0], $params2[1], $params2[2]);
    }

    $records = $records
      ->execute()
      ->as_array();

    return $this->countRange($page, count($records), $this->inc);
  }

  public function countRange($page, $count, $limit) {
    $numPage = ceil($count/$limit);

    if($numPage <= 1) return false;

    if($numPage < 10) {
      $endP = $numPage;
    } else {
      $endP = ($page < 6) ? 10 : ((($page + 4) > $numPage) ? $numPage : ($page + 4));
    }

    $startP = ($page < 6) ? 1 : ((($page + 4) <= $numPage) ? ($page - 4) : ($endP - 8));

    $predP = ($page > 1) ? ($page-1) : 1;

    $sledP = ($page < $numPage - 1) ? ($page + 1) : $numPage;



    return array(
      'predP' => $predP,
      'startP' => $startP,
      'endP' => $endP,
      'sledP' => $sledP,
      'lastP' => $numPage
    );
  }
  
  public function countSearchResults($results)
  {
    $countResults = array('all' => 0);
    $config = Kohana::$config->load('search');
    
    foreach ($config as $table => $value)
    {
      $countResults[$table] = 0;
    }
    
    foreach ($results as $result)
    {
      $countResults['all']++;
      $countResults[$result['table']]++;
    }
    
    return $countResults;
  }
}

?>