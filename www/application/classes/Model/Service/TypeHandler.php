<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_TypeHandler extends Kohana_Model
{	
		
		public function getNewType($type)
		{
			$type = DB::select()
            ->from('type_v')
            ->where('id', '=', $type)
            ->execute()
            ->as_array();
			
			if(empty($type[0])) {
			  return array();
      }
			
			return $type[0];
		}
		
		
}

?>