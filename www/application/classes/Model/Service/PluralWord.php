<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_PluralWord extends Kohana_Model
{
  public function getPluralWord($num, $form1, $form2, $form3) {
    $num = abs($num) % 100;
    $num1 = $num % 10;
    if ($num > 10 && $num < 20) return $form3;
    if ($num1 > 1 && $num1 < 5) return $form2;
    if ($num1 == 1) return $form1;
    return $form3;
  }
}