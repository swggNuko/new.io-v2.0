<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Service_FundHandler extends Kohana_Model
{	
		
		public function getExhibitFund($fund) 
		{
			$fund = DB::select()
            ->from('funds')
            ->where('id', '=', $fund)
            ->execute()
            ->as_array();
			
			if(empty($fund[0]['title'])) $fund[0]['title'] = '';
			
			return $fund[0];
		}
		
		
}

?>