<?php defined('SYSPATH') or die('No direct script access.');
 
abstract class Model_Common extends Kohana_Model
{
  protected $name = 'common';
  protected $options = array();
  
  public function __construct($options = null)
  {
    $config = Kohana::$config->load('default-options.'.$this->name);
    
    if (!empty($config))
    {
      $this->options = $config;
    }
    
    if (is_array($options))
    {
      foreach ($options as $k => $v)
      {
        $this->options[$k] = $v;
      }
    }
  }
  
  public function set($key, $value)
  {
    $this->options[$key] = $value;
  }
  
  public function get($key)
  {
    if (isset($this->options[$key])) {
      return $this->options[$key];
    }
    
    return false;
  }
}

?>