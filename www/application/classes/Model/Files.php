<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Files extends Kohana_Model
{
    protected $table_ = 'files';
		

    /**
     * Get all slides
     * @return array
     */
		public $siteConfig_;
		
		function __construct() 
		{
			$this->siteConfig_ =  Kohana::$config->load('site');
		}
		
    public function getFile($hash)
    {
         return DB::select()                 // SELECT - DB:SELECT
            ->from($this->table_)               // Из таблицы 'post'
            ->where('hash', '=', $hash)
            ->execute()                   // Выполняем
            ->as_array();                 // Результат ввиде массива
    }
  
  public function getFileById($id)
  {
    return DB::select()
      ->from($this->table_)
      ->where('id', '=', $id)
      ->execute()
      ->as_array();
  }
  public function getPath($hash) 
  {	
    if($hash == '') {
      return false;
    }

    $rowImg = $this->getFile($hash);
    
    if (empty($rowImg)) {
      return false;
    }
    
    $rowImg = $rowImg[0];
    $year=date('Y', $rowImg['date_creating']);
    $month=date('m', $rowImg['date_creating']);
    $day=date('d', $rowImg['date_creating']);
    
    return $this->siteConfig_['uploadsPath'].'/'.$year.'/'.$month.'/'.$day.'/';

  }
		
		public function getImage($hash, $size = '')
		{
			$path = $this->getPath($hash);
			$hashForMiniatures = $this->getMD5HashForMiniatures($hash);
			$name = $hash;
			
			switch($size) {
				case 'F350-350':
				  $locName = 'F350-350_' . $hashForMiniatures . '.jpg';
					if ($this->isFileExists($path, $locName)) {
						$name = $locName;
					}
				  break;
			  
				case 'R350-350':
				  $locName = 'R350-350_' . $hashForMiniatures . '.jpg';
					if ($this->isFileExists($path, $locName)) {
						$name = $locName;
					}
				  break;
				
				case 'R975-975':
				  $locName = 'R975-975_' . $hashForMiniatures . '.jpg';
					if ($this->isFileExists($path, $locName)) {
						$name = $locName;
					}
				  break;	
					
				default:
				  break;
			}
			
			return $path . $name;
		}
		
		private function getMD5HashForMiniatures($hash)
    {
     return md5(str_replace('.jpg', '', $hash));	    
    }
		
		public function isFileExists($path, $locName)
		{
			return file_exists(__DIR__ . '/../../../' . $path . $locName);
		}
}
?>