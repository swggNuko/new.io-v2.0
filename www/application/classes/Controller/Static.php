<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Static extends Controller_Common 
{
    public function action_about() 
		{
			$this->setTemplateParams();
			
			View::set_global('title', 'О музее');				
    	View::set_global('description', '');
      
			$content = View::factory('/pages/static/about')
											->bind('news', $news);
			
			$newsModel = new Model_News();
			$news = $newsModel->getLast3News();
										
      $this->template->content = $content;
    }
	
		public function action_contacts() 
		{
			$this->setTemplateParams();
			
			View::set_global('title', 'Контакты');				
    	View::set_global('description', '');
		
      $content = View::factory('/pages/static/contacts');
      $this->template->content = $content;
  	}
		
		public function action_structure() 
		{
			$this->setTemplateParams();
			
			View::set_global('title', 'Структура музея');				
    	View::set_global('description', '');
		
      $content = View::factory('/pages/static/structure');
      $this->template->content = $content;
  	}
  
  public function action_volunteers()
  {
    $this->setTemplateParams();
    
    $content = View::factory('/pages/static/volunteers')
      ->bind('volunteersCount', $volunteersCount)
      ->bind('volunteersEvents', $volunteersEvents);
    
    $VolunteersModel = new Model_Volunteers();
    $volunteersCount = $VolunteersModel->getVolunteersCount();    
    $volunteersEvents = $VolunteersModel->getEvents();
    
    View::set_global('title', 'Волонтеры');				
    View::set_global('description', '');
		
    array_push($this->template->scripts, 'volunteers');
    
    $this->template->content = $content;
  }
		
  private function setTemplateParams() 
  {
      $this->template->headerClasses = 'header-gray';
      $this->template->headerImagesPostfix = 'white';
      $this->template->glassesImage = false;
  }
 
} // End Static
?>