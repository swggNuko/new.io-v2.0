<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Handler extends Controller_Template
{  
  public $template = '/api/main';
  
  public function action_index() 
  {
    $this->template
      ->bind('data', $data);
    
    $module = $this->request->param('module');
    $type = $this->request->param('type');
    $APIModule = null;
    
    switch ($module)
    {
      case 'exhibit':
        $APIModule = new Model_Api_Exhibits();
        break;
      case 'events':
        $APIModule = new Model_Api_Events();
        break;
      case 'news':
        $APIModule = new Model_Api_News();
        break;
      case 'pages':
        $APIModule = new Model_Api_Pages();
        break;
      case 'search':
        $APIModule = new Model_Api_Search();
        break;
      case 'calendar':
        $APIModule = new Model_Api_Calendar();
        break;
      case 'messages':
        $APIModule = new Model_Api_Messages();
        break;
      case 'volunteers':
        $APIModule = new Model_Api_Volunteers();
        break;
      case 'excursions':
        $APIModule = new Model_Api_Excursions();
        break;
      default:
        throw new HTTP_Exception_404();
        break;
    }
    
    $data = $APIModule->handler($type, array_merge($_GET, $_POST));
  }
}

?>