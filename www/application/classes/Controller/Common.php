<?php defined('SYSPATH') or die('No direct script access.');
 
abstract class Controller_Common extends Controller_Template 
{ 
  public $template = 'main-template';

  public function before() 
  {
    parent::before();//нужен, чтобы метод before родителя не перетирался.
    $this->template = Controller_Common::CreateTemplate();
    
  }
  
  public static function CreateTemplate($template = 'main-template') {

    $template = View::factory($template)
      ->bind('admin', $admin);

    $AuthModel = new Model_Service_Auth();

    $admin = $AuthModel->checkAdminAccess();

    $siteConfig =  Kohana::$config->load('site');

    View::set_global('title', $siteConfig['title']);				
    View::set_global('description', $siteConfig['description']);
    View::set_global('fixedTimetable', 'false');	

    $template->content = '';

    $template->styles = array(
      'animate.min',
      'bootstrap',
      'font-awesome.min', 
      'jquery-ui',
      'linearicons',
      'magnific-popup',
      'main',
      'style',
      'nice-select',
      'owl.carousel',
    );
    $template->scripts = array(
      'vendor/jquery-2.2.4.min',
      'vendor/bootstrap.min',
      'easing.min',
      'hoverIntent',
      'superfish.min',
      'jquery.ajaxchimp.min',
      'jquery.magnific-popup.min',
      'mn-accordion',
      'jquery-ui',
      'jquery.nice-select.min',
      'owl.carousel.min',
      'mail-script',
      'main',
    );
    
    $template->libs = array(
      /*
      'jquery-3.2.1.min',
      'jquery.cookie',
      'jquery-ui.min',
      'mouse.parallax'
      */
    );
    

    $template->headerClasses = ''; // Дополнительные классы для header
    $template->headerImagesPostfix = 'gray'; // Постфиксы для изображений в header
    $template->timetableClasses = ''; // Дополнительные классы для расписания
    $template->glassesImage = false;
    
    return $template;
  }
} //Common
?>