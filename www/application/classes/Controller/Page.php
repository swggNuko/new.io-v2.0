<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Page extends Controller_Common
{	
  protected $editable_name_ = 'pages';

  public function action_index() 
  {	
    $this->template->headerClasses = 'header-gray header-bg';
    $this->template->headerImagesPostfix = 'white';
    $this->template->glassesImage = true;	
    $this->template->timetableClasses = 'timetable-gray';

    array_push($this->template->scripts, 'header');

    View::set_global('fixedTimetable', 'true');


    $content = View::factory('/pages/main')
      ->bind('news', $news)
      ->bind('technews', $technews)
      ->bind('protocol', $protocol);


    //$events = $EventsModel->getLast3Event();

    /*
    getActualEvents($type)
    $type = 1 - выставка
    $type = 2 - концерт
    */

    $NewsModel = new Model_News(array(
      'handleShortcodes' => false
    ));
    $news = $NewsModel->getLast3News();

    $technews = $NewsModel->getLastTechNews(4);

    $protocol = Kohana::$config->load('site.protocol');
    
    $this->template->content = $content;
  }
  
  public function action_page()
  {
    $this->template->headerClasses = 'header-gray';
    $this->template->headerImagesPostfix = 'white';
    $this->template->glassesImage = false;
    
    array_push($this->template->styles, 'modules/show-media');
    array_push($this->template->scripts, 'modules/show-module');

    
    $content = View::factory("/pages/page")
      ->bind("page", $page);

    $page = $this->request->param('page');
    
    $PageModel = new Model_Page();
    $page = $PageModel->getPage($page);
    
    if (empty($page)) {
      throw new HTTP_Exception_404;
    }

    View::set_global('title', $page['title']);				
    View::set_global('description', '');

    $this->template->content = $content;
  }
} // Page
?>