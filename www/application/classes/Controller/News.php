<?php defined('SYSPATH') or die('No direct script access.');
class Controller_News extends Controller_Common
{	
	public $table_ = 'news';
	public $numbering_ = false;
    protected $editable_name_ = 'news';
	
	public function action_index() 
	{	
      $this->setTemplateParams();

      $content = View::factory('/pages/news/news')
        ->bind('page', $page)
        ->bind('name', $name)
        ->bind('news', $news)
        ->bind('noNews', $noNews)
        ->bind('numbering', $this->numbering_);

      $page = $this->request->param('page');

      $name = 'archive';

      if($page < 1) $page = 1;

      $NumberingModel = new Model_Service_Numbering();
      $this->numbering_ = $NumberingModel->getPagesRange(
                                      $page, 
                                      $this->table_
                                      );	

      $noNews = false;

      $newsModel = new Model_News();
      $news = $newsModel->getNews($page);

      if(empty($news) && $page != 1) {
        throw new HTTP_Exception_404;
      } elseif(!$news) {
        $noNews = true;
      }
      
      View::set_global('title', 'New.IO | Новости');
      View::set_global('description', '');

      $this->template->content = $content;
 	}

  public function action_category()
  {
    $this->setTemplateParams();

    $content = View::factory('/pages/news/news')
      ->bind('name', $name)
      ->bind('page', $page)
      ->bind('news', $news)
      ->bind('noNews', $noNews)
      ->bind('numbering', $this->numbering_);

    $page = $this->request->param('page');
    $category = $this->request->param('category');

    $name = 'category/'.$category;

    if($category < 1) $category = 1;
    if($page < 1) $page = 1;

    $NumberingModel = new Model_Service_Numbering();
    $this->numbering_ = $NumberingModel->getPagesRange(
      $page,
      $this->table_,
      array('type_v','=',$category)
    );

    $noNews = false;

    $newsModel = new Model_News();
    $news = $newsModel->getNewsCategory($page,$category);

    if(empty($news) && $page != 1) {
      throw new HTTP_Exception_404;
    } elseif(!$news) {
      $noNews = true;
    }

    View::set_global('title', 'New.IO | Новости');
    View::set_global('description', '');

    $this->template->content = $content;
  }

  public function action_categories()
  {
    $this->setTemplateParams();

    $content = View::factory('/pages/news/category')
      ->bind('categories', $categories)
      ->bind('news', $news);

    $newsModel = new Model_News();


    $categories = $newsModel->getCategories();

    $news = $newsModel->getLast3News();




    View::set_global('title', 'New.IO | Категории');
    View::set_global('description', '');

    $this->template->content = $content;
  }
	
	public function action_new() 
	{	
      array_push($this->template->scripts, 'exhibit');

      $this->setTemplateParams();

      $content = View::factory('/pages/news/new')
        ->bind('new', $new)
        ->bind('prevId', $prevId)
        ->bind('nextId', $nextId);

      $id = $this->request->param('id');

      $newsModel = new Model_News(array(
        'imageType' => 'R975-975'
      ));
      
      $new = $newsModel->getNew($id);

      if(empty($new)) {
          throw new HTTP_Exception_404;
      }

      $prevId = $newsModel->getPrev($id);
      $nextId = $newsModel->getNext($id);
      
      View::set_global('title', $new['title']);				
      View::set_global('description', '');

      $this->template->content = $content;
 	}
	
	private function setTemplateParams() 
	{
      $this->template->headerClasses = 'header-gray';
      $this->template->headerImagesPostfix = 'white';
      $this->template->glassesImage = false;
	}
	 
} // News
?>