<?php
  $pageTitle='Страницы';
	$GLOBALS['access'] = array(1, 2);
  include_once('templates/header.php');
?>

  <?php

    include_once('components/config.php');
    $opts['table'] = 'pages';
    

    $opts['key'] = 'id';
    
    $opts['key_type'] = 'int';
    
    $opts['inc'] = 30;
    
    $opts['view'] = 'page';
    
    $opts['fdd']['title'] = array(
        'name'     => 'Название',
        'type'     => 'text',
        'maxlen'   => 255,
        'sort'     => true,
        'editable' => true,
				'display-in-table' => true,
        'others'    => array(
          'classes-show-table' => 'title',
          'classes-edit-table' => 'input__title'
        )
    );
    
    $opts['fdd']['keywords'] = array(
        'name'     => 'Ключевые слова',
        'type'     => 'text',
        'maxlen'   => 255,
        'sort'     => true,
        'editable' => true,
				'display-in-table' => true,
        'others'    => array(
        'classes-edit-table' => 'input__work-title'
      )
    );
        
    
    $opts['fdd']['description'] = array(
        'name'     => 'Описание',
        'type'     => 'textarea',
				'display-in-table' => true,
				'editable' => true
    );
    
    $opts['fdd']['content'] = array(
        'name'     => 'Содержимое',
        'type'     => 'textarea',
        'ckeditor' => true,
				'display-in-table' => true,
				'editable' => true
    );
    
    $opts['fdd']['content_right'] = array(
        'name'     => 'Правая колонка',
        'type'     => 'textarea',
        'ckeditor' => true
    );
    
    $opts['fdd']['content_footer'] = array(
        'name'     => 'Подвал',
        'type'     => 'textarea',
        'ckeditor' => true
    );
    
    $opts['fdd']['url'] = array(
        'name'     => 'Url',
        'type'     => 'text',
        'maxlen'   => 255,
        'sort'     => true,
        'editable' => true,
				'display-in-table' => true,
        'others'    => array(
        'classes-edit-table' => 'input__work-title'
      )
    );
    
    $opts['fdd']['date_last_editing'] = array(
        'name'     => 'Последнее редактирование',
        'type'     => 'date',
        'maxlen'   => 10,
        'sort'     => true,
        'disabled' => true,
      'others'    => array(
        'classes-show-table' => 'date'
      )
    );
        
    
        
    $opts['fdd']['title_en'] = array(
        'name'     => 'Title',
        'type'     => 'text',
        'maxlen'   => 255,
        'sort'     => true,
      'others'    => array(
        'classes-show-table' => 'title', 
        'classes-edit-table' => 'input__title'
      )
    );
    
    $opts['fdd']['keywords_en'] = array(
        'name'     => 'Keywords',
        'type'     => 'text',
        'maxlen'   => 255,
        'others'    => array(
        'classes-edit-table' => 'input__work-title'
      )
    );
    
    $opts['fdd']['description_en'] = array(
        'name'     => 'Description',
        'type'     => 'textarea'
    );
    
    $opts['fdd']['content_en'] = array(
        'name'     => 'Content',
        'type'     => 'textarea',
        'ckeditor' => true
    );
    
    
    
    include_once('components/edit.class.php');
    new editClass($opts);
  ?>
  
<?php
  include_once('templates/footer.php');
?>
