<?php
  $pageTitle='Последние действия в системе';
  include_once('templates/header.php');
?>

<?php
  require_once('components/controllers/view/LastActivity/LastActivity.class.php');
  
  $lastActivity = new View\LastActivity();
  
  $userId = (isset($_GET['userId']) ? $_GET['userId'] : 0);
  
  $array = $lastActivity->getLastActivityForLastWeak($userId);
?>

<div class="personal-area-header">
  <?=(isset($_GET['name']) ? '<div class="associate-name">' . $_GET['name'] . '</div>' : '')?>
  <div class="title">Последние действия в системе</div>
</div>

<div class="main-content-table">
  <table>
    <tr>
      <th>Дата</th>
      <th>Раздел</th>
      <th>Пользователь</th>
      <th>Действие</th>
    </tr>

    <?php 
      foreach ($array as $ar) {
    ?>    

    <tr class="row">
      <td><?=date('d.m.y H:i', $ar['date'])?></td>
      <td><?=$ar['name']?></td>
      <td><?=$ar['user']?></td>
      <td>
        <?=$ar['type']?>
        <?php
          if ($ar['record_url'] != '') {
            echo ' <a href="' . $ar['record_url'] . '" ' . (($ar['record_url']{0} == '#') ? 'class="in-development"' : '') . '>«' . $ar['record_name'] . '»</a>';
          }
        ?>
      </td>
    </tr>

    <?php	  
      }
    ?>

  </table>
</div>

<?php
  include_once('templates/footer.php');
?>