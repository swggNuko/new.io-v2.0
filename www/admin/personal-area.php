<?php
  $pageTitle='Личный кабинет пользователя';
  $GLOBALS['access'] = array(1, 2, 3, 4, 5);
  include_once('templates/header.php');
?>
<script src="js/personal-area.js"></script>
<?php
  require_once(__DIR__ . '/components/controllers/data/PersonalArea/PersonalArea.class.php');
  $personalArea = new Base\PersonalArea($GLOBALS['userId']);
  $personalAreaData = $personalArea->getUserData();
  
  require_once(__DIR__ . '/components/controllers/view/Notes/Notes.class.php');
  $note = new Base\Notes($GLOBALS['userId']);
?>
  <div class="personal-area-header">
    <div class="associate-name"><?=$personalAreaData['name'] . ' ' . $personalAreaData['surname']?></div>
    <div class="title">Личный кабинет сотрудника</div>
  </div>
      
  <div class="personal-area-content">
    <div class="lt">
      <div class="note">
        <div class="header">для заметок</div>
        <span class="content" contenteditable="true" id="personal-area-note">
          <?=$note->getNote()?>
        </span>
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">Фамилия</span>
        <input type="text" maxlength="255" class="edit-item__input personal-area-edit_input" value="<?=$personalAreaData['surname']?>"  id="personal-area-surname" data-field="surname" >
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">Имя</span>
        <input type="text" maxlength="255" class="edit-item__input personal-area-edit_input" value="<?=$personalAreaData['name']?>"  id="personal-area-name" data-field="name">
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">Отчество</span>
        <input type="text" maxlength="255" class="edit-item__input personal-area-edit_input" value="<?=$personalAreaData['patronymic']?>"  id="personal-area-patronymic">
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">Должность</span>
        <input type="text" maxlength="255" class="edit-item__input personal-area-edit_input" value="<?=$personalAreaData['post']?>"  id="personal-area-post" data-field="post">
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">Права</span>
        <input type="text" maxlength="255" value="<?=$personalAreaData['rights']?>" class="edit-item__input" title="Поле недоступно для редактирования" disabled>
        <img src="img/main-png/message.png" class="change-request" onClick="alert('Для изменения этих данных свяжитесь с администратором системы');" />
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">Логин</span>
        <input type="text" maxlength="255" value="<?=$personalAreaData['login']?>" class="edit-item__input" title="Поле недоступно для редактирования" disabled>
        <img src="img/main-png/message.png" class="change-request" title="Запрос на изменение" onClick="alert('Для изменения этих данных свяжитесь с администратором системы');" />
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">Пароль</span>
        <input type="password" maxlength="255" class="edit-item__input edit-item__password personal-area-edit_input" data-field="password" id="personal-area-password">
        <span class="edit-item__pass-name">ещё раз</span>
        <input type="password" maxlength="255" class="edit-item__input edit-item__password personal-area-edit_input" data-field="password"  id="personal-area-password_rep">
        <img src="img/main-png/ok.png" class="password-status" id="personal-area-password_status" style="display:none;" />
      </div>
          
      <div class="edit-item">
        <span class="edit-item__name">E-mail</span>
        <input type="text" maxlength="255" class="edit-item__input personal-area-edit_input" value="<?=$personalAreaData['e_mail']?>"  id="personal-area-e_mail" data-field="e_mail">
      </div>
      
      
      <div class="edit-item">
        <span class="edit-item__name">Редактирование на сайте</span>
        <select id="personal-area-disabled_edit" data-field="site_disabled_edit" class="edit-item__select personal-area-edit_select">
          <option value="0" <?=($personalAreaData['site_disabled_edit'] == 0) ? 'selected' : ''?>>Да</option>
          <option value="1" <?=($personalAreaData['site_disabled_edit'] == 1) ? 'selected' : ''?>>Нет</option>
        </select>
      </div>
          
    </div>
        
    <div class="rt">
      <div class="button title">дневник активности</div>
      <?php
        $lastUserActivity = $personalArea->getLastUserActivity();
      ?>    
      <div class="activity-diary">
        <?php
          if (count($lastUserActivity) == 0) {
            echo '<div class="item empty">За последние сутки Вы ничего не делали в системе. <a href="' . $personalArea->getRandomUrl() . '">Исправить!</a></div>';	          
          } else {
            foreach ($lastUserActivity as $activity) {
        ?>
        
        <div class="item">
          <span class="date"><?=date('d.m.Y H:i', $activity['date'])?></span> <?=$activity['type']?> <a href="<?=$activity['record_url']?>" style="color: #;">«<?=$activity['record_name']?>»</a>
        </div>
        
        <?php	            
            }
          }
        ?>
      </div>
       
      <a href="last-activity.php?userId=<?=$GLOBALS['userId']?>&name=<?=$personalAreaData['name'] . ' ' . $personalAreaData['surname']?>" class="button" title="Последние действия пользователя за неделю">показать полностью</a>
    </div>
  </div>
<?php
  include_once('templates/footer.php');
?>