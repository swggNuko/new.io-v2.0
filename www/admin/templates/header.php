﻿<?php
  include_once('components/entry.php');
  
  include_once('components/admin-config.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?=$pageTitle?></title>
<link rel="stylesheet" type="text/css" href="css/ptsansnarrow.css">
<link rel="stylesheet" type="text/css" href="css/all.css">
<link rel="stylesheet" type="text/css" href="css/interface-elements.css">
<link rel="stylesheet" type="text/css" href="css/personal-area.css">

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/libs/jquery-ui-1.10.4.custom.js"></script>
<script src="ckeditor/ckeditor.js"></script>  

<script src="js/edit.js"></script>  
<script src="js/main.js?v.1.0.1"></script>
<script src="js/admin-messages.js"></script>

<script src="js/header/tools-ajax.js"></script> 
<script src="js/header/tools.js"></script>

<script src="js/edit.class/jquery.imgareaselect.pack.js"></script>
<script src="js/edit.class/resize-image.js"></script>
<script src="js/edit.class/fields-map.js"></script>

<link rel="stylesheet" type="text/css" href="css/edit.class/imgareaselect-default.css">
 
</head>

<body>
<div class="main">
<!--main START-->
  
    <div class="main-header">
    <!--main-header START-->
      <div class="main-header-title">
          <img src="img/logo.png" class="logo"  onClick="document.location='/';" />
            <span class="title" onClick="document.location='main.php';">Панель управления</span>
            <!--<img src="img/main/settings_icon.svg" class="settings in-development" />-->
        </div>
        
        <div class="main-header-user_info">
          <img src="img/main/search_icon.svg" class="search in-development" />
            <?php
              $user = new User($GLOBALS['userId']);
            ?>
            <a href="personal-area.php" class="name"><img src="img/main/user_icon.svg" class="icon" /> <?=$user->getCurrentUserName();?></a>
            <a href="<?=$_SERVER['REQUEST_URI'] . (($_GET) ? '&' : '?')?>exit=1" class="rights">выход</a>
        </div>
        
        
        <div class="main-header-menu">
          
          
        </div>
    
    <!--main-header END-->
    </div>
    
    <div class="main-content">
    <!--main-content START-->
    <div class="pop-up-bg"></div>
    
    <!--#admin-message START-->
    <div class="pop-up-bg"></div>
    <div class="pop-up" id="admin-message">
      <div class="content">
        <div class="message"></div>
        <div class="buttons">
          <div class="button ok" data-status="ok">да</div>
          <div class="button not-ok" data-status="not-ok">нет</div>
          <div class="button close" data-status="close">закрыть</div>
        </div>
      </div>
    </div>
    <!--#admin-message END-->