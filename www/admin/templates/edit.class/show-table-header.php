<!--.edit-class view settings START-->
<div class="pop-up" id="fields-pop-up">
  <div class="content">
    <div class="col-left">
    <!--.col-left START-->
      <div class="title">Поля записей раздела</div>
      <div class="checkbox-group" id="fields-pop-up-checkbox-group">
      <?php
        foreach ($allowFields as $field) {
      ?>
        <div class="item">
          <input type="checkbox" id="edit-class-field-<?=$field['id']?>" data-id="<?=$field['id']?>" data-name="<?=$field['name']?>" <?=($field['display']) ? 'checked' : ''?> data-priority="<?=$field['priority']?>">
          <label for="edit-class-field-<?=$field['id']?>" class="checkbox-label"><span></span> <?=$field['name']?></label>
        </div>
      <?php
        }
      ?>
      </div>
    <!--.col-left END-->
     </div>
        
    <div class="col-right">
    <!--.col-right START-->
      <div class="field-mapping">
        <div class="title">Настройка порядка отображения полей</div>
        <div class="fields" id="fields-map"  data-table="<?=$table?>">
          <!--<div class="item" id="fields-map-id" data-id="id"><span>ID</span></div>-->
        </div>
      </div>
    <!--.col-right END-->
    </div>
  </div>
</div>
<!--.edit-class view settings END-->

<div class="main-content-header">
  <?php
    $onlyReadDopStyle = '';
    
    if ($onlyRead) {
      $onlyReadDopStyle = 'style="display: none;"';  	    
    }
  ?>
  <div class="what_doing" <?=$onlyReadDopStyle?>>Что делать с отмеченным?</div>
            
    <div class="checkbox" <?=$onlyReadDopStyle?>>
      <input type="checkbox" id="check-all" onChange="changeAll();" />
    <label for="check-all"><span></span></label>
    </div>
    
    <!--        
    <div class="action">
      <img src="img/main/edit_icon1.svg" />
        Редактировать
    </div>
    -->
    
    
    <div class="groups-pop-up"  id="groups-pop-up">
    <!--.groups-pop-up START-->
      <div class="nav">
        <div class="item active" id="groups-pop-up-nav-create" data-id="groups-pop-up-create">Создать группу</div>
        <div class="item" id="groups-pop-up-nav-add" data-id="groups-pop-up-add">Группа</div>
      </div>
              
      <div class="content" id="groups-pop-up-create">
      <!--#groups-pop-up-create START-->
        <div class="field-title">Название группы</div>
        <input type="text" id="groups-pop-up-create__text" />
                
        <div class="field-title">Тип группы</div>
        <select  id="groups-pop-up-create__type">
        <?php
          foreach ($groupsTypes as $type) {
            echo '<option value="' . $type['id'] . '">' . $type['name'] . '</option>';	          
          }
        ?>
        </select>
                
        <div class="bottom">
         <span class="groups-pop-up-num_elements">2 элемента</span>
         <div class="button" id="groups-pop-up-create__button">
           <span>Создать</span>
         </div>
       </div>
     <!--#groups-pop-up-create END-->
     </div>
              
     <div class="content" id="groups-pop-up-add" style="display: none;">
     <!--#groups-pop-up-add START-->
       <div class="field-title">Выбрать группу</div>
               
       <div class="content__select-zone">
         <input type="text" placeholder="id или название" class="zone__text" id="groups-pop-up-add__text" autocomplete="off">
         <img src="img/main-png/arrow-bottom.png" class="zone__arrow" id="groups-pop-up-add__arrow">
       </div>
          
       <div class="content-select_list" id="groups-pop-up-add__list" style="display: none;">
        <ul>
        </ul>    
      </div>
                
      <div class="bottom">
        <span class="groups-pop-up-num_elements">2 элемента</span>
        
        <div class="button" id="groups-pop-up-add__button-delete">
          <span>Удалить из группы</span>
        </div>
        
        <div class="button" id="groups-pop-up-add__button" data-action="add">
          <span>Добавить</span>
        </div>
      </div>
    <!--#groups-pop-up-add END-->  
    </div>
              
    <!--groups-pop-up END-->  
    </div>
           
    <div class="action delete" onClick="sendForm('show-table-form', 'delete-show-table');" <?=$onlyReadDopStyle?>>
      <img src="img/main/delete_icon.svg" />
        Удалить
     </div>
            
    <a href="<?=$_SERVER['PHP_SELF'].'?sort='.$this->sortParam[$numCols].'&type='
    .$sortType.'&page='.$_GET['page'].'&operation=create'?>" class="create" <?=$onlyReadDopStyle?>>
         <span>создать</span>
    </a>
            
    <div class="search <?=($onlyRead) ? 'long-search' : ''?>">
      <form method="get" action="">
        <input type="text" name="search"  placeholder="Поиск.." <?=($_GET['search'] != '') ? 'value="' . $_GET['search'] . '"' : ''?>/>
        <img src="img/main/search_icon_grey.svg" class="submit" />
        <input type="submit" style="display:none;">
      </form>
    </div>
            
    <div class="action settings" style="visibility:hidden;">
      <img src="img/main/settings_icon_grey.svg" />
        Настройки раздела
    </div>
            
    <div class="title">
    <?php 
      global $pageTitle; 
      echo $pageTitle;
    ?>
    </div>
</div>