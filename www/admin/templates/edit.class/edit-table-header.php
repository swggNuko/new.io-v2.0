<div class="main-content-header <?=$mainContentHeaderClass?>">
   <div class="id_record">ID<?=$_GET['id']?></div>
  
  <?php
      if(isset($_GET['id']) && !empty($_GET['id'])) {
        $deleteUrl = $_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='.$_GET['type']
          .'&page='.$_GET['page'].'&operation=delete&id='.$_GET['id'];
        
        $opacity = '';
      }
      else {
        $deleteUrl = '#';
        $opacity = 'opacity: 0.5;';
      }
    ?>
    
    <?php
      $publishedPrefix = '';
      $opacityPublished = $opacity;
      if($hidden == '') {
        $opacityPublished = 'opacity: 0.5;';
      }
      else if($hidden == 1) {
        $publishedPrefix = 'not-';
      }
    ?>
    
     <?php
       $onlyReadDopStyle = '';
    
       if ($onlyRead) {
         $onlyReadDopStyle = ' display: none; ';  	    
       }
     ?>
            
    <div class="action preview_icon" onClick="predView('<?=$view?>', '<?=$url?>');" style="<?=$opacityPublished?>">
      <img src="img/main/preview_icon.svg" />
      <span>Просмотр</span>
    </div>
    
    
            
    <a href="<?=$deleteUrl?>" class="action trash" style="<?=$opacity . $onlyReadDopStyle?>">
      <img src="img/main-png/trash-icon-gray.png" />
        Удалить
    </a>
            
    <div class="save" onClick="sendForm('edit', 'save');" style="<?=$onlyReadDopStyle?>">
      <span><?=(($_GET['operation'] != 'delete') ? 'Сохранить' : 'Добавить в корзину')?></span>
    </div>
    
    
            
    <div class="action <?=$publishedPrefix?>published" style="<?=$opacityPublished . $onlyReadDopStyle?>" onClick="sendForm('edit', '<?=$hidden?>');">
      <img src="img/main-png/<?=$publishedPrefix?>published-item.png" />
        на сайте
    </div>
            
    <a href="<?=$_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='.$_GET['type']
    .'&page='.$_GET['page'] . $dopGetParams?>" class="comeback">
      Вернуться в раздел
    </a>
            
    <div class="title">
      <?php
      global $pageTitle;
      echo $pageTitle;
    ?>
    </div>
</div>