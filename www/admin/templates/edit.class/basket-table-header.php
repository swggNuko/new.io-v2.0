<!--.edit-class view settings START-->
<div class="pop-up" id="fields-pop-up">
  <div class="content">
    <div class="col-left">
    <!--.col-left START-->
      <div class="title">Поля записей раздела</div>
      <div class="checkbox-group" id="fields-pop-up-checkbox-group">
      <?php
        foreach ($allowFields as $field) {
      ?>
        <div class="item">
          <input type="checkbox" id="edit-class-field-<?=$field['id']?>" data-id="<?=$field['id']?>" data-name="<?=$field['name']?>" <?=($field['display']) ? 'checked' : ''?>>
          <label for="edit-class-field-<?=$field['id']?>" class="checkbox-label"><span></span> <?=$field['name']?></label>
        </div>
      <?php
        }
      ?>
      </div>
    <!--.col-left END-->
     </div>
        
    <div class="col-right">
    <!--.col-right START-->
      <div class="field-mapping">
        <div class="title">Настройка порядка отображения полей</div>
        <div class="fields" id="fields-map"  data-table="<?=$table?>">
          <!--<div class="item" id="fields-map-id" data-id="id"><span>ID</span></div>-->
        </div>
      </div>
    <!--.col-right END-->
    </div>
  </div>
</div>
<!--.edit-class view settings END-->

<div class="main-content-header">
  <div class="what_doing">Что делать с отмеченным?</div>
            
    <div class="checkbox">
      <input type="checkbox" id="check-all" onChange="changeAll();" />
    <label for="check-all"><span></span></label>
    </div>
    
            
    <div class="action fenix" onClick="sendForm('show-table-form', 'reestablish-show-table');">
      <img src="img/main-png/fenix.png" />
        Восстановить
    </div>
    
           
    <div class="action delete" onClick="sendForm('show-table-form', 'delete-show-table');">
      <img src="img/main-png/red-delete.png" />
        Удалить безвозвратно
     </div>
            
            
    <div class="search">
      <form method="get" action="">
        <input type="text" name="search"  placeholder="Поиск.." <?=($_GET['search'] != '') ? 'value="' . $_GET['search'] . '"' : ''?>/>
        <img src="img/main/search_icon_grey.svg" class="submit" />
        <input type="submit" style="display:none;">
      </form>
    </div>
            
    <div class="action settings" style="visibility:hidden;">
      <img src="img/main/settings_icon_grey.svg" />
        Настройки раздела
    </div>
            
    <div class="title">
    <?php 
      global $pageTitle; 
      echo $pageTitle;
    ?>
    </div>
</div>