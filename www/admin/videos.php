<?php
  $pageTitle='Видео';
  $GLOBALS['access'] = array(1, 2, 4);
  include_once('templates/header.php');
  
?>

<?php
  include_once('components/config.php');
  $opts['table'] = 'videos';
  $opts['key'] = 'id';
  $opts['key_type'] = 'int';
  $opts['inc'] = 30;
  $opts['view'] = 'video';
  
  $opts['fdd']['title'] = array(
    'name'     => 'Название',
    'type'     => 'text',
    'maxlen'   => 255,
    'sort'     => true,
    'editable' => true,
    'display-in-table' => true,
    'others'    => array(
     'classes-show-table' => 'title',
      'classes-edit-table' => 'input__title'
    )
  );

  $opts['fdd']['video'] = array(
    'name'     => 'Ссылка на видео',
    'display-in-table' => true,
    'type'     => 'text',
    'editable' => true,
  );

  $opts['fdd']['type_v'] = array(
    'name'        => 'Тип',
    'sort'        => true,
    'type'        => 'select',
    'display-in-table' => true,
    'values_from' => 'type_v',
    'from_name'   => 'name'
);

  
  $opts['fdd']['anons'] = array(
    'name'     => 'Текст анонса',
    'display-in-table' => true,
    'type'     => 'textarea',
    'editable' => true,
  );
    
  $opts['fdd']['images'] = array(
    'name'     => 'Изображение',
    'type'     => 'file',
    'filetype' => 'image',
		'display-in-table' => true
  );


  
  $opts['fdd']['date'] = array(
    'name'     => 'Дата',
    'type'     => 'date',
    'maxlen'   => 10,
    'sort'     => true,
    'display-in-table' => true,
    'others'    => array(
      'classes-show-table' => 'date'
    )
  );
  
   
  include_once('components/edit.class.php');
  new editClass($opts);
  
?>
  
<?php
  include_once('templates/footer.php');
?>
