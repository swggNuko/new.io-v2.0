<?php
  $pageTitle='Панель управления';
  include_once('templates/header.php');
?>
  <h1>Вы можете делать следующие вещи:</h1>
  
    <ul>
      <?php
        require_once(__DIR__ . '/components/controllers/view/instruments/Instruments.class.php');
        $instruments = new View\Instruments($GLOBALS['userId']);
        $instrumentsList = $instruments->getInstruments();
        
        preg_match("/^(.*?)\./", pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_BASENAME), $rgMatches);
            
        for ($i=0; $i < count($instrumentsList); $i++) {
          if (array_key_exists($instrumentsList[$i]['code'], $links)) {
      ?>
                
      <li><a href="<?=$links[$instrumentsList[$i]['code']]?>"><?=$instrumentsList[$i]['name']?></a></li>
                
      <?php
        } else {
      ?>
      <li><a href="#"  onClick="myAlert('Инструмент в разработке'); return false;"><?=$instrumentsList[$i]['name']?></a></li>
      <?php
          }
        }
      ?>
  </ul>
  

<?php
  include_once('templates/footer.php');
?>
