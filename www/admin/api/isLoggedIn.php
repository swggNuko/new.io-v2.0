<?php
  define('LOGIN_CONTROL', 1);
  
  $req = array(
    'logged_in' => false
  );
  
  if (isset($_POST['hash'])) {
    require_once(__DIR__ . '/../components/config.php');
    include_once( __DIR__ . '/../components/controllers/users/User.class.php' );
    $user = new User();
    
    if ($user->checkUser($_POST['hash'])) {
      $req['logged_in'] = true;   
    }
  }
  
  echo json_encode($req);
?>