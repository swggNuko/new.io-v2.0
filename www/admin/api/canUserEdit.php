<?php
  define('LOGIN_CONTROL', 1);
  
  $req = array(
    'can_edit' => false,
    'userId' => 0
  );
  
  if (isset($_POST['hash']) && isset($_POST['table'])) {
    require_once(__DIR__ . '/../components/config.php');
    include_once( __DIR__ . '/../components/controllers/users/User.class.php' );
    include_once( __DIR__ . '/../components/controllers/view/instruments/Instruments.class.php' );
    $user = new User();
    
    if ($user->checkUser($_POST['hash'])) {
      $userId = $user->getCurrentId();  
      $instruments = new View\Instruments($userId); 
      
      $req['can_edit'] = $instruments->canUserEdit($_POST['table']);
      $req['userId'] = $userId;
      
      $userData = $user->getCurrentUserData();
      
      if ($userData['site_disabled_edit'] == 1) {
        $req['can_edit'] = false;
      }
    }
  }
  
  echo json_encode($req);
?>