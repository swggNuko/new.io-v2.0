<?php
  include_once('components/entry.php');
  
  include_once('components/config.php');
      
  $dbHandle = new MySQLi($opts['host'], $opts['user'], $opts['password'], $opts['db']);
  $dbHandle->query("SET NAMES utf8");
      
  if (mysqli_connect_errno($dbHandle)) {
    echo 'Ошибка подключения к базе данных';
    exit;
  }
  
  switch($_GET['action']) {
    case 'delete':
      include_once('components/ajax-functions/delete-file.php');
      echo deleteFile($dbHandle, $_GET['hash'], $opts['uploadsPath'], $_GET['table'], $_GET['param'], $_GET['id_record']);
      
      break;
    
    case 'controller':
      $filePath = 'components/controllers/' . $_GET['type'] . '/' . $_GET['name'] . '/' . $_GET['handler'];
      if(file_exists($filePath)) {
        include_once($filePath);
      } else {
        echo 'Обработчик ' . $_GET['handler'] . ' не найден в контроллере ' . $_GET['type'] . '/' . $_GET['name'];
      }
      break;
    
    case 'module':      
      $filePath = 'modules/' . $_GET['module'] . '/' . $_GET['handler'];
      if(file_exists($filePath)) {
        include_once($filePath);
      } else {
        echo 'Обработчик ' . $_GET['handler'] . ' не найден в модуле ' . $_GET['module'];
      }
      break;
    
    default: 
      echo 'Неизвестный запрос. Указан неверный параметр action: ' .$_GET['action'];
      break;
    
  }
?>