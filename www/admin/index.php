<?php
include_once('components/entry.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login V2</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="css/ptsansnarrow.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <!--===============================================================================================-->
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/entry/reset-password.js"></script>
</head>
<body >

<div class="limiter">
  <div style="background-color: #252525" class="container-login100">
    <div style="background-color: #999999" class="wrap-login100">
					<span class="login100-form-title p-b-26" style="color: white; font-family: PTSansNarrowRegular" >
						       <?php
                   if(isset($_GET['error'])) {
                     switch($_GET['error']) {
                       case 1:
                         echo 'Неверный логин или пароль';
                         break;
                       case 2:
                         echo 'Ваш токен недействителен. Войдите заново';
                         break;
                       default:
                         echo 'Неизвестная ошибка';
                         break;
                     }
                   }
                   else {
                     echo 'Добро пожаловать';
                   }
                   ?>
					</span>
        <span class="login100-form-title p-b-48">
					</span>
        <form action="index.php" method="post" id="entry-form">
          <div class="main-form">
            <div  class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
              <input style="color: white" class="input100" type="text" name="login" placeholder="логин" id="login" value="<?=(isset($_GET['login']) ? $_GET['login'] : '' )?>">
            </div>

            <div class="wrap-input100 validate-input" data-validate="Enter password">
              <span></span>
              <input style="color: white" class="input100" type="password" name="password" id="password" placeholder="пароль">
            </div>

            <div class="container-login100-form-btn">
              <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <div style="font-family: PTSansNarrowRegular; font-weight: bold" onClick="sendForm('entry-form');" class="submint login100-form-btn">
                  Войти
                </div>
              </div>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/login.js"></script>

</body>
</html>