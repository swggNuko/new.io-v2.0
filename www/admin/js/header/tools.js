// JavaScript Document

'use strict';

const MAX_NUMBER_TOOLS = 8;

$(function() {
  
  var updatePanelTool = function (key, active) {
    
    var panelTool = $('.panel-tool-' +  + key);
    
    if (active == 1) {
      panelTool.css('display', 'inline-block');
    } else {
      panelTool.css('display', 'none');
    }
  };
  
  var updateToolsList = function(actualCheckBox) {
    var countActive = 0;
    
    for (var key in toolsArray) {
      countActive = (toolsArray[key]['active'] == 1) ? (countActive + 1) : countActive;
    }
    
    if (countActive == MAX_NUMBER_TOOLS) {
      for (var key in toolsArray) {
        updatePanelTool(key, toolsArray[key]['active']);
        
        if (toolsArray[key]['active'] == 1) {
          $('#header-tool-' + key).removeClass('disabled');
          $('#header-tool-' + key).addClass('active');
          $('#check-tool-' + key).prop('disabled', false);
        } else {
          $('#header-tool-' + key).removeClass('active');
          $('#header-tool-' + key).addClass('disabled');
          $('#check-tool-' + key).prop('checked', false);
          $('#check-tool-' + key).prop('disabled', true);
        }
      }
    } else {
      
      for (var key in toolsArray) {
        
        updatePanelTool(key, toolsArray[key]['active']);
        
        if (key == actualCheckBox) continue;
        
        $('#header-tool-' + key).removeClass('disabled');
        $('#header-tool-' + key).addClass('active');
        $('#check-tool-' + key).prop('disabled', false);
        
        if (toolsArray[key]['active'] == 1) {
          $('#check-tool-' + key).prop('disabled', false);
          $('#check-tool-' + key).prop('checked', true);
        } else {
          $('#check-tool-' + key).prop('checked', false);
        }
        
      }
      
    }
    
  };
  
  $('#header-tools label').click(function(e) {
    var actualCheckBox = $(this).parent().data('check-id');
    
    if ($(this).parent().hasClass('active')) {
      
      toolsArray[actualCheckBox]['active'] = ($('#check-tool-' + actualCheckBox).prop('checked')) ? 0 : 1;
      updateToolsList(actualCheckBox);
      
      updateToolsSettings(actualCheckBox);
      
    } else {
      /*var href = $('.panel-tool-' + actualCheckBox).attr('href');
      if (href == '#') {
        alert('Инструмент в разработке');
      } else {
        document.location = href;
      }*/
    }
        
  });
  
});