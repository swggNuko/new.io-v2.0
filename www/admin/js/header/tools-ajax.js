// JavaScript Document

function updateToolsSettings(id) {
  
  $.ajax({
      type: 'POST',
      url: 'ajax.php?action=controller&type=view&name=instruments&handler=instruments-handler.php',
      data: 'id=' + id,
      dataType: 'json',
      success: function(json) {
        if(!json['success']) {
          myAlert(json['message']);
        }
      }
    });
  
}