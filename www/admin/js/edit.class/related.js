// JavaScript Document
'use strict';

function changeZoneList(elem, id) {
  
  if (elem == '') {
    elem = document.getElementById(id);
  }
      
  if (elem.value == '') {
    $('#' + elem.id + '-list').hide(0);
    $('#' + elem.id + '-arrow').attr('src', 'img/main-png/arrow-bottom.png');
    return;
  }
  
  $('#' + elem.id + '-list').show(0);
  $('#' + elem.id + '-arrow').attr('src', 'img/main-png/arrow-top.png');  
}

function makeSearch(elem) {
  var params = {};
  params['value'] = $(elem).val();
  params['table'] = $(elem).data('table');
  params['field'] = $(elem).data('field');
  
  $.ajax({
    url: 'ajax.php?action=controller&type=data&name=edit.class/elements&handler=elements-related-handler.php',
    type: 'POST',
    data: params,
    dataType: 'json',        
    success:function (json) {
      if(!json['success']) {
        myAlert(json['message']);
      } else {
        printSearchRecordsResults(elem.id, json['message'], $(elem).data('signature'));            
      }
    }
  });
       
}

function printSearchRecordsResults(id, results, signature) {
  var wroteRecords = 0;
    
  if (results.length > 0) {
    var resultsString = '';
    
    $.each(results, function(index, value){
      
      if (isIssetRecord(id, value['id'])) {
        return true;
      }
      
      resultsString += '<li class="hover" data-parent-id="' + id + '" data-id="' + value['id'] + '" data-name="' + value['name'] + '">ID' + value['id'] + ' «' + value['name'] + '»</li>';
      
      wroteRecords++;
      
    });
    
    $('#' + id + '-list ul').html(resultsString);
    
    $('.item__related-select_list .hover').click(function(e) {
      selectRelatedRecord(this, signature);
      //addRelatedRecord(this, signature);
    });
      
  } 
  if (wroteRecords == 0) {
    $('#' + id + '-list ul').html('<li>Ничего не найдено</li>');    
  }
}

function selectRelatedRecord(elem, signature) {
  $('#' + $(elem).data('parent-id') + '-list li').removeClass('active');
  $(elem).removeClass('hover').addClass('active');  
}

function isIssetRecord(id, valueId) {
  var flag = false;
  
  $('#' + id + '-related-list .h-input').each(function(i) {
    if ($(this).val() == valueId) {
      flag = true;      
    }
  });
  
  return flag;  
}

function isMaxRecord(id) {
  var maxRecord = $('#' + id).data('max-record');
  var countRecords = $('#' + id + '-related-list .h-input').length;
  
  if (maxRecord == countRecords) {
    myAlert('Достигнуто максимальное количество связанных записей');
    return true;    
  }
  
  return false;  
}

function addRelatedRecord(elem, signature) {
  
  if (isMaxRecord($(elem).data('parent-id'))) {
    return;    
  }
  
  //Relation type START
  var relationType = '';
  var relationTypeName = '';
  var relationNameClass = '';
  var selectId = '#' + $(elem).data('parent-id') + '-select';
  var selectArray = new Array();
  
  if ($('select').is(selectId)) {
    
    relationNameClass = 'related-list__item-name-2';
    
    if($(selectId).val() == null) {
      myAlert('Выберите тип связи');
      return;
    }
    
    $(selectId + ' option').each(function(){
      selectArray.push(this.text);
    });
    
    relationTypeName = '<span class="related-list__item-type">' + selectArray[$(selectId).val()] + '</span>';    
    relationType = '<input type="hidden" value="' + $(selectId).val() + '" name="' +    $(elem).data('parent-id') + '-type[]" class="h-input">';
    
    
    $(selectId).removeAttr('selected');
    //$(selectId + ' .disabled').attr('disabled', 'selected');
    $(selectId + ' .disabled').prop('selected', true);
  }
  //Relation type END
  
  
  $('#' + $(elem).data('parent-id')).val('');
  changeZoneList('', $(elem).data('parent-id'));
  
  $('#' + $(elem).data('parent-id') + '-related-list').show(0);
      
  $('#' + $(elem).data('parent-id') + '-related-list').append('<div class="related-list__item" id="' + $(elem).data('parent-id') + '-related-list-' + $(elem).data('id') + '"><span class="related-list__item-id">ID' + $(elem).data('id') + '</span><span class="related-list__item-name ' + relationNameClass + '">' + signature + ' «' + $(elem).data('name') + '»</span>' + relationTypeName + '<img src="img/main-png/close.png"    class="related-list__item-close" data-id="' + $(elem).data('id') + '" /><input type="hidden" value="' + $(elem).data('id') + '" name="' +    $(elem).data('parent-id') + '-data[]" class="h-input">' + relationType + '</div>');  
  
  $('.related-list__item-close').click(function(e) {
    removeRelatedRecord(this);                      
  });
}

function removeRelatedRecord(elem) { 
  
  var parentId = $(elem).parents('.item__related-list').attr('id');
  
  $(elem).parent().remove();
  
  if ($('#' + parentId + ' .h-input').length == 0) {
    $('#' + parentId).hide(0);    
  }
}

$(document).ready(function(e) {
  
  $('.related-select__select-zone .zone__text').keyup(function(e) {
    changeZoneList(this);
    makeSearch(this);    
  });
  
  $('.related-select__select-add').click(function(e){
    var signature = $('#' + $(this).data('id')).data('signature');
    var elem = $('#' + $(this).data('id') + '-list .active');
    
    if (elem.length == 0) return;
    
    addRelatedRecord(elem, signature);
  });
  
  $('.related-list__item-close').click(function(e) {
    removeRelatedRecord(this);                      
  });
  
  
});