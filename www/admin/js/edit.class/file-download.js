// JavaScript Document
'use strict';

$(document).ready(function(e) {
  
  $('.pdf-downloader').change(function(e) { 
    
    var file_data = $(this).prop('files')[0];    
    var form_data = new FormData();
    form_data.append($(this).attr('name'), file_data);
    form_data.append('input_name', $(this).attr('name'));
    form_data.append('action', '');
    form_data.append('record_id', '');
    form_data.append('table', '');
    form_data.append('filetype', 'pdf');
    
    var param = $(this).attr('name');
    
    $('#' + param).css('display', 'none');//Выключаем полосу   
    $('#' + param + '_loader').css('display', 'block');//Включаем индикатор загрузки
    $('#' + param + '_pdf').css('display', 'none');//Прячем ссылку
    $('#' + param + '_pdf-logo').css('display', 'none');//Прячем логотип
    
    $.ajax({
      url: 'ajax.php?action=controller&type=data&name=edit.class&handler=file-download-handler.php',
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(json){
        if(!json['success']) {
          myAlert(json['message']);
        } else {
          $('#' + param + '_hash').val(json['message']);//Поле для хэш (записываетя в базу)
          
          $('#' + param + '_delete').css('display', 'block');//Кнопка удаления
          $('#' + param + '_delete').attr('data-hash', json['message']);//Устанавливаем хэш для удаления картинки
          
          $('#' + param + '_pdf-logo').css('display', 'inline-block');//Иконка PDF
          
          $('#' + param + '_pdf').css('display', 'block');//Ссылка на файл
          $('#' + param + '_pdf').attr('href', (json['path'] + json['message']));//Ссылка на файл
          $('#' + param + '_pdf').html(json['filename']);//Название файла
          
          $('#' + param).addClass('item__upload_new');//Добавляем полосе новый класс
          $('#' + param + '_item__dop_img_inf').addClass('item__dop_img_inf_with_img');//Добавляем блоку с Копирайтом и Подписью новый класс
     
          $('#' + param + '_text').html('загрузить заново');
        }
        
        $('#' + param + '_loader').css('display', 'none');//Выключаем индикатор загрузки
        $('#' + param).css('display', 'block');//Включаем обратно полосу
        
        if ($('#' + param + '_hash').val() != '') {
          $('#' + param + '_pdf').css('display', 'block');//Прячем ссылку
          $('#' + param + '_pdf-logo').css('display', 'inline-block');//Прячем логотип	        
        }
      }
    });
  });
  
  $('.image-downloader').change(function(e) {    
    var file_data = $(this).prop('files')[0];    
    var form_data = new FormData();
    form_data.append($(this).attr('name'), file_data);
    form_data.append('input_name', $(this).attr('name'));
    form_data.append('action', '');
    form_data.append('record_id', '');
    form_data.append('table', '');
    form_data.append('filetype', 'image');
    
    var param = $(this).attr('name');
    
    $(this).parent('label').parent('.item__upload').css('display', 'none');   
    $('#' + param + '_loader').css('display', 'block');
    $('#' + param + '_change').css('display', 'none');
    $('#' + param + '_img').css('display', 'none');
    
    $.ajax({
      url: 'ajax.php?action=controller&type=data&name=edit.class&handler=file-download-handler.php',
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(json){
        if(!json['success']) {
          myAlert(json['message']);
        } else {
          $('#' + param + '_hash').val(json['message']);
          
          
          $('#' + param).addClass('item__upload_new');
          $('#' + param + '_item__dop_img_inf').addClass('item__dop_img_inf_with_img');
     
          $('#' + param + '_text').html('загрузить заново');	
          
          
          $('#' + param + '_img').attr('src', (json['path'] + json['message']));//
          
          $('#' + param + '_delete').css('display', 'block');
          $('#' + param + '_delete').attr('data-hash', json['message']);//
          
          
          $('#' + param + '_change').attr('data-src', (json['path'] + json['message'])); //
          $('#' + param + '_change').attr('data-miniatures', (json['path'] + json['miniatures']));//
                    
          $('#' + param + '_save-changes').data('hash', json['message']);    //   
          
          $('#' + param + '_img').css('margin-top', '0');  
        }
        
        $('#' + param).css('display', 'block');
        $('#' + param + '_loader').css('display', 'none');
        
        if ($('#' + param + '_hash').val() != '') {
          $('#' + param + '_img').css('display', 'block');
          $('#' + param + '_change').css('display', 'block');	        
        }
      }
    });
     
  });
});