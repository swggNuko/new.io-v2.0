// JavaScript Document
'use strict';

var isGroupsDivOpen = false;
var $groupsDiv = $('#groups-pop-up');
var countChecked = 0;
var jsonChekedValues;

function getGroupsSignature(num) {
  
  var signature = '';
  
  switch (num % 10) {
    case 1 :
      signature = 'элемент';
      break;
    case 2 :
      signature = 'элемента';
      break;
    case 3 :
      signature = 'элемента';
      break;
    case 4 :
      signature = 'элемента';
      break;
    case 5 :
      signature = 'элементов';
      break;
    case 6 :
      signature = 'элементов';
      break;
    case 7 :
      signature = 'элементов';
      break;
    case 8 :
      signature = 'элементов';
      break;  
    case 9 :
      signature = 'элементов';
      break;
    default :
      signature = 'элементов';
      break; 	  
  }
  
  return signature;
}

function changeGroupsDiv() {
  if (isGroupsDivOpen) {
    //Close
    isGroupsDivOpen = false;
    $groupsDiv.hide();
  } else {
    //Open
    isGroupsDivOpen = true;
    $groupsDiv.show();
    
    countChecked = 0;
    var checkedValues = Array();
    $('#show-table-form input[type="checkbox"]:checked').each(function(index, element) {
      if (element['value'] != 0) {
        countChecked++;
        checkedValues.push(element['value']);
      }                                                            
    });
    
    jsonChekedValues = JSON.stringify(checkedValues);
    
    
    $('.groups-pop-up-num_elements').html(countChecked + ' ' + getGroupsSignature(countChecked));    
  }
}

function changeGroupsZoneList() {
  var elem = document.getElementById('groups-pop-up-add__text');
      
  if (elem.value == '') {
    $('#groups-pop-up-add__list').hide(0);
    $('#groups-pop-up-add__arrow').attr('src', 'img/main-png/arrow-bottom.png');
    return;
  }
  
  $('#groups-pop-up-add__list').show(0);
  $('#groups-pop-up-add__arrow').attr('src', 'img/main-png/arrow-top.png');  
}

function makeGroupsSearch() {
  var params = {};
  params['value'] = $('#groups-pop-up-add__text').val();
  params['table'] = 'groups';
  params['max_num_records'] = 2;
  
  $.ajax({
    url: 'ajax.php?action=controller&type=data&name=edit.class/elements&handler=elements-related-handler.php',
    type: 'POST',
    data: params,
    dataType: 'json',        
    success:function (json) {
      if(!json['success']) {
        myAlert(json['message']);
      } else {
        printSearchGroupsResults(json['message']);           
      }
    }
  });
       
}

function printSearchGroupsResults(results) {
  var wroteRecords = 0;
    
  if (results.length > 0) {
    var resultsString = '';
    
    $.each(results, function(index, value){
      
      resultsString += '<li class="hover" data-id="' + value['id'] + '" data-name="' + value['name'] + '">ID' + value['id'] + ' «' + value['name'] + '»</li>';
      
      wroteRecords++;
      
    });
    
    $('#groups-pop-up-add__list ul').html(resultsString);
    
    $('#groups-pop-up-add__list .hover').click(function(e) {
      selectRelatedGroup(this);
    });
      
  } 
  if (wroteRecords == 0) {
    $('#groups-pop-up-add__list ul').html('<li>Ничего не найдено</li>');
    $('#groups-pop-up-add__button').html('<span>Создать</span>');
    $('#groups-pop-up-add__button').attr('data-action', 'create');    
  } else {
    $('#groups-pop-up-add__button').html('<span>Добавить</span>');
    $('#groups-pop-up-add__button').attr('data-action', 'add');
  }
}

function selectRelatedGroup(elem) {
  $('#groups-pop-up-add__list li').removeClass('active').addClass('hover');
  $(elem).removeClass('hover').addClass('active');  
}

function sendAjaxToGroupsHandler(action, groupId) {
  var params = {};
  params['action'] = action;
  params['table'] = $('#show-table-form').data('table');
  params['elementsId'] = jsonChekedValues;
  params['groupId'] = groupId;
  params['name'] = $('#groups-pop-up-create__text').val();
  params['type'] = $('#groups-pop-up-create__type').val();
  
  $.ajax({
    url: 'ajax.php?action=controller&type=data&name=edit.class&handler=groups-handler.php',
    type: 'POST',
    data: params,
    dataType: 'json',        
    success:function (json) {
      if(!json['success']) {
        myAlert(json['message']);
      } else {
        document.location.reload();            
      }
    }
  });	
}

function addElementsInGroup() {
  if ($('#groups-pop-up-add__list li').hasClass('active')) {
    
    if (countChecked == 0) {
      alert('Выберите элементы для группировки!');	    
    } else {
      //Делаем Ajax запрос и в случае успешности обновляем страницу
      sendAjaxToGroupsHandler('add', $('#groups-pop-up-add__list .active').data('id'));	    
    }
  } else {
    alert('Вы не выбрали группу!');	  
  }
}

function deleteElementsFromGroup() {
  if ($('#groups-pop-up-add__list li').hasClass('active')) {
    
    if (countChecked == 0) {
      alert('Выберите элементы для группировки!');	    
    } else {
      //Делаем Ajax запрос и в случае успешности обновляем страницу
      sendAjaxToGroupsHandler('delete', $('#groups-pop-up-add__list .active').data('id'));	    
    }
  } else {
    alert('Вы не выбрали группу!');	  
  }	
}

function createGroupAndAddElements() {
  if (countChecked == 0) {
    alert('Выберите элементы для группировки!');	    
  } else {
    //Делаем Ajax запрос и в случае успешности обновляем страницу
    sendAjaxToGroupsHandler('create', 0)	    
  }	
}

$(document).ready(function(e) {
  
  $('#groups-pop-up > .nav > .item').click(function(e) {                                                              
    $('#groups-pop-up > .nav > .item').removeClass('active');
    $(this).addClass('active');
    
    $('#groups-pop-up > .content').css('display', 'none');;
    $('#' + $(this).data('id')).css('display', 'block');
  });
  
  $('#groups-pop-up-but').click(function(e) {
    changeGroupsDiv();
  });
  
  $(document).mouseup(function (e){ 
    var div = $groupsDiv; 
    
    if (!div.is(e.target)
        && div.has(e.target).length === 0) { 
      
      if (div.css('display') == 'block') {
        changeGroupsDiv();
      }
    }
  });
  
  $('#groups-pop-up-add__text').keyup(function(e) {
    changeGroupsZoneList();
    makeGroupsSearch();    
  });
  
  $('#groups-pop-up-add__button').click(function(e) {
    
    if ($(this).attr('data-action') == 'create') {
      //Open create type
      $('#groups-pop-up-create__text').val($('#groups-pop-up-add__text').val());
      $('#groups-pop-up-add__text').val('');
      $('#groups-pop-up-add__list').hide();
      
      $('#groups-pop-up > .nav > .item').removeClass('active');
      $('#groups-pop-up-nav-create').addClass('active');
      $('#groups-pop-up > .content').css('display', 'none');;
      $('#groups-pop-up-create').css('display', 'block');
      
    } else {
      addElementsInGroup();
    }
  });
  
  $('#groups-pop-up-create__button').click(function(e) {
    createGroupAndAddElements();                                                              
  });
  
  $('#groups-pop-up-add__button-delete').click(function(e) {
    deleteElementsFromGroup();                                                                
  });
    
});