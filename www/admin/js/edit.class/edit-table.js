var contentold={};

function savedata(elementIdSave, contentSave, recordId, recordEditKey, recordTable) {
  
  $.ajax({
    url: 'ajax.php?action=controller&type=data&name=edit.class&handler=table-update-handler.php',
    type: 'POST',
    data: {
      content: contentSave,
      id: recordId,
      key: recordEditKey,
      table: recordTable
    },
    dataType: 'json',        
    success:function (json) {
      if(!json['success']) {
        alert(json['message']);
        $('#' + elementIdSave).html(contentold[elementIdSave]);
      }
      
       }
     });
   }               

$(document).ready(function() {
  $('.main-content-table .row [contenteditable="true"]').mousedown(function (e) {
    
    e.stopPropagation();                                
    elementid=this.id;
    
    contentold[elementid]=$(this).html(); 
    
    $(this).bind('keydown', function(e) {  
      if(e.keyCode==27){
        e.preventDefault();
        $(this).html(contentold[elementid]);
      }
    });
    
    return;
    
  })
  
  .blur(function (event) {
    
    var elementidsave=this.id;         
    var  contentsave = $(this).html();
    var recordId = $(this).data('id');
    var recordEditKey = $(this).data('key');
    var recordTable = $(this).data('table');
    
    event.stopImmediatePropagation();
    
    if (contentsave!=contentold[elementidsave]) {    
      savedata(elementidsave, contentsave, recordId, recordEditKey, recordTable);
    }
  }); 
  
  
  $('.edit-checkbox').change(function(e) {
    
    var isOneChecked = false;
    
    $('.edit-checkbox').each(function(index, element) {
      if ($(this).prop('checked')) {
        isOneChecked = true;	      
      }
    });
    
    if (isOneChecked) {
      $('#check-all').prop('checked', true);
    } else {
      $('#check-all').prop('checked', false);
    }
    
  });
       
});