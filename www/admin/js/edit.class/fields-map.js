// JavaScript Document
'use strict';

$(function() {
  var isSomeChanged = false;
  var order;
  
  $('#fields-map').sortable({
    axis: "x",
    containment: "parent",
    update: function() {
      saveFieldSequence();
    }
  });
  
  /*field-map special functions START*/
  var saveFieldSequence = function() {
    
    order = $('#fields-map').sortable("toArray");
    
    var fieldsArray = Array();
    
    for (var i = 0; i < order.length; i++) {
      fieldsArray[i] = $('#' + order[i]).data('id');
    }
    
    $.ajax({
      url: 'ajax.php?action=controller&type=data&name=edit.class&handler=fields-map-handler.php',
      type: 'POST',
      data: {
        table: $('#fields-map').data('table'),
        fields: fieldsArray
      },
      dataType: 'json',        
      success:function (json) {
        if(!json['success']) {
          myAlert(json['message']);
        } else {
          isSomeChanged = true;
        }
      }
    });
    
  };
  
	var getSorted = function(selector, attrName) {
    return $($(selector).toArray().sort(function(a, b){
        var aVal = parseInt(a.getAttribute(attrName)),
            bVal = parseInt(b.getAttribute(attrName));
        return aVal - bVal;
    }));
   }
	
  var fieldIndex = 0;
  var insertActiveFields = function() {
    
    $('#fields-map').html('');
    
		var $checkboxGroup = getSorted('#fields-pop-up-checkbox-group input[type="checkbox"]:checked', 'data-priority');
		
    $.each($checkboxGroup, function(index, value) {
      
      if ($(value).prop('checked')) {
        $('#fields-map').append('<div class="item" id="fields-map-' + $(value).data('id')+ '" data-id="' + $(value).data('id') + '"><span>' + $(value).data('name') + '</span></div>');
        fieldIndex++;
      }
      
    });
    
  };
  
  $('#fields-pop-up-checkbox-group input[type="checkbox"]').change(function(){
    if ($(this).prop('checked')) {
      $('#fields-map').append('<div class="item" id="fields-map-' + $(this).data('id') + '" data-id="' + $(this).data('id') + '"><span>' + $(this).data('name') + '</span></div>');
      fieldIndex++;
      saveFieldSequence();
    } else {
      $('#fields-map-' + $(this).data('id')).remove();
      fieldIndex--;
      saveFieldSequence();
    }
  });
  /*field-map special functions END*/
  
  /*pop-up controller*/
  
  var closePopUp = function() {
    $('.pop-up-bg').css('display', 'none');
    $('#fields-pop-up').css('display', 'none');  
  };
  
  var openPopUp = function() {
    $('.pop-up-bg').css('display', 'block');
    $('#fields-pop-up').css('display', 'block');  
  };
  
  /*$('.pop-up-bg').click(function(){
    closePopUp();
    
    if (isSomeChanged) {
      document.location.reload();
    }
  });*/
  
  $(document).mouseup(function (e){ 
    var div = $(".pop-up > .content"); 
    if (!div.is(e.target)
         && div.has(e.target).length === 0) { 
      closePopUp();
    
      if (isSomeChanged) {
        document.location.reload();
      }
    }
  });
  
  $('#open-pop-up-fields').click(function(){
    openPopUp();
    insertActiveFields();
    isSomeChanged = false;
  });
        
});