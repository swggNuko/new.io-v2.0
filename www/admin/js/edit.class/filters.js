// JavaScript Document
'use strict';
$(document).ready(function(e) {
  
  var serializeGet = function(obj) {
    var str = [];
    for(var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  var addGet = function(url, get) {

    if (typeof(get) === 'object') {
      get = serializeGet(get);
    }

    if (url.match(/\?/)) {
      return url + '&' + get;
    }

    if (!url.match(/\.\w{3,4}$/) && url.substr(-1, 1) !== '/') {
      url += '/';
    }
  
    return url + '?' + get;
  }
  
  var deleteGetVars = function(newHref, vars) {
    
    var params = newHref
                    .replace('?', '')
                    .split('&')
                    .reduce(
                      function(p,e){
                        var a = e.split('=');
                        p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                        return p;
                      },
                      {}
                    );  
    
    var m = newHref.split('&');
    var m1 = m[0].split('?');
    
    var finHref = m1[0];
    
    var count = 0;
    
    for (var m in params) {
      if (m != vars + '[]' && count != 0) {
        finHref = addGet(finHref, m + '=' + params[m]);
      }
      
      count++;
    }
    
    return finHref;
  };
  
  var $filterDiv = $(".filters-pop-up");
  
  $('.filter').click(function(e) {
    $(this).addClass('active');
    $('#' + $(this).data('id')).show(0);
    
    var pos = $('.main-content-table').offset();
    
    $('#' + $(this).data('id')).css('left', (e.pageX - pos.left - $('#' + $(this).data('id')).outerWidth()) + 'px');
    $filterDiv = $('#' + $(this).data('id'));  
  });
  
  var flag = false;
  
  $('.filters-pop-up input[type="checkbox"]').change(function(e) {
    flag = true;
  });
  
  $(document).mouseup(function (e){ 
    var div = $filterDiv; 
    
    if (!div.is(e.target)
        && div.has(e.target).length === 0) { 
      
      if (div.css('display') == 'block') {
        div.hide();
        
        var newHref = document.location.href;
        var count = 0;
        
        $('#' + div.data('field') + '-filters input[type="checkbox"]').each(function(index, value) {
          if ($(value).prop('checked')) {
            if (count == 0) {
              //Delete get params
              newHref = deleteGetVars(newHref, div.data('field'));
            }
            count++;
            
            newHref = addGet(newHref, div.data('field') + '[]=' + $(value).val());
          }
        });
        
        if (count == 0) {
          newHref = deleteGetVars(newHref, div.data('field'));	        
        }
        
        if (flag) {
          document.location=newHref;
        }
        
        $('.filter').removeClass('active');
      }
    }
  });
  
});