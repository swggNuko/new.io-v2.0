// JavaScript Document
'use strict';

var IMG_HEADER_ID;//Global var with resize image header id
var IMG_ID;//Global var with resize image id

function preview(img, selection) {
    var scaleX = 190 / (selection.width || 1);
    var scaleY = 190 / (selection.height || 1);
		
    var src = $('#' + IMG_ID + ' + div > img').data('src');
    $('#' + IMG_ID + ' + div > img').attr('src', src);
		
    $('#' + IMG_ID + ' + div > img').css({
        width: Math.round(scaleX * $('#' + IMG_ID).width()) + 'px',
        height: Math.round(scaleY * $('#' + IMG_ID).height()) + 'px',
        marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
        marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
    });
}

function getRandomInRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function openResizeArea(src, miniatures) {

   $('<div><img src="' + miniatures + '?' + getRandomInRange(1, 100) + '" data-src="' + src + '" style="position: relative; width: 100%;" id="' + IMG_HEADER_ID + '_resize-area" /><div>') .css({
        position: 'absolute',
        top: '24px',
        left: '-80px',
        overflow: 'hidden',
        width: '190px',
        height: '190px'
    }) .insertAfter($('#' + IMG_ID));	
}

function offResizeMode() {
  $('#' + IMG_ID).imgAreaSelect({
    remove: true
  });
  
  $('#' + IMG_HEADER_ID + '_resize-area').remove();
  
  $('#' + IMG_HEADER_ID).show(0);
  $('#' + IMG_HEADER_ID + '_change').show(0);
  $('#' + IMG_HEADER_ID + '_save-changes').hide(0);
  $('#' + IMG_HEADER_ID + '_cancel-changes').hide(0);	
}

$(document).ready(function () {
   
    var params = {}; //Result resize params
    
    $('.cancel-resize-changes').click(function(e) {
      IMG_HEADER_ID = $(this).data('id');
      IMG_ID = IMG_HEADER_ID + '_img';
      
      offResizeMode();
      	    
    });
    
    $('.send-resize-params').click(function(e) {
      IMG_HEADER_ID = $(this).data('id');
      IMG_ID = IMG_HEADER_ID + '_img';
      
      offResizeMode();
      
      params['hash'] = $(this).data('hash');
			
      $.ajax({
        url: 'ajax.php?action=controller&type=data&name=edit.class&handler=resize-image-handler.php',
        type: 'POST',
        data: params,
        dataType: 'json',        
        success:function (json) {
          if(!json['success']) {
            myAlert(json['message']);
          } else {
            myAlert('Миниатюра сохранена!');	          
          }
        }
      });   	    
    });
    
    $('.open-resize-area').click(function(e) {
      IMG_HEADER_ID = $(this).data('id');
      IMG_ID = IMG_HEADER_ID + '_img';
      var src = $(this).data('src');
			      var miniatures = $(this).data('miniatures');
      
      $('#' + IMG_ID).imgAreaSelect({
        aspectRatio: '1:1',
        handles: true,
        onSelectChange: preview,
        onSelectEnd: function ( image, selection ) {
            params = selection;
        }
     });
    
      openResizeArea(src, miniatures);
      
      $('#' + IMG_HEADER_ID).hide(0);
      $(this).hide(0);
      $('.send-resize-params').show(0);
      $('.cancel-resize-changes').show(0);	    
    });
    
    
});