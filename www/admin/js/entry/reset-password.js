// JavaScript Document
'use strict';

function generatePassword(userLogin) {
  
  $.ajax({
    url: 'components/ajax-functions/reset-password-handler.php',
    type: 'POST',
    data: {
      login: userLogin
    },
    dataType: 'json',        
    success:function (json) {
      if(!json['success']) {
        myAlert(json['message']);
      } else {
        myAlert('Новый пароль отправлен на ' + json['message']);	      
      }
    }
  });
  	
}

$(document).ready(function(e) {
  $('#index-change_pass').click(function(e) {
	  
    var userLogin = $('#login').val();
    
    if (userLogin == '') {
      myAlert('Укажите имя пользователя и мы вышлем новый пароль на e-mail');
      return;	    
    }
    
    myConfirm('Выслать пароль для пользователя ' + userLogin + '?', function() {
      generatePassword(userLogin);
    });
  
  });
});