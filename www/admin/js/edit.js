// JavaScript Document
'use strict';
$(document).ready(function(e) {
  
  $('form .submit').click(function(e) {
    $(this).parent('form').submit();
  });
  
  $('.delete-pdf-now').click(function(e) {
    myConfirm('Вы уверены, что хотите удалить файл?', function() {
      
      var $this;
      
      if (!$('.delete-pdf-now').is(e.target)) {
        $this = $(e.target).offsetParent();	      
      } else {
        $this = $(e.target);	      
      }
      
      var hash=$this.data('hash');
      var table=$this.data('table');
      var param=$this.data('param');
      var idRecord=$this.data('idRecord');
      $.ajax({
          url: 'ajax.php?action=delete&hash=' + hash 
          + '&table=' + table + '&param=' + param 
          + '&id_record=' + idRecord,
          
          success: function(data){
            $('#' + param + '_pdf').css('display', 'none');
            $('#' + param + '_pdf-logo').css('display', 'none');  
            $('#' + param + '_delete').css('display', 'none');
            $('#' + param + '_hash').val('');
            
            $('#' + param).removeClass('item__upload_new');
            $('#' + param + '_item__dop_img_inf').removeClass('item__dop_img_inf_with_img');
            
            $('#' + param + '_text').html('загрузить');
          
         }
      });	    
    });
  });
    
  $('.delete-img-now').click(function(e) {
    
    myConfirm('Вы уверены, что хотите удалить изоражение?', function() {
      
      var $this;
      
      if (!$('.delete-img-now').is(e.target)) {
        $this = $(e.target).offsetParent();	      
      } else {
        $this = $(e.target);	      
      }
      
      var hash=$this.data('hash');
      var table=$this.data('table');
      var param=$this.data('param');
      var idRecord=$this.data('idRecord');
      
      $.ajax({
          url: 'ajax.php?action=delete&hash=' + hash 
          + '&table=' + table + '&param=' + param 
          + '&id_record=' + idRecord,
          success: function(data){
          $('#' + param).removeClass('item__upload_new');
          $('#' + param + '_item__dop_img_inf').removeClass('item__dop_img_inf_with_img');
          
          $('#' + param + '_img').css('display', 'none');
          $('#' + param + '_delete').css('display', 'none');
          $('#' + param + '_change').css('display', 'none');
          
          $('#' + param + '_text').html('загрузить');
          
          $('#' + param + '_hash').val('');
         }
      });
      
    });
  });
  
  $('#en').click(function() {
      $('.en').css('display', 'block');
      $('.ru').css('display', 'none');
      $('#en').css('color', '#FF1857');
      $('#ru').css('color', '#b4b6b8');
  });
    
  $('#ru').click(function() {
    $('.ru').css('display', 'block');
    $('.en').css('display', 'none');
    $('#ru').css('color', '#FF1857');
    $('#en').css('color', '#b4b6b8');
  });
  
});

function predView(view, url) {
    if(view == '' || url == '') {
      myAlert('Предварительный просмотр недоступен');
    }
    else {
      window.open('/' + view + '/' + url, '_blank');
    }
}