// JavaScript Document
'use strict';

var contentold={};

function isNormalPassword() {
  
  $('#personal-area-password_status').css('display', 'inline-block');

  if ($('#personal-area-password').val() != '' && $('#personal-area-password').val() == $('#personal-area-password_rep').val()) {
    $('#personal-area-password_status').attr('src', 'img/main-png/ok.png');
    $('#personal-area-password_status').attr('title', 'Пароль успешно изменен');
    return true;	  
  } else {
    $('#personal-area-password_status').attr('src', 'img/main-png/not-ok.png');
    $('#personal-area-password_status').attr('title', 'Пароли не совпадают');
    return false;	  
  }
}

function saveUserData(elementIdSave, contentSave, fieldName) {
  
  if (fieldName == 'password') {
    if (!isNormalPassword()) {
      return false;	    
    }
  }
  
  $.ajax({
    url: 'ajax.php?action=controller&type=data&name=PersonalArea&handler=update-handler.php',
    type: 'POST',
    data: {
      value: contentSave,
      field: fieldName
    },
    dataType: 'json',        
    success:function (json) {
      if(!json['success']) {
        myAlert(json['message']);
        $('#' + elementIdSave).val(contentold[elementIdSave]);
      }  
    }
  });	
}

$(document).ready(function(e) {
  $('.personal-area-edit_input').mousedown(function (e) {
    e.stopPropagation();                                
    var elementid=this.id;
    
    contentold[elementid]=$(this).val(); 
    
    $(this).bind('keydown', function(e) {  
      if(e.keyCode==27){
        e.preventDefault();
        $(this).val(contentold[elementid]);
      }
    });
    
    return;
    
  })
  
  .blur(function (event) {
    
    var elementidsave=this.id;         
    var  contentsave = $(this).val();
    var fieldname = $(this).data('field');
    
    event.stopImmediatePropagation();
    
    if (contentsave!=contentold[elementidsave]) {    
      saveUserData(elementidsave, contentsave, fieldname);
    }
  }); 
  
  $('.personal-area-edit_select').change(function(e) {
    var elementidsave=this.id;         
    var  contentsave = $(this).val();
    var fieldname = $(this).data('field');
    
    saveUserData(elementidsave, contentsave, fieldname);
  });
});