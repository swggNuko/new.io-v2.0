// JavaScript Document
'use strict';

function isConflict(formId) {
  
  var flag = false;
  var worked = 0;
  var len = 0;
  
  $.when( $('.edit-check-conflict').each(function(i,elem) {
    len++;
    
    var value = $(this).val();
    var table = $(this).data('table');
    var key = $(this).attr('name');
    var id = $(this).data('id');
    
    var name = $(this).data('name');
    
    $.when( $.ajax({
    url: 'ajax.php?action=controller&type=data&name=edit.class&handler=conflict-handler.php',
    type: 'POST',
    data: {
      value: value,
      key: key,
      table: table,
      id: id
    },
    dataType: 'json',        
    success:function (json) {
        if(json['success'] && json['message']) {
          flag = true;
          console.log(flag);
          myAlert('Такой ' + name + ' уже используется');
        } else if(!json['success']) {
          myAlert(json['message']);
        }
      
      }
    })
    ).then(function() {
      worked++;
      if(worked == len) {
        if(!flag) {
          $('#' + formId).submit();
        }
      }
    });
    
  })
  ).then( function () {
    if(len == 0) {
      $('#' + formId).submit();
    }
  });
}
/** 
  * sendForm(id, action, action2) - функция отправки формы
  * id - id формы
  * action - основное действие
  * acction2 - связанное действие
  */

function sendForm(id, action, action2) {
  if(action == '') {
    myAlert('Действие невозможно');
    return;
  }
  else if(action == '0') {
    $('#' + id).append('<input type="hidden" value="true" name="hiddenInput" />');
  }
  else if(action == '1') {
    $('#' + id).append('<input type="hidden" value="false" name="hiddenInput" />');
  }
  else if(action == 'delete-show-table') {
    /*if(!confirm('Вы уверены, что хотите удалить выбранные записи?')) return;
    $('#show-table-form-delete_status').val('true');
    $('#show-table-form-reestablish_status').val('false');*/
    
    myConfirm('Вы уверены, что хотите удалить выбранные записи?', function() {
      $('#show-table-form-delete_status').val('true');
      $('#show-table-form-reestablish_status').val('false');	
      isConflict(id);    
    });
    
    return;
  }
  else if(action == 'reestablish-show-table') {
    $('#show-table-form-reestablish_status').val('true');
    $('#show-table-form-delete_status').val('false');
  }
  else if(action == 'delete-show-table-exhibit') {
    /*if(!confirm('Вы уверены, что хотите удалить выбранную запись?')) return;
    $('#' + action2).attr('checked', 'checked');
    $('#show-table-form-delete_status').val('true');*/
    
    myConfirm('Вы уверены, что хотите удалить выбранные записи?', function() {
      $('#' + action2).attr('checked', 'checked');
      $('#show-table-form-delete_status').val('true');	
      isConflict(id);    
    });
    
    return;
    
  }
  
  if(document.getElementById('password-edit') != null) {
    if(document.getElementById('password-edit').value != '') {
      if(document.getElementById('password-edit').value != document.getElementById('password-edit-2').value) {
        myAlert('Введенные пароли не совпадают');
        return;
      }
    }
  }
  
  isConflict(id);
  //document.getElementById(id).submit();
  
}

function changeCheckbox(value, index, ar) {
    if(!document.getElementById('check-all').checked) document.getElementById('check-'+value).checked=false;
    else document.getElementById('check-'+value).checked=true;
  }
    
function changeAll() {
  idArray.forEach(changeCheckbox);  
}

$(document).ready(function(e) {
  $('.in-development').click(function() {
    myAlert('Инструмент в разработке');
  });
	
	var changePage = function(type) {
		var url = $('#page-' + type).data('url');
		$(location).attr('href', url);
	};
	
	$(document).keyup(function(e) {
    switch(e.keyCode) {
      case 37: 
        changePage('prev');
        break;
      case 39:
        changePage('next');
        break;
      default:
        break;
    } 
	});
});