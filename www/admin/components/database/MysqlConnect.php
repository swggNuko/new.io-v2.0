<?php

  namespace Base;
  
  
  require_once(__DIR__ . '/Mysql.php');
  require_once(__DIR__ . '/Mysql/Exception.php');
  require_once(__DIR__ . '/Mysql/Statement.php');
  
  class MysqlConnect
  {
    private $host;
    private $user;
    private $password;
    private $db;
    private $table = 'users';
    public $dbHandle;
    
    function __construct()
    {
      require(__DIR__ . '/../config.php');
      
      $this->host = $opts['host'];
      $this->user = $opts['user'];
      $this->password = $opts['password'];
      $this->db = $opts['db'];
      
      $this->connectDB();        
    }
    
    private function connectDB() 
    {
      $this->dbHandle = \Database_Mysql::create($this->host, $this->user, $this->password)
                       ->setCharset('utf8')
                       ->setDatabaseName($this->db);
    }
  }
?>