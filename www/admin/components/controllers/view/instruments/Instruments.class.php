<?php

  namespace View;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  require_once(__DIR__ . '/../../users/User.class.php');
  
  class Instruments
  {
    private $table_ = 'personal_instruments';
    
    private $userId_;
    
    private $connectDB_;
    
    private $instrumentsList_;
    
    private $rights_;

    
    private $instrumentsListDef_;
    
    function __construct($userId)
    {        
      
      $this->userId_ = $userId;
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
      $user = new \User($GLOBALS['userId']);
      $this->rights_ = $user->getCurrentRights();
      
      require(__DIR__ . '/../../../admin-config.php');
      $this->instrumentsListDef_ = $defaultInstruments;
      
      $this->setInstruments();
      
    }

    public function canUserEdit($code)
    {
      foreach ($this->instrumentsList_ as $item) {
        if ($item['code'] == $code) {
          return in_array($this->rights_, $item['access']); 
        }
      }
      
      return false;
    }
    
    public function getInstruments() 
    {      
      return $this->instrumentsList_;
    }
    
    public function changeActive($id)
    {
      if (!array_key_exists($id, $this->instrumentsList_)) {
        throw new Exception('Invalid key');
      }
      
      $this->instrumentsList_[$id]['active'] = ($this->instrumentsList_[$id]['active']) ? 0 : 1;
      
      $this->updateInstruments();
    }
    
    
    private function updateInstruments() 
    {
      $this->connectDB_->dbHandle->query('UPDATE `'.$this->table_.'` SET `instruments` = "?s" WHERE `'.$this->table_.'`.`user` = ?i', json_encode($this->instrumentsList_), $this->userId_);
    }
    
    private function setInstruments()
    {
      $resultInstruments = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` WHERE `user` = "?i"', $this->userId_);
      
      if ($resultInstruments->getNumRows() == 0) {
        
        $this->instrumentsList_ = $this->instrumentsListDef_;
        $this->insertDefaultInstruments();
        
      } else {
        $rowInstruments = $resultInstruments->fetch_assoc();
        $this->instrumentsList_ = json_decode($rowInstruments['instruments'], true);  
            
      }
      
      $this->checkConformityUserInstruments();  
      
    }
    
    private function findInstrument($code) 
    {
      
      for ($i = 0; $i < count($this->instrumentsListDef_); $i++) {
        if (!isset($this->instrumentsList_[$i])) continue;
        if ($this->instrumentsList_[$i]['code'] == $code) {
          return $i;
        }
      }
      
      return -1;
    }
    
    private function checkConformityUserInstruments() 
    {
      
      for ($i = 1; $i < count($this->instrumentsListDef_); $i++) {
        
        $instrCode = $this->findInstrument($this->instrumentsListDef_[$i]['code']);
        
        if (in_array($this->rights_, $this->instrumentsListDef_[$i]['access'])) {
          if ($instrCode == -1) {            
            array_push($this->instrumentsList_, $this->instrumentsListDef_[$i]);
          }
        } else {
          if ($instrCode != -1) {
            unset($this->instrumentsList_[$instrCode]);
          }
        }
        
      }
      
      
      $this->instrumentsList_ = array_values($this->instrumentsList_);
    }
    
    
    private function setDefaultInstruments() 
    {
      $this->instrumentsList_ = $this->instrumentsListDef_;
      
      $this->connectDB_->dbHandle->query('UPDATE `'.$this->table_.'` SET `instruments` = "?s" WHERE `'.$this->table_.'`.`user` = ?i', json_encode($this->instrumentsList_), $this->userId_);
    }
    
    private function insertDefaultInstruments()
    {
      $queryData = array(
        'user' => $this->userId_,
        'instruments' => json_encode($this->instrumentsListDef_)
      );
      
      $this->connectDB_->dbHandle->query('INSERT INTO `' . $this->table_ . '` SET ?As', $queryData);
    }
    
  }
?>