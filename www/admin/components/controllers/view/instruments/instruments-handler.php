<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('Instruments.class.php');
  $instruments = new View\Instruments($GLOBALS['userId']);
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $req['message'] = $instruments->changeActive($_POST['id']);
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
  
?>