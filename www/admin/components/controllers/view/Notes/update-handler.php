<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('Notes.class.php');
  $record = new Base\Notes($GLOBALS['userId']);
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $req['message'] = $record->setNote($_POST['content']);
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
  
?>