<?php
  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  
  class Notes
  {    
    private $userId_;
    
    private $connectDB_;
    
    private $table_ = 'notes';
    
    function __construct($userId)
    {        
      
      $this->userId_ = $userId;
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
      $this->checkNote();
    }

    public function getNote()
    { 
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` 
         WHERE `user_id`="?i"', $this->userId_);
      
      $row = $res->fetch_assoc();
      
      return $row['note'];
    }
    
 
    public function setNote($note)
    { 
      $this->connectDB_->dbHandle->query('UPDATE `' . $this->table_ . '` 
           SET `note` = "?s" WHERE `'.$this->table_.'`.`user_id` = ?i', $note, $this->userId_);
    }

    private function checkNote()
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` 
         WHERE `user_id`="?i"', $this->userId_);
      
      if ($res->getNumRows() == 0) {
        $queryData=array(
          'id' => '',
          'user_id' => $this->userId_,
          'note' => ''
        );
        
        $this->connectDB_->dbHandle->query('INSERT INTO `' . $this->table_ . '` SET ?As', $queryData);	      
      }
    }
  }
?>