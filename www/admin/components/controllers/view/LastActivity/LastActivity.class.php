<?php
  namespace View;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  require_once(__DIR__ . '/../../users/User.class.php');
  
  class LastActivity
  {
    private $tables_ = array();
    
    private $connectDB_;
    
    function __construct()
    {
      $this->connectDB_ = new \Base\MysqlConnect();
      
      require(__DIR__ . '/../../../admin-config.php');
      $this->tables_ = $tablesOfActiveComponents;	    
    }
    
    //PUBLIC FUNCTION ZONE
    public function getLastThreeForLastWeak()
    {
     $dateStart = time() - 60*60*12;
     $dateEnd = time();
     
     return $this->getActivity($dateStart, $dateEnd, 3);
    }
    
    public function getLastActivityForLastWeak($userId = 0)
    {
      $dateStart = time() - 60*60*24*7;
      $dateEnd = time();
      
      return $this->getActivity($dateStart, $dateEnd, 10000, $userId);	    
    }
    
    public function getLastUserActivity($userId)
    {
      $dateStart = time() - 60*60*24;
      $dateEnd = time();
      
      return $this->getActivity($dateStart, $dateEnd, 20, $userId);
    }
    //PUBLIC FUNCTION ZONE

    private function getActivity($dateStart, $dateEnd, $maxSize = 9999999999, $userId = 0)
    {
      
      $lastActivity = array();
      
      $user = new \User();
     
      foreach ($this->tables_ as $key => $table) {
        
        $userIdSql = '';
        
        if ($userId != 0) {
          $userIdSql = $this->connectDB_->dbHandle->prepare('AND (`user_creating` = "?i" OR `user_last_editing` = "?i")', $userId, $userId);	
        }
        
        $res = $this->connectDB_->dbHandle->query(
          'SELECT * FROM `' . $table['table'] . '` WHERE 
          (`date_creating` <= "?i" AND `date_creating` >= "?i" 
          OR `date_last_editing` <= "?i" AND `date_last_editing` >= "?i") ' . $userIdSql . '
          ORDER BY  `' . $table['table'] . '`.`date_last_editing` DESC', 
          $dateEnd, $dateStart, $dateEnd, $dateStart);
        
        if ($res->getNumRows() > 0) {
          while ($row = $res->fetch_assoc()) {
            $insertRow = array();
            
            $insertRow['type'] = ($row['date_creating'] == $row['date_last_editing']) ? 'создание' : 'редактирование';
            
            if (isset($table['pre-signature'])) {
              $insertRow['type'] = $table['pre-signature'];	            
            }
            
            if (isset($row['basket']) && $row['basket'] == 1) {
              $insertRow['type'] = 'восстановление';	            
            }
            
            
            $insertRow['type'] .= ' ' . $table['signature'];
            $insertRow['date'] = $row['date_last_editing'];
            $insertRow['record_url'] = $table['link'] . (($table['url_to_edit_disabled']) ? '': '?operation=edit&id=' . $row['id']);
            $insertRow['name'] = $table['name'];
            
            try {
              $insertRow['record_name'] = $this->getRecordName($table['table'], $row['id'], $table['record_name']);
            }
            catch (Exception $error) {
              $insertRow['record_name'] = '';	            
            }
            
            try {      
              $insertRow['user'] = $user->getUserNameFromId($row['user_last_editing']);
            }
            catch (\Exception $error) {
              $insertRow['user'] = 'Удаленный пользователь';
            }
            
            
            
            array_push($lastActivity, $insertRow);
          }
        }
        	      
      }
      
      usort($lastActivity, function($a,$b) 
        { 
          return -1 * ($a['date']-$b['date']); 
        }
      );
      
      
      return array_slice($lastActivity, 0, $maxSize);	    
    }
    
    private function getRecordName($table, $id, $field)
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $table . '` WHERE `id` = "?i"', $id);
      
      if ($res->getNumRows() != 1) {
        throw new Exception('Unknown record');	      
      }
      
      $row = $res->fetch_assoc();
      
      return $row[$field];
      	    
    }
    
	  
  }
?>