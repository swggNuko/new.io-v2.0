<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('PersonalArea.class.php');
  $record = new Base\PersonalArea($GLOBALS['userId']);
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $req['message'] = $record->updateUserData($_POST['field'], $_POST['value']);
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
  
?>