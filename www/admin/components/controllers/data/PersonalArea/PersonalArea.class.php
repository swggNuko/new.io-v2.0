<?php
  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  require_once(__DIR__ . '/../../users/User.class.php');
  require_once(__DIR__ . '/../../view/LastActivity/LastActivity.class.php');
  require_once(__DIR__ . '/../../view/instruments/Instruments.class.php');
  
  class PersonalArea
  {    
    private $userId_;
    
    private $connectDB_;
    
    private $user_;
    
    
    function __construct($userId)
    {        
      
      $this->userId_ = $userId;
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
      $this->user_ = new \User($this->userId_);
      
    }

    public function getUserData()
    { 
      $userData = $this->user_->getCurrentUserData();
      
      return $userData;
    }

    public function updateUserData($field, $value)
    {
      $this->user_->updateCurrentUserData($field, $value);
    }
    

    public function getLastUserActivity()
    {
      $activityArray = array();
      
      $lastActivity = new \View\LastActivity();
      
      $activityArray = $lastActivity->getLastUserActivity($this->userId_);
      
      return $activityArray;
    }
    

    public function getRandomUrl()
    {
      $url = '';
      
      $instruments = new \View\Instruments($this->userId_);
      
      $userInstruments = $instruments->getInstruments();
      
      $i = rand(0, count($userInstruments) - 1);
      
      require(__DIR__ . '/../../../admin-config.php');
      
      $url = $links[$userInstruments[$i]['code']];
      
      return $url;
    }
    /*PUBLIC FUNCTION ZONE END*/
  }
?>