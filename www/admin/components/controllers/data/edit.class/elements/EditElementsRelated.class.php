<?php
  
  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../../database/MysqlConnect.php');
  require_once(__DIR__ . '/EditElements.interface.php');
  
  class EditElementsRelated implements EditElementsInterface
  {
    private $connectDB_;
    
    private $params_;
    
    private $userId_;
    
    private $table_;
    
    function __construct($table, $params = array())
    {
      $this->params_ = $params;
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
      $this->userId_ = $GLOBALS['userId'];
      
      
      $this->table_ = (empty($this->params_['others']['table'])) ? $table : $this->params_['others']['table'];      
    }
    
    //PUBLIC FUNCTION ZONE START
    
    public function printElement()
    {       
      switch ($this->params_['others']['type']) {
        case 'one-field':
          $this->printOneFieldElement();
          break;
        
        case 'two-field':
          $this->printTwoFieldElement();
          break;  
        
        default:
          throw new Exception('Unknown type of related element "' . $this->params_['others']['type'] . '"');        
      }
    }
    
    
    //PUBLIC FUNCTION ZONE END
    
    public function searchRecords($value, $maxNumRecords = 3)
    {
      $nameColumn = ($this->findNameColumn()) ? 'name' : 'title';
      
      if (isset($this->params_['others']['field']) && !empty($this->params_['others']['field'])) {
        $nameColumn = $this->params_['others']['field'];	      
      }
      
      $res = $this->connectDB_->dbHandle->query(
        'SELECT * FROM `' . $this->table_ . '` WHERE `id` LIKE "%?s%" OR 
        `' . $nameColumn . '` LIKE "%?s%" ORDER BY `' . $this->table_ . '`.`id` DESC', 
        $value,
        $value
      );
      
      $searchResults = array();
      
      if ($res->getNumRows() > 0) {
        $count = 1;
        
        while (($row = $res->fetch_assoc()) && ($count <= $maxNumRecords)) {
          $searchResult = array();
          $searchResult['id'] = $row['id'];
          $searchResult['name'] = $row[$nameColumn];
          
          array_push($searchResults, $searchResult);
          
          $count++;          
        }
      }
      
      return $searchResults;      
    }
    
    private function findNameColumn($columnName = 'name')
    {
      $flag = false;
       
      $resColumn = $this->connectDB_->dbHandle->query(
        'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "' . $this->table_ . '"'
      );
      
      while ($row = $resColumn->fetch_assoc()) {
        if ($row['COLUMN_NAME'] == $columnName) {
          $flag = true;
          break;          
        }
      }
      
      return $flag;      
    }
    
    private function getArrayOfIdFromRelated()
    {
      $allArray = json_decode($this->params_['value']);
      $idArray = array();
      
      foreach ($allArray as $arr) {
        array_push($idArray, $arr->id);
      }
      
      return $idArray;
    }
    
    private function getTypeToRelatedId($id) {
      //$this->params_['value']
      $allArray = json_decode($this->params_['value']);
      
      $type = '';
      
      foreach ($allArray as $arr) {
        if ($arr->id == $id) {
          $type = $arr->type;
        }
      }
      
      return $type;
    }
    
    private function getRelatedRecords()
    {
      $records = array();
      
      $idList = (!empty($this->params_['value']) && $this->params_['value'] != 'null') ? $this->getArrayOfIdFromRelated() : array();
      
      $res = $this->connectDB_->dbHandle->query(
        'SELECT * FROM `' . $this->params_['others']['table'] . '` WHERE `id` IN (?ai)', 
        $idList
      );
      
      if ($res->getNumRows() > 0) {
        while ($row = $res->fetch_assoc()) {
          $record = array();
          $record['id'] = $row['id'];
          
          if (isset($this->params_['others']['field']) && !empty($this->params_['others']['field'])) {
            $record['name'] = $row[$this->params_['others']['field']];	      
          } else {
            $record['name'] = (isset($row['name'])) ? $row['name'] : $row['title'];
          }
          
          $record['type'] = $this->getTypeToRelatedId($row['id']);
          array_push($records, $record);          
        }
      }
      
      return $records;      
    }
    
    private function getTypeName($typeId)
    {
      $typeName = 'неизвестный тип связи';
      
      for ($i = 1; $i <= count($this->params_['others']['select']); $i++) {
        if ($i == $typeId) $typeName = $this->params_['others']['select'][$i];      
      }
      
      return $typeName;
      
    }
    
    private function printOneFieldElement()
    {  
      $records = $this->getRelatedRecords();
      
      
      echo '
        <div class="item__related-list" id="' . $this->params_['key'] . '-related-list">
        <!--.item__related-list START-->
      ';
        
      if (count($records) > 0) {
      
        foreach ($records as $record) {
        
          echo '
              <div class="related-list__item"  id="' . $this->params_['key'] . '-related-list-' . $record['id'] . '">
                <span class="related-list__item-id">ID' . $record['id'] . '</span>
                <span class="related-list__item-name">' . $this->params_['others']['signature'] . ' «' . $record['name'] . '»</span>
                <img src="img/main-png/close.png" class="related-list__item-close" data-id="' . $record['id'] . '" />
                <input type="hidden" value="' . $record['id'] . '" class="h-input" name="' . $this->params_['key'] . '-data[]">
          ';
          
          if (!empty($record['type'])) {
            echo '<input type="hidden" value="' . $record['type'] . '" class="h-input" name="' . $this->params_['key'] . '-type[]">';
          }
          
          echo '
              </div>
          ';
        }
      } else {
        echo '
          <style>
            #' . $this->params_['key'] . '-related-list { display: none; }
          </style>
        ';        
      }
      
      echo '
        <!--item__related-list END-->  
        </div>
      ';
                  
      echo '<div class="item__related-select">
                    <div class="related-select__select-name">' . $this->params_['others']['main_signature'] . '</div>
                    
                    <div class="related-select__select-zone">
                      <input type="text" placeholder="выбор" class="zone__text" name="' . $this->params_['key'] . '" id="' . $this->params_['key'] . '" data-table="' . $this->table_ . '" data-signature="' . $this->params_['others']['signature'] . '" data-max-record="' . $this->params_['others']['max_record'] . '" autocomplete="off" data-field="' . $this->params_['others']['field'] . '" />
                      <img src="img/main-png/arrow-bottom.png" class="zone__arrow" id="' . $this->params_['key'] . '-arrow" />
                    </div>
                    
                    <div class="related-select__select-add" data-id="' . $this->params_['key'] . '">Добавить</div>
    
                  </div>
                  
                  <div class="item__related-select_list" id="' . $this->params_['key'] . '-list">
                    <ul>
                      <li>Ничего не найдено</li>
                    </ul>    
                  </div>';      
    }
    
    private function printTwoFieldElement()
    {  
      $records = $this->getRelatedRecords();  
      
      echo '
        <div class="item__related-list" id="' . $this->params_['key'] . '-related-list">
        <!--.item__related-list START-->
      ';
        
      if (count($records) > 0) {
      
        foreach ($records as $record) {
        
          echo '
              <div class="related-list__item"  id="' . $this->params_['key'] . '-related-list-' . $record['id'] . '">
                <span class="related-list__item-id">ID' . $record['id'] . '</span>
                <span class="related-list__item-name related-list__item-name-2">' . $this->params_['others']['signature'] . ' «' . $record['name'] . '»</span>
                <span class="related-list__item-type">' . $this->getTypeName($record['type']) . '</span>
                <img src="img/main-png/close.png" class="related-list__item-close" data-id="' . $record['id'] . '" />
                <input type="hidden" value="' . $record['id'] . '" class="h-input" name="' . $this->params_['key'] . '-data[]">
                <input type="hidden" value="' . $record['type'] . '" class="h-input" name="' . $this->params_['key'] . '-type[]">
              </div>
          ';
        }
      } else {
        echo '
          <style>
            #' . $this->params_['key'] . '-related-list { display: none; }
          </style>
        ';        
      }
      
      echo '
        <!--item__related-list END-->  
        </div>
      ';
                  
      echo '<div class="item__related-select">
                    <div class="related-select__select-name">' . $this->params_['others']['main_signature'] . '</div>
                    
                    <div class="related-select__select-zone  related-select__select-zone-2">
                      <input type="text" placeholder="выбор" class="zone__text" name="' . $this->params_['key'] . '" id="' . $this->params_['key'] . '" data-table="' . $this->table_ . '" data-signature="' . $this->params_['others']['signature'] . '" data-max-record="' . $this->params_['others']['max_record'] . '" autocomplete="off" />
                      <img src="img/main-png/arrow-bottom.png" class="zone__arrow" id="' . $this->params_['key'] . '-arrow" />
                    </div>
                    
                                        <select class="related-select__select-select" id="' . $this->params_['key'] . '-select">
                      <option disabled selected class="disabled">выбор</option>
      ';
                for ($i = 1; $i <= count($this->params_['others']['select']); $i++) {                echo '<option value="' . $i . '">' . $this->params_['others']['select'][$i] . '</option>';            }
      
                      
      echo '         </select>';
                    
      echo '         <div class="related-select__select-add" data-id="' . $this->params_['key'] . '">Добавить</div>
    
                    </div>
                  
                  <div class="item__related-select_list item__related-select_list-2" id="' . $this->params_['key'] . '-list">
                    <ul>
                      <li>Ничего не найдено</li>
                    </ul>    
                  </div>';      
    }
  }
  
?>