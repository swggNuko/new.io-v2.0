<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/EditElementsRelated.class.php');
  
  $req = array('success' => false, 'message' => '');
  
  try {
    $params = array();
    
    if (isset($_POST['field']) && !empty($_POST['field'])) {
      $params['others']['field'] = $_POST['field'];	    
    }
    
    $element = new Base\EditElementsRelated($_POST['table'], $params);
		
    $maxNumRecords = (isset($_POST['max_num_records']) ? $_POST['max_num_records'] : 3);
		
    $res = $element->searchRecords($_POST['value']);
    $req['message'] = $res;  
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
?>