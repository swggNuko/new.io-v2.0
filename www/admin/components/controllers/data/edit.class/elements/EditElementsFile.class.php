<?php
  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../../database/MysqlConnect.php');
  require_once(__DIR__ . '/EditElements.interface.php');
  require_once(__DIR__ . '/../EditDownloader.class.php');
  
  class EditElementsFile implements EditElementsInterface
  {
    private $connectDB_;
    
    private $params_;
    
    private $userId_;
    
    private $table_ = 'files';
    
    private $recordTable_;
    
    private $recordId_;
    
    private $file_;
    
    function __construct($table, $recordId, $params = array())
    {
      $this->params_ = $params;
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
      $this->userId_ = $GLOBALS['userId'];
      
      $this->recordTable_ = $table;
      
      $this->recordId_ = $recordId;
      
      $this->file_ = new EditDownloader($this->userId_);
    }
    
    //PUBLIC FUNCTION ZONE START
    
    public function printElement()
    {       
      switch ($this->params_['filetype']) {
        case 'image':
          try {
           $this->printImageElement();
          }
          catch(Exception $e) {
            echo $e->getMessage();	          
          }
          break;
          
        case 'pdf':
          $this->printPdfElement();
          break;
          
        default:
          throw new \Exception('Unknown type of file field');
          break;
      }
    }
    
    //PUBLIC FUNCTION ZONE END
    
    private function printPdfElement() {      
      /*NEW CODE START*/
      
      $caption = '';
      $copyright = '';
      $captionEn = '';
      $copyrightEn = '';
      $fileName = '';
      $itemDopInfImgClass = '';
      
      if ($this->params_['value'] == '') {
        echo '
          <div class="item__upload" id="'.$this->params_['key'].'">
            <label>
              <input type="file" class="pdf-downloader" name="'.$this->params_['key'].'" '.(($this->params_['disabled']) ? 'disabled' : '').'>
              <span id="'.$this->params_['key'].'_text">загрузить</span> 
              <img src="img/main-png/upload.png">
            </label>
            
            <div class="delete-pdf-now" id="'.$this->params_['key'].'_delete" data-hash="'.$this->params_['value'].'" data-table="'
              .$this->recordTable_.'" data-param="'.$this->params_['key'].'" style="display: none;" data-id-record="'.$this->recordId_.'"><img src="img/main-png/delete-img.png"></div>
              
          </div>
          
          <img src="img/main-png/pdf-logo.png" class="pdf-logo" style="display: none;"  id="' . $this->params_['key'] . '_pdf-logo">
          <a href="#" id="' . $this->params_['key'] . '_pdf" target="_blank" class="pdf-name" style="display: none;">huck-the-system.pdf</a>
        ';	      
      } else {
        
        $result=$this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` WHERE `hash` = "?s"', $this->params_['value']);
              
        if($result->getNumRows() > 0) {
          $row = $result->fetch_assoc();
          $caption = $row['description'];
          $copyright = $row['copyright'];
          $captionEn = $row['description_en'];
          $copyrightEn = $row['copyright_en'];
          $fileName = $row['name'];
        }
        
        echo '
          <div class="item__upload_new item__upload" id="'.$this->params_['key'].'">
            <label>
              <input type="file" class="pdf-downloader" name="'.$this->params_['key'].'" '.(($this->params_['disabled']) ? 'disabled' : '').'>
              <span id="'.$this->params_['key'].'_text">загрузить заново</span> 
              <img src="img/main-png/upload.png">
            </label>
            <div class="delete-pdf-now" id="'.$this->params_['key'].'_delete" data-hash="'.$this->params_['value'].'" data-table="'
              .$this->recordTable_.'" data-param="'.$this->params_['key'].'" data-id-record="'.$this->recordId_.'"><img src="img/main-png/delete-img.png"></div>
          </div>
                    
          <img src="img/main-png/pdf-logo.png" class="pdf-logo" id="' . $this->params_['key'] . '_pdf-logo">
          <a href="' . $this->file_->getPath($this->params_['value'], 'client') . $this->params_['value'] . '"  id="' . $this->params_['key'] . '_pdf" target="_blank" class="pdf-name"> ' . $fileName . ' </a>
        ';
        
        $itemDopInfImgClass = 'item__dop_img_inf_with_img';	      
      }
      
      echo '
        <img src="img/main/loader.gif" class="loader" id="'.$this->params_['key'].'_loader">
        <input type="hidden" name="'.$this->params_['key'].'_hash" id="'.$this->params_['key'].'_hash" value="'.$this->params_['value'].'">
        <div class="item__dop_img_inf ' . $itemDopInfImgClass . '" id="'.$this->params_['key'].'_item__dop_img_inf">
          <span class="item__time">Подпись</span>
          <input type="text"  name="'.$this->params_['key'].'_caption" 
            value="'.$caption.'" class="input__caption-title" '.(($this->params_['disabled']) ? 'disabled' : '').'>
                         
          <span class="item__time">Копирайт</span>
          <input type="text" name="'.$this->params_['key'].'_copyright" 
            value="'.$copyright.'" class="input__copyright-title" '.(($this->params_['disabled']) ? 'disabled' : '').'>
        </div>
      ';
      
      $linkStyle = 'style="display : none;"';
            
      
      if ($this->params_['value'] != '') {
    }
  }
    
    private function printImageElement() {
      $caption='';
      $copyright='';
      $captionEn='';
      $copyrightEn='';
      $dopItemInfClass='';
      
      if($this->params_['value'] == '') {
        echo '
          <div class="item__upload" id="'.$this->params_['key'].'">
            <label>
              <input type="file" class="image-downloader" name="'.$this->params_['key'].'" '.(($this->params_['disabled']) ? 'disabled' : '').'>
              <span id="'.$this->params_['key'].'_text">загрузить</span> 
              <img src="img/main-png/upload.png" />
             </label>
                     
            <div class="delete-img-now" id="'.$this->params_['key'].'_delete" data-hash="'.$this->params_['value'].'" data-table="'
              .$this->recordTable_.'" data-param="'.$this->params_['key'].'" data-id-record="'.$this->recordId_.'" style="display: none;" >
              <img src="img/main-png/delete-img.png"  />
            </div>
                     
          </div>';
                   
        echo '<img src="" style="display: none;"  id="'.$this->params_['key'].'_img" /><br/>';
              
        echo '<span class="open-resize-area" data-id="'.$this->params_['key'].'" data-miniatures="" style="margin-left: 144px; color: blue; cursor: pointer; display: none;" id="'.$this->params_['key'].'_change" data-src="">Изменить миниатюру изображения</span>';
            
        echo '<span class="send-resize-params" data-id="'.$this->params_['key'].'" style="margin-left: 144px; color: blue; cursor: pointer; display: none;"  id="'.$this->params_['key'].'_save-changes" data-hash="' . $this->params_['value'] . '">Сохранить изменения</span> <span class="cancel-resize-changes" data-id="'.$this->params_['key'].'" style="margin-left: 144px; color: blue; cursor: pointer; display: none;"  id="'.$this->params_['key'].'_cancel-changes">Отмена</span>';
              
      } else {
      
        $result=$this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` WHERE `hash` = "?s"', $this->params_['value']);
              
        if($result->getNumRows() > 0) {
          $row=$result->fetch_assoc();
          $caption=$row['description'];
          $copyright=$row['copyright'];
          $captionEn=$row['description_en'];
          $copyrightEn=$row['copyright_en'];
        }
        
        $dopItemInfClass='item__dop_img_inf_with_img';
        
        echo '
          <div class="item__upload_new item__upload" id="'.$this->params_['key'].'">
            <label>
              <input type="file" class="image-downloader" name="'.$this->params_['key'].'" '.(($this->params_['disabled']) ? 'disabled' : '').'>
              <span id="'.$this->params_['key'].'_text">загрузить заново</span> 
              <img src="img/main-png/upload.png"  />
            </label>
          
          <div class="delete-img-now" id="'.$this->params_['key'].'_delete" data-hash="'.$this->params_['value'].'" data-table="'
            .$this->recordTable_.'" data-param="'.$this->params_['key'].'" data-id-record="'.$this->recordId_.'">
            <img src="img/main-png/delete-img.png"  />
          </div>
        </div>';
        
        echo '<img src="'.$this->file_->getPath($this->params_['value'], 'client').$this->params_['value'].'"  id="'.$this->params_['key'].'_img" /><br/>';
              
        echo '<span class="open-resize-area" data-id="'.$this->params_['key'].'" data-miniatures="'.$this->file_->getSrcToMiniatures($this->params_['value']).'" style="margin-left: 144px; color: blue; cursor: pointer;" id="'.$this->params_['key'].'_change" data-src="' . $this->file_->getPath($this->params_['value'], 'client').$this->params_['value'] . '">Изменить миниатюру изображения</span>';
        
        echo '<span class="send-resize-params" data-id="'.$this->params_['key'].'" style="margin-left: 144px; color: blue; cursor: pointer; display: none;"  id="'.$this->params_['key'].'_save-changes" data-hash="' . $this->params_['value'] . '">Сохранить изменения</span> <span class="cancel-resize-changes" data-id="'.$this->params_['key'].'" style="margin-left: 144px; color: blue; cursor: pointer; display: none;"  id="'.$this->params_['key'].'_cancel-changes">Отмена</span>';
      }
            
      echo '<img src="img/main/loader.gif" class="loader" id="'.$this->params_['key'].'_loader" />';
      echo '<input type="hidden" name="'.$this->params_['key'].'_hash" id="'.$this->params_['key'].'_hash" value="'.$this->params_['value'].'">';
      echo '
        <div class="item__dop_img_inf '.$dopItemInfClass.'" id="'.$this->params_['key'].'_item__dop_img_inf">
          <span class="item__time">Подпись</span>
          <input type="text"  name="'.$this->params_['key'].'_caption" 
            value="'.$caption.'" class="input__caption-title" '.(($this->params_['disabled']) ? 'disabled' : '').'>
                         
          <span class="item__time">Копирайт</span>
          <input type="text" name="'.$this->params_['key'].'_copyright" 
            value="'.$copyright.'" class="input__copyright-title" '.(($this->params_['disabled']) ? 'disabled' : '').'>
        </div>';
    }
    
  }
?>