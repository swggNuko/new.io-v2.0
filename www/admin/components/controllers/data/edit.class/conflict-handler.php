<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('EditHandler.class.php');
  $record = new Base\EditHandler($GLOBALS['userId']);
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $req['message'] = $record->isConflictValue($_POST['table'], $_POST['key'], $_POST['value'], $_POST['id']);
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
  
?>