<?php

  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  
  class Groups
  {    
    private $userId_;
    
    private $connectDB_;
    
    private $table_ = 'groups';
    
    private $tableTypesGroups_ = 'type_groups';
    
    function __construct($userId)
    {        
      
      $this->userId_ = $userId;//Устанавливаем текущего пользователя
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
    }

    public function createGroup($name, $type, $elements) 
    {      
      if (count($elements) == 0) {
        throw new Exception('Передан пустой массив элементов');	      
      }
      
      $timeNow = time();
      
      $queryData=array(
        'id' => '',
        'name' => $name,
        'type_g' => $type,
        'elements' => json_encode($elements),
        'date_last_editing' => $timeNow,
        'date_creating' => $timeNow,
        'user_creating' => $this->userId_,
        'user_last_editing' => $this->userId_ 
      );
     
     
      $this->connectDB_->dbHandle->query('INSERT INTO `' . $this->table_ . '` SET ?As', $queryData);	    
    }


    public function handleElements($groupId, $elements, $action = 'add')
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` WHERE `id` = "?i"', $groupId);
      
      if ($res->getNumRows() != 1) {	      
        throw new Exception('Группа с id=' . $groupId . ' не существует');
      }
      
      if (count($elements) == 0) {
        throw new Exception('Передан пустой массив элементов');	      
      }
      
      $row = $res->fetch_assoc();
      
      if ($action == 'add') {
        
        $dbElements = json_decode($row['elements'], true);
        $dbElements = is_array($dbElements) ? $dbElements : array();
        
        $elements = array_merge($this->my_array_diff($dbElements, $elements), $elements);
        
      } elseif($action == 'delete') {
        $dbElements = json_decode($row['elements'], true);
        $dbElements = is_array($dbElements) ? $dbElements : array();
        
        $elements = $this->my_array_diff($dbElements, $elements);	      
      } else {
        throw new Exception('Неизвестное действие ' . $action . ' с элементами группы');	      
      }
      
      $jsonElements = json_encode($elements);
      
      $this->connectDB_->dbHandle->query('UPDATE `' . $this->table_ . '` SET `elements` = "?s", `date_last_editing` = "?i", `user_last_editing` = "?i" WHERE `id` = "?i"', $jsonElements, time(), $this->userId_, $groupId);
      
    }
    

    public function getGroupsTypes()
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM ?f', $this->tableTypesGroups_);
      
      $returnArray = array();
      
      while ($row = $res->fetch_assoc()) {
        array_push($returnArray, array('id' => $row['id'], 'name' => $row['name']));	      
      }
      
      return $returnArray;
    }
    
    public function getGroupsForRecord($table, $recordId) {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM ?f WHERE `elements` LIKE "%?s%"', $this->table_, json_encode(array($table, $recordId)));	      
      $groups = array();
      
      while ($row = $res->fetch_assoc()) {
        array_push($groups, $row['id']); 	      
      }
      
      return $groups;
    }
    
    //PUBLIC FUNCTION ZONE END
    
    private function my_array_diff($arr1, $arr2)
    {
      $index = array();
      
      for ($i = 0; $i < count($arr1); $i++) {
        for ($j = 0; $j < count($arr2); $j++) {
          if ($arr1[$i] == $arr2[$j]) {
            array_push($index, $i);	          
          }
        }
      }
      
      foreach ($index as $i) {
        unset($arr1[$i]);	      
      }
      
      $arr1 = array_values($arr1);
      
      return $arr1;	    
    }
  }
?> 