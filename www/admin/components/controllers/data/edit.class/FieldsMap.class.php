<?php

  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  
  class FieldsMap
  {    
    private $userId_;
    
    private $connectDB_;
    
    private $table_ = 'view_fields';
    
    private $userTable_;
    
    private $fdd_;
    
    function __construct($userId, $userTable, $fdd = array())
    {        
      
      $this->userId_ = $userId;
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
      $this->userTable_ = $userTable;
      
      $this->fdd_ = $fdd;
      
    }
    
    //PUBLIC FUNCTION ZONE START
    
    public function getDisplayFields() 
    {
      $displayFields = array_keys($this->fdd_);
      
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` WHERE `user_id` = "?i" AND `table_name` = "?s"', $this->userId_, $this->userTable_);
      
      if ($res->getNumRows() == 0) {
      
        $i = 0;
      
        foreach($this->fdd_ as $key) {      
          if(!$key['sort'] || strpos($displayFields[$i], '_en')) {
            unset($displayFields[$i]);
            $i++;
            continue;
          }
        
          $i++;
        }
      } else {
        $row = $res->fetch_assoc();
        $displayFields = json_decode($row['fields'], true);
      }
      
      return $displayFields;
    }
    
    public function setDisplayFields($displayFields) 
    {
      if (count($displayFields) == 0) {
        throw new \Exception('Нужно выбрать хотя бы один столбец для отображения!');	      
      }
      
      $jsonDisplayFields = json_encode($displayFields);
      
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` WHERE `user_id` = "?i" AND `table_name` = "?s"', $this->userId_, $this->userTable_);
      
      if ($res->getNumRows() == 0) {
        $queryData=array(
        'id' => '',
        'user_id' => $this->userId_,
        'table_name' => $this->userTable_,
        'fields' => $jsonDisplayFields
        );
    
        $this->connectDB_->dbHandle->query('INSERT INTO `' . $this->table_ . '` SET ?As', $queryData);
      } else {
        $this->connectDB_->dbHandle->query('UPDATE `' . $this->table_ . '` SET `fields` = "?s" WHERE `user_id` = "?i" AND `table_name` = "?s"', $jsonDisplayFields, $this->userId_, $this->userTable_);
      }
    }
    
    public function getAllowFields()
    {
      $allowFields = array();
      
      $fields = array_keys($this->fdd_);
           
       $i = 0;
      
      $displayFields = $this->getDisplayFields();
      
       foreach($this->fdd_ as $key) {      
        if($key['display-in-table']) {
					
					$arraySearchItem = array_search($fields[$i], $displayFields);
					$displayItem = ($arraySearchItem !== false) ? true : false;
					$priorityItem = ($displayItem) ? ($arraySearchItem + 1) : 0;
					
          $insertArray = array(
            'id' => $fields[$i], 
            'name' => $key['name'],
            'display' => $displayItem,
						'priority' => $priorityItem
          );
          
          array_push($allowFields, $insertArray);
        }
        
        $i++;
       }
      
      return $allowFields;  
      
    }
    
    //PUBLIC FUNCTION ZONE END
    
  }
?> 