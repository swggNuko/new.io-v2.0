<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('FieldsMap.class.php');
  
  $fields = new Base\FieldsMap($GLOBALS['userId'], $_POST['table']);
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $fields->setDisplayFields($_POST['fields']);
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
  
?>