<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/Groups.class.php');
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $group = new Base\Groups($GLOBALS['userId']);
    $action = $_POST['action'];
    
    $elements = array();
    
    $table = $_POST['table'];
    $elementsId = json_decode($_POST['elementsId'], true);
    
    foreach ($elementsId as $elementId) {
      array_push($elements, array($table, $elementId));	    
    }
    		
    if ($action == 'add') {
      //Add
      $group->handleElements($_POST['groupId'], $elements, 'add');	    
    } elseif($action == 'create') {
      //Create and Add
      $group->createGroup($_POST['name'], $_POST['type'], $elements);	    
    } elseif($action == 'delete') {
      $group->handleElements($_POST['groupId'], $elements, 'delete');
    } else {
      throw new Exception('Неизвестное действие');	    
    }
      
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
?>