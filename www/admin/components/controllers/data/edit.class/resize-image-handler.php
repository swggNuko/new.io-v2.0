<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('EditDownloader.class.php');
  $image = new Base\EditDownloader($GLOBALS['userId']);
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $image->resizeImage(
      $_POST['x1'], 
      $_POST['y1'], 
      $_POST['x2'], 
      $_POST['y2'], 
      $_POST['width'], 
      $_POST['height'], 
      $_POST['hash']
      );
      
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
  
?>