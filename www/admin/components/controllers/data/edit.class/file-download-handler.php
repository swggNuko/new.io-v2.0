<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('EditDownloader.class.php');
  $userId = $GLOBALS['userId'];
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $downloadFile = new Base\EditDownloader(
      $userId, 
      $_POST['input_name'], 
      $_POST['action'], 
      $_POST['record_id'], 
      $_POST['table'], 
      $_POST['filetype']
    );
    
    $req['message'] = $downloadFile->getHashName();
    $req['path'] = $downloadFile->getPath($req['message'], 'client');
    
    switch ($downloadFile->getType()) {
      case 'jpg':
        $req['miniatures'] = 'F350-350_' . $downloadFile->getMD5HashForMiniatures($req['message']) . '.jpg';
        break;
      case 'pdf':
        $req['filename'] = $downloadFile->getOriginalName();
        break;
      default:
        break;	    
    }
    
    
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
?>