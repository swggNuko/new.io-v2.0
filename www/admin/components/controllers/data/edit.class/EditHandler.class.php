<?php

  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  
  class EditHandler
  {    
    private $userId_;
    
    private $connectDB_;
    
    function __construct($userId)
    {        
      
      $this->userId_ = $userId;
      
      $this->connectDB_ = new \Base\MysqlConnect();
      
    }
    
    //PUBLIC FUNCTION ZONE START
    
    public function updateRecord($table, $key, $id, $content) 
    {
      $this->connectDB_->dbHandle->query('UPDATE `'.$table.'` SET `' . $key . '` = "?s", `user_last_editing` = "?i", `date_last_editing` = "?i", `basket` = "0" WHERE `'.$table.'`.`id` = ?i', $content, $this->userId_, time(), $id);
      
      return $content;
    }
    
    public function isConflictValue($table, $key, $value, $id)
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $table . '` WHERE `' . $key . '` = "?s"', $value);
      
      if ($res->getNumRows() > 0) {
        $row = $res->fetch_assoc();
                if ($row['id'] != $id) {  return true;              }            }
              return false;
    }
    
    //PUBLIC FUNCTION ZONE END
    
  }
?>