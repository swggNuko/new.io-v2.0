<?php

  namespace Base;
  
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  require_once(__DIR__ . '/../../../lib/class.upload.php/class.upload.php');
  
  define('LIMIT_BYTES', 1024 * 1024 * 10);
  define('IMAGE_WIDTH_ADMIN', 600);
  
  class EditDownloader
  {    
    private $userId_;
    
    private $connectDB_;
    
    private $file_; //key in $_FILES
    private $fileName_;
    private $filePath_;
    private $errorCode_;
    private $fileType_;
    
    private $action_;
    private $idRecord_;
    private $table_;
    
    private $clientUploadsPath_;
    private $uploadsPath_;
    private $uploadsPathFull_;
    
    private $year_;
    private $month_;
    private $day_;
    private $dirDate_;
    
    private $hash_;
    
    private $type_;
    private $fileTypeAllow_;
    
    
    function __construct($userId, $file = '', $action = '', $idRecord = '', $table = '', $filetype = '')
    {        
      
      $this->userId_ = $userId;
      $this->connectDB_ = new \Base\MysqlConnect();
      
      include(__DIR__ . '/../../../config.php');
      
      $this->uploadsPath_ = __DIR__ . '/../../../../' . $opts['uploadsPath'];
      $this->uploadsPathFull_ = $opts['uploadsPathFull'];
      $this->clientUploadsPath_ = '../' . $opts['uploadsPath'];
      $this->fileTypeAllow_ = $filetype;
      
      
      if (!empty($file)) {
        $this->initFile($file, $action, $idRecord, $table);
      }
      
      
    }
    
    private function initFile($file, $action, $idRecord, $table)
    {
      $this->file_ = $file;
      $this->action_ = $action;
      $this->idRecord_ = $idRecord;
      $this->table_ = $table;
      
      $this->fileName_ = $_FILES[$this->file_]['name'];  
      $this->filePath_  = $_FILES[$this->file_]['tmp_name'];
      $this->errorCode_ = $_FILES[$this->file_]['error'];
      $this->fileType_ = $_FILES[$this->file_]['type'];
      
      if($this->filePath_ == '') return 0;
      $this->year_ = date('Y', time());
      $this->month_ = date('m', time());
      $this->day_ = date('d', time());
      
      $this->dirDate_ = array(
        0 => $this->year_,
        1 => $this->month_,
        2 => $this->day_
       );
       
      $this->createDirectory($this->dirDate_);
      
      if ($this->errorCode_ !== UPLOAD_ERR_OK || !is_uploaded_file($this->filePath_)) {
           
          $errorMessages = array(
              UPLOAD_ERR_INI_SIZE   => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
              UPLOAD_ERR_FORM_SIZE  => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
              UPLOAD_ERR_PARTIAL    => 'Загружаемый файл был получен только частично.',
              UPLOAD_ERR_NO_FILE    => 'Файл не был загружен.',
              UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
              UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
              UPLOAD_ERR_EXTENSION  => 'PHP-расширение остановило загрузку файла.',
          );
          
          $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';
          
          $outputMessage = isset($errorMessages[$this->errorCode_]) ? $errorMessages[$this->errorCode_] : $unknownMessage;
          
          //return array('result' => 'failed', 'message' => $outputMessage);
          
          throw new \Exception($outputMessage);
      }
      
      $fi = finfo_open(FILEINFO_MIME_TYPE);
    
      $mime = (string) finfo_file($fi, $this->filePath_);
    
      finfo_close($fi);
      
      
      if (strpos($mime, 'image') !== false && $this->fileTypeAllow_ == 'image') {
        $this->type_ = 'jpg';
        $this->downloadImage();
      } else if ($mime == 'application/pdf' && $this->fileTypeAllow_ == 'pdf') {
        $this->type_ = 'pdf';
        $this->downloadPdf();          
      } else {
        throw new \Exception('Недопустимый формат файла. ');        
      }      
    }
    
    //PUBLIC FUNCTION ZONE START
    
    public function getType() {
      return $this->type_;      
    }
    
    public function getSrcToMiniatures($value, $miniatureSize = 'F350-350')
    {
      $mainImagePath = $this->getPath($value, 'client').$value;
      
      $miniaturesImagePath = $this->getPath($value, 'client') . $miniatureSize . '_' . $this->getMD5HashForMiniatures($value) . '.jpg';
      
      if (file_exists(__DIR__ .'/../../../' . $miniaturesImagePath))  {
        return $miniaturesImagePath;
      } else {
        return $mainImagePath;
      }
    }
    
    public function getHashName()
    {
      return $this->hash_;      
    }
    
    public function getOriginalName()
    {
      return $this->fileName_;	    
    }
    
    public function resizeImage($x1, $y1, $x2, $y2, $w, $h, $hash)
    {
     if (empty($x1) || empty($y1) || empty($x2) || empty($y2) || empty($w) || empty($h) || empty($hash)) {
       throw new Exception('Недостаточно аргументов. Свяжитесь с разработчиком для решения проблемы.');       
     }
     
     $path = $this->getPath($hash);   
    
     $filename = $path . $hash;
     
     $new_filename = $path . 'F350-350_' . $this->getMD5HashForMiniatures($hash) . '.jpg';

     list($current_width, $current_height) = getimagesize($filename);
     

     $coef = ($current_width - IMAGE_WIDTH_ADMIN) / IMAGE_WIDTH_ADMIN;
     $x1 *= (1 + $coef);
     $y1 *= (1 + $coef);
     $x2 *= (1 + $coef);
     $y2 *= (1 + $coef);
     $w *= (1 + $coef);
     $h *= (1 + $coef);

     $crop_width = 350;
     $crop_height = 350;


     $new = imagecreatetruecolor($crop_width, $crop_height);

     $current_image = imagecreatefromjpeg($filename);

     imagecopyresampled($new, $current_image, 0, 0, $x1, $y1, $crop_width, $crop_height, $w, $h);

     imagejpeg($new, $new_filename, 95);
      
    }
    
    
    public function setImage($hash)
    {
      if (empty($hash)) {
        throw new Exception('Получен пустой хэш изображения');        
      }
      
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `files` WHERE `hash` = "?s"', $hash);
      
      if ($res->getNumRows() != 1) {
        throw new Exception('Изображение не найдено');       
      }
     
      $row=$res->fetch_assoc();
      
      $this->year_=date('Y', $row['date_creating']);
      $this->month_=date('m', $row['date_creating']);
      $this->day_=date('d', $row['date_creating']);
      
      $this->hash_ = $hash;     
    }
    
    public function createImageMiniatures()
    {
      $path = $this->uploadsPath_ . '/' . $this->year_ . '/' . $this->month_ . '/' . $this->day_ . '/';
      $originalImage = $path . $this->hash_;  
      $handle = new \upload($originalImage); 
      
      /*
        F - fixed
        R - relative
      */
      
      $onlyHash = $this->getMD5HashForMiniatures($this->hash_);
      
      $handle->file_new_name_body = 'F350-350_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_crop = 'L';
      $handle->image_y = 350;
      $handle->image_x = 350; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image. ' . $handle->error);        
      }
      
      $handle->file_new_name_body = 'R350-350_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_y = true;
      $handle->image_x = 350; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');        
      }
      
      $handle->file_new_name_body = 'R975-975_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_y = true;
      $handle->image_x = 975; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');        
      }
      
      
    }
    
    public function getMD5HashForMiniatures($hash)
    {
     $formats = array('.jpg');
     
     return md5(str_replace($formats, '', $hash));      
    }
    
    public function getPath($hash, $pathType = 'relative') 
    {  
      if($hash == '') return 0;
          
      $resultImg = $this->connectDB_->dbHandle->query('SELECT * FROM `files` WHERE `hash` = "?s"', $hash);
      
      $rowImg=$resultImg->fetch_assoc();
      
      $year=date('Y', $rowImg['date_creating']);
      $month=date('m', $rowImg['date_creating']);
      $day=date('d', $rowImg['date_creating']);
      
      if($pathType == 'relative') {
        return $this->uploadsPath_.'/'.$year.'/'.$month.'/'.$day.'/';
      } elseif($pathType == 'client') {
        return $this->clientUploadsPath_.'/'.$year.'/'.$month.'/'.$day.'/';      
      } else {
        return $this->uploadsPathFull_.'/'.$year.'/'.$month.'/'.$day.'/';
      }
      
    }
    
    //PUBLIC FUNCTION ZONE END
    
    //FOR PDF START
    private function downloadPdf()
    {    
      if (filesize($this->filePath_) > LIMIT_BYTES) {
        throw new \Exception('Размер файла не должен превышать 10 Мбайт.');
      }
    
      $name = md5($this->filePath_.time().rand());
          
      $format = '.pdf';
    
      if (!move_uploaded_file($this->filePath_, $this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format)) {
        throw new \Exception('При записи файла на диск произошла ошибка.');
      }  
      
      
      
      if($this->action_ == 'deleteOld') $this->deleteFile($this->file_, $this->idRecord_);
    
      $queryData=array(
        'id' => '',
        'name' => $this->fileName_,
        'hash' => $name.$format,
        'type' => $format,
        'protected' => '0',
        'copyright' => $_POST[$this->file_.'_copyright'],
        'description' => $_POST[$this->file_.'_caption'],
        'copyright_en' => $_POST[$this->file_.'_copyright_en'],
        'description_en' => $_POST[$this->file_.'_caption_en'],
        'user_creating' => $this->userId_,
        'user_last_editing' => $this->userId_,
        'date_last_editing' => time(),
        'date_creating' => time(),
        'basket' => '',
        'basket_id' => ''
      );
    
      $this->connectDB_->dbHandle->query('INSERT INTO `files` SET ?As', $queryData);
        
      $this->hash_ = $name.$format;
        
    }
    //FOR PDF END
      
    //FOR IMAGE START
    private function downloadImage()
    {
      $image = getimagesize($this->filePath_);
    
      if (filesize($this->filePath_) > LIMIT_BYTES) {
        throw new \Exception('Размер изображения не должен превышать 10 Мбайт.');
      }

    
      $name = md5($this->filePath_.time().rand());
    
      $extension = image_type_to_extension($image[2]);
      
      $formats = array('jpeg');
          
      $format = str_replace($formats, 'jpg', $extension);
    
      if (!move_uploaded_file($this->filePath_, $this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format)) {
        throw new \Exception('При записи изображения на диск произошла ошибка.');
      }  
      
      if ($this->fileType_ == 'image/gif') {
        $img = imagecreatefromgif($this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format);
        imagejpeg($img, $this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.'.jpg');
        unlink($this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format);
      } elseif($this->fileType_ == 'image/png') {
        $img = imagecreatefrompng($this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format);
        imagejpeg($img, $this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.'.jpg');
        unlink($this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format);        
      } elseif($this->fileType_ == 'image/jpeg') {
        $img = imagecreatefrompng($this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format);
        imagejpeg($img, $this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.'.jpg');
        
        if ($format != '.jpg') {
          unlink($this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format);
        }
      }
     
      
      $format = '.jpg';
      
      
      if($this->action_ == 'deleteOld') $this->deleteFile($this->file_, $this->idRecord_);
    
      $queryData=array(
        'id' => '',
        'name' => $this->fileName_,
        'hash' => $name.$format,
        'type' => $format,
        'protected' => '0',
        'copyright' => $_POST[$this->file_.'_copyright'],
        'description' => $_POST[$this->file_.'_caption'],
        'copyright_en' => $_POST[$this->file_.'_copyright_en'],
        'description_en' => $_POST[$this->file_.'_caption_en'],
        'user_creating' => $this->userId_,
        'user_last_editing' => $this->userId_,
        'date_last_editing' => time(),
        'date_creating' => time(),
        'basket' => '',
        'basket_id' => ''
      );
    
    $this->connectDB_->dbHandle->query('INSERT INTO `files` SET ?As', $queryData);
        
    $this->hash_ = $name.$format;
    
    $this->createImageMiniatures();      
    }
    
    public function deleteMiniatures($hash)
    {
      
      $path = $this->getPath($hash);
      $onlyHash = $this->getMD5HashForMiniatures($hash);
      
      unlink($path . 'F350-350_' . $onlyHash . '.jpg');
      unlink($path . 'R350-350_' . $onlyHash . '.jpg');
      unlink($path . 'R975-975_' . $onlyHash . '.jpg');      
    }
    //FOR IMAGE END
    
    private function createDirectory($dirDate) 
    {
      $location=$this->uploadsPath_;
      
      for($i=0; $i < 3; $i++) {
        $location.='/'.$dirDate[$i];
        if(!is_dir($location)) mkdir($location);  
      }
            
    }
    
    private function deleteFile($file, $idRecord) 
    {
      $result=$this->connectDB_->dbHandle->query('SELECT * FROM `'.$this->table_.'` WHERE `id` = ?i', $idRecord);
      
      $row=$result->fetch_assoc();  
      
      if($row[$file] != '') {
        unlink($this->getPath($row[$file]).$row[$file]);
        $this->connectDB_->dbHandle->query('DELETE FROM `files` WHERE `files`.`hash` = "?s"', $row[$file]);  
      }
    }
    
  }
?>