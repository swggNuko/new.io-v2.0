<?php

 
  
  require_once(__DIR__ . '/../../../database/MysqlConnect.php');
  require_once(__DIR__ . '/../../../lib/class.upload.php/class.upload.php');
  
  class EditDownloader
  {    
    private $userId_;
    
    private $connectDB_;
    
    private $file_; //key in $_FILES
    private $fileName_;
    private $filePath_;
    private $errorCode_;
    
    private $action_;
    private $idRecord_;
    private $table_;
    
    private $uploadsPath_;
    private $uploadsPathFull_;
    
    private $year_;
    private $month_;
    private $day_;
    private $dirDate_;
    
    private $hash_;
    
    function __construct()
    {        
      $this->connectDB_ = new \Base\MysqlConnect();
      
      include(__DIR__ . '/../../../config.php');
      
      $this->uploadsPath_ = __DIR__ . '/../../../../' . $opts['uploadsPath'];
      $this->uploadsPathFull_ = $opts['uploadsPathFull'];
      
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `files` WHERE `id` > "?i"', $_GET['id']);
      
			$counter = 0;
			
      while ($row = $res->fetch_assoc($res)) {
      $counter++;
			
			if ($counter > 40) {
				
				echo '<div><strong>Program end. Exit.</strong></div>';
				break;
			}
			
			  
      $path = $this->getPath($row['hash']);
      $originalImage = $path . $row['hash'];
      
      echo $path . '<br>';
      	
      $handle = new \upload($originalImage); 
      
      $onlyHash = $this->getMD5HashForMiniatures($row['hash']);
			
			if (!file_exists($originalImage)) {
				echo '<div>' . $originalImage . ' <strong>Not found</strong></div>';
				continue;
			}
			
			if (file_exists($path . 'F350-350_' . $onlyHash)) {
				echo '<div>Miniature exhists. <strong>Continue.</strong></div>';
				continue;
			}
        
      $handle->file_new_name_body = 'F350-350_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_crop = 'L';
      $handle->image_y = 350;
      $handle->image_x = 350; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');	      
      }
      
      $handle->file_new_name_body = 'R350-350_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_y = true;
      $handle->image_x = 350; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');	      
      }
      
      $handle->file_new_name_body = 'R975-975_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_y = true;
      $handle->image_x = 975; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');	      
      }
      
      
        echo $onlyHash . ' - DONE<br>';	      
      }
      
     
      
      
    }
    
    private function initFile($file, $action, $idRecord, $table)
    {
      $this->file_ = $file;
      $this->action_ = $action;
      $this->idRecord_ = $idRecord;
      $this->table_ = $table;
      
      $this->fileName_ = $_FILES[$this->file_]['name'];  
      $this->filePath_  = $_FILES[$this->file_]['tmp_name'];
      $this->errorCode_ = $_FILES[$this->file_]['error'];
      
      if($this->filePath_ == '') return 0;
      
      $this->year_=date('Y', time());
      $this->month_=date('m', time());
      $this->day_=date('d', time());
      
      $this->dirDate_=array(
        0 => $year,
        1 => $month,
        2 => $day
       );
       
      $this->createDirectory($this->dirDate);
      
      if ($this->errorCode_ !== UPLOAD_ERR_OK || !is_uploaded_file($this->filePath_)) {
           
          $errorMessages = array(
              UPLOAD_ERR_INI_SIZE   => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
              UPLOAD_ERR_FORM_SIZE  => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
              UPLOAD_ERR_PARTIAL    => 'Загружаемый файл был получен только частично.',
              UPLOAD_ERR_NO_FILE    => 'Файл не был загружен.',
              UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
              UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
              UPLOAD_ERR_EXTENSION  => 'PHP-расширение остановило загрузку файла.',
          );
          
          $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';
          
          $outputMessage = isset($errorMessages[$this->errorCode_]) ? $errorMessages[$this->errorCode_] : $unknownMessage;
          
          //return array('result' => 'failed', 'message' => $outputMessage);
          
          throw new \Exception($outputMessage);
      }
      
      $fi = finfo_open(FILEINFO_MIME_TYPE);
    
      $mime = (string) finfo_file($fi, $this->filePath_);
    
      finfo_close($fi);
      
      if (strpos($mime, 'image') !== false) {
        $this->downloadImage();
      } else {
        throw new \Exception('Недопустимый формат файла.');	      
      }	    
    }
    
    //PUBLIC FUNCTION ZONE START
    
    public function getHashName()
    {
      return $this->hash_;	    
    }
    
    public function resizeImage($x1, $y1, $x2, $y2, $w, $h, $hash)
    {
     if (empty($x1) || empty($y1) || empty($x2) || empty($y2) || empty($w) || empty($h) || empty($hash)) {
       throw new Exception('Недостаточно аргументов. Свяжитесь с разработчиком для решения проблемы.');	     
     }
     
     $path = $this->getPath($hash); 	
      

     $filename = $path . $hash;
     
     $new_filename = $path . 'F350-350_' . $this->getMD5HashForMiniatures($hash) . '.jpg';

     list($current_width, $current_height) = getimagesize($filename); 


     $crop_width = 350;
     $crop_height = 350;

     $new = imagecreatetruecolor($crop_width, $crop_height);

     $current_image = imagecreatefromjpeg($filename);
     imagecopyresampled($new, $current_image, 0, 0, $x1, $y1, $crop_width, $crop_height, $w, $h);
     imagejpeg($new, $new_filename, 95);
	    
    }
    
    //PUBLIC FUNCTION ZONE END
      
    //FOR IMAGE START
    private function downloadImage()
    {
      $image = getimagesize($this->filePath_);
    
      if (filesize($this->filePath_) > LIMIT_BYTES) {
        throw new \Exception('Размер изображения не должен превышать 10 Мбайт.');
      }

    
      $name = md5($this->filePath_.time().rand());
    
      $extension = image_type_to_extension($image[2]);
    
      $format = str_replace('jpeg', 'jpg', $extension);
    
      if (!move_uploaded_file($this->filePath_, $this->uploadsPath_.'/'.$this->year_.'/'.$this->month_.'/'.$this->day_.'/'.$name.$format)) {
        throw new \Exception('При записи изображения на диск произошла ошибка.');
      }  
    
      if($this->action_ == 'deleteOld') $this->deleteFile($this->file_, $this->idRecord_);
    
      $queryData=array(
        'id' => '',
        'name' => $this->fileName_,
        'hash' => $name.$format,
        'type' => $format,
        'protected' => '0',
        'copyright' => $_POST[$this->file_.'_copyright'],
        'description' => $_POST[$this->file_.'_caption'],
        'copyright_en' => $_POST[$this->file_.'_copyright_en'],
        'description_en' => $_POST[$this->file_.'_caption_en'],
        'user_creating' => $this->userId_,
        'user_last_editing' => $this->userId_,
        'date_last_editing' => time(),
        'date_creating' => time(),
        'basket' => '',
        'basket_id' => ''
      );
    
    $this->connectDB_->dbHandle->query('INSERT INTO `files` SET ?As', $queryData);
        
    $this->hash_ = $name.$format;
    
    $this->createImageMiniatures();	    
    }
    
    private function createImageMiniatures()
    {
      $path = $this->uploadsPath_ . '/' . $this->year_ . '/' . $this->month_ . '/' . $this->day_ . '/';
      $originalImage = $path . $this->hash_;	
      $handle = new \upload($originalImage); 
      
      
      $onlyHash = $this->getMD5HashForMiniatures($this->hash_);
      
      $handle->file_new_name_body = 'F350-350_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_crop = 'L';
      $handle->image_y = 350;
      $handle->image_x = 350; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');	      
      }
      
      $handle->file_new_name_body = 'R350-350_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_y = true;
      $handle->image_x = 350; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');	      
      }
      
      $handle->file_new_name_body = 'R975-975_' . $onlyHash;
      $handle->image_resize = true;
      $handle->image_ratio_y = true;
      $handle->image_x = 975; 
      $handle->process($path);
      if (!$handle->processed) {
        throw new \Exception('Some problem with resizing image');	      
      }
      
      
    }
    //FOR IMAGE END
    
    private function getMD5HashForMiniatures($hash)
    {
     return md5(str_replace('.jpg', '', $hash));	    
    }
    
    private function createDirectory($dirDate) 
    {
      $location=$this->uploadsPath_;
      
      for($i=0; $i < 3; $i++) {
        $location.='/'.$dirDate[$i];
        if(!is_dir($location)) mkdir($location);  
      }
            
    }
    
    private function deleteFile($file, $idRecord) 
    {
      $result=$this->connectDB_->dbHandle->query('SELECT * FROM `'.$this->table_.'` WHERE `id` = ?i', $idRecord);
      
      $row=$result->fetch_assoc();  
      
      if($row[$file] != '') {
        unlink($this->getPath($row[$file]).$row[$file]);
        $this->connectDB_->dbHandle->query('DELETE FROM `files` WHERE `files`.`hash` = "?s"', $row[$file]);  
      }
    }
    
    
    protected function getPath($hash, $pathType = 'relative') 
    {  
      if($hash == '') return 0;
          
      $resultImg = $this->connectDB_->dbHandle->query('SELECT * FROM `files` WHERE `hash` = "?s"', $hash);
      
      $rowImg=$resultImg->fetch_assoc();
      
      $year=date('Y', $rowImg['date_creating']);
      $month=date('m', $rowImg['date_creating']);
      $day=date('d', $rowImg['date_creating']);
      
      if($pathType == 'relative') {
        return $this->uploadsPath_.'/'.$year.'/'.$month.'/'.$day.'/';
      } else {
        return $this->uploadsPathFull_.'/'.$year.'/'.$month.'/'.$day.'/';
      }
      
    }
    
  }
  
  new EditDownloader();
?>