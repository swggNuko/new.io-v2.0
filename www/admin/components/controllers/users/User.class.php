<?php
  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once(__DIR__ . '/../../database/MysqlConnect.php');
  
  class User
  {
    
    private $connectDB_;
    private $table_ = 'users';
    private $tableRecentMovement_ = 'recent_movements';
    
    private $userId_;

    private $validFields_ = array(
      'name',
      'surname',
      'patronymic',
      'e_mail',
      'post',
      'password',
      'site_disabled_edit'
    );
    
    function __construct($userId = 0)
    {
      $this->connectDB_ = new \Base\MysqlConnect();
      
      $this->userId_ = $userId;
      
    }
    
    //PUBLIC FUNCTIONS ZONE START
    
    public function checkUser($hash)
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` 
         WHERE `hash`="?s"', $hash);
       
      if ($res->getNumRows() == 1) {
        $row = $res->fetch_assoc();
        $this->userId_ = $row['id'];
        return true;
      }
      
      return false;
            
    }

    public function resetPassword($login)
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` WHERE `login` = "?s"', $login);
      
      if ($res->getNumRows() == 0) {
        throw new Exception('Пользователь ' . $login . ' не найден');	      
      }
      
      //generate password START
      $newPassword = '';
      
      $arr = array('a','b','c','d','e','f',
                 'g','h','i','j','k','l',
                 'm','n','o','p','r','s',
                 't','u','v','x','y','z',
                 'A','B','C','D','E','F',
                 'G','H','I','J','K','L',
                 'M','N','O','P','R','S',
                 'T','U','V','X','Y','Z',
                 '1','2','3','4','5','6',
                 '7','8','9','0');
    
       for($i = 0; $i < 8; $i++) {
         $index = rand(0, count($arr) - 1);
         $newPassword .= $arr[$index];
       }
      //generate password END
      
      $row = $res->fetch_assoc();
      
      require_once(__DIR__ . '/../../lib/PHPMailer/FreeMailer.php');
  
      $mailer = new FreeMailer();
      $mailer->CharSet = "utf-8";
      $mailer->isHTML(true);
      $mailer->Subject = 'ИКСИС:Восстановление пароля';
      $mailer->AddAddress($row['e_mail'], ($row['surname'] . ' ' . $row['name']));
      
      $message = 'Доброго времени суток, ' . $row['name'] . '!<br>';
      $message .= 'Ничего страшного, что Вы забыли пароль. Мы сгенерировали Вам новый: ' . $newPassword . ' .<br>';
      $message .= 'Пользуйтесь на здоровье!<br><br>';
      $message .= 'С глубоким уважением и любовью, <br>Ваш ИКСИС';
      
      $mailer->Body = $message;
      
      require(__DIR__ . '/../../config.php');
      
      $mailer->From = 'noreply@' . $opts['domen'];
      $mailer->FromName = $opts['site_name'];
       
      if(!$mailer->Send())
      {
        throw new Exception('Проблемы с отправкой сообщения. Попробуйте ещё раз немного позже.');
      }
      
      $this->connectDB_->dbHandle->query('UPDATE `' . $this->table_ .'` SET `password` = "?s" WHERE `' . $this->table_. '`.`id` = "?i";', md5(md5($newPassword)), $row['id']);
      
      return $row['e_mail'];
    }
    
    public function logInControl($login, $password) 
    {
       
       $password = md5(md5($password));
       
       $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` 
         WHERE `login`="?s" AND `password`="?s"', $login, $password);
       
       $hash = '0';  
       
       if ($res->getNumRows() == 1) {
         $row = $res->fetch_assoc();
         $this->userId_ = $row['id'];
         
         $hash = md5(md5($this->userId_ . $login . $password . time()));
         
         $this->connectDB_->dbHandle->query('UPDATE `' . $this->table_ . '` 
           SET `hash` = "?s" WHERE `'.$this->table_.'`.`id` = ?i', $hash, $this->userId_);
       }
       
       $this->checkRecentMovementsRecord();
       
       return $hash;
    
    }
    

    public function updateCurrentUserData($field, $value)
    {
      $this->updateUserData($this->userId_, $field, $value);
    }
    

    public function updateUserData($userId, $field, $value)
    {
      if (!in_array($field, $this->validFields_)) {
        throw new Exception('Unknown field ' . $field);	      
      }
      
      if ($field == 'password') {
        $value = md5(md5($value));	      
      }
      
      $this->connectDB_->dbHandle->query('UPDATE `'.$this->table_.'` SET `' . $field . '` = "?s" WHERE `'.$this->table_.'`.`id` = ?i', $value, $userId);
    }

    public function getCurrentUserData() 
    {
      return $this->getUserData($this->userId_);
    }
    

    public function getUserData($userId)
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` 
         WHERE `id`="?i"', $userId);
      
      if ($res->getNumRows() == 1){
        $row = $res->fetch_assoc();
        
        $userData = array();
        
        $userData['name'] = $row['name'];
        $userData['surname'] = $row['surname'];
        $userData['patronymic'] = $row['patronymic'];
        $userData['post'] = $row['post'];
        $userData['rights'] = $row['rights'];
        $userData['login'] = $row['login'];
        $userData['e_mail'] = $row['e_mail'];
        $userData['site_disabled_edit'] = $row['site_disabled_edit'];
        
        switch ($userData['rights']) {
        case 1:
          $userData['rights'] = 'Администратор';
          break;
        case 2:
          $userData['rights'] = 'Редактор';
          break;
        case 3:
          $userData['rights'] = 'Хранитель фонда';
          break;
        case 4:
          $userData['rights'] = 'Сотрудник';
          break;
        case 5:
          $userData['rights'] = 'Экскурсовод';
          break;
        default:
          $userData['rights'] = 'Неизвестно';
          break;	      
      }
        
        return $userData;
      } else{
        throw new Exception('Неизвестный пользователь 1=' . $userId);
      }
    }
    

    public function getCurrentUserName() 
    {
      return $this->getUserNameFromId($this->userId_);
    }
    

    public function getUserNameFromId($userId) 
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` 
         WHERE `id`="?i"', $userId);
      
      if ($res->getNumRows() == 1){
        $row = $res->fetch_assoc();
        $userName = $row['surname'] . ' ' .  mb_substr($row['name'], 0, 1) . '. ' .  mb_substr($row['patronymic'], 0, 1) . '.';
        return $userName;
      } else{
        throw new Exception('Неизвестный пользователь 2=' . $userId);
      }
    }
    
    public function getCurrentId()
    {
      return $this->userId_;      
    }
    
    public function getCurrentRights() 
    {
      return $this->getUserRights($this->userId_);
    }
    
    public function getUserRights($userId)
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->table_ . '` 
         WHERE `id`="?i"', $userId);
      
      if ($res->getNumRows() == 1){
        $row = $res->fetch_assoc();
        return $row['rights'];
      } else{
        throw new Exception('Неизвестный пользователь 3=' . $userId);
      }
      
    }
    
    public function setRecentMovement()
    {      
      $page = str_replace('/admin', '', $_SERVER['REQUEST_URI']);
      
      $pos = strripos($page, 'ajax.php');
			
			if ($pos >= 1) {
				return;
			}
			
      $this->connectDB_->dbHandle->query('UPDATE `' . $this->tableRecentMovement_ . '` 
           SET `section` = "?s" WHERE `'.$this->tableRecentMovement_.'`.`user_id` = ?i', $page, $this->userId_);
	    
    }
    
    public function getRecentMovement()
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->tableRecentMovement_ . '` 
         WHERE `user_id`="?i"', $this->userId_);
      
      if ($res->getNumRows() == 1){
        $row = $res->fetch_assoc();
        return $row['section'];
      } else {
        return '/main.php';	      
      }
    }
    
    
    private function checkRecentMovementsRecord()
    {
      $res = $this->connectDB_->dbHandle->query('SELECT * FROM `' . $this->tableRecentMovement_ . '` 
         WHERE `user_id`="?i"', $this->userId_);
      
      if ($res->getNumRows() == 0){
        $queryData=array(
          'id' => '',
          'user_id' => $this->userId_,
          'section' => '/main.php'
        );
        $this->connectDB_->dbHandle->query('INSERT INTO `' . $this->tableRecentMovement_ . '` SET ?As', $queryData);
      }
      	    
    }
    
  }
?>