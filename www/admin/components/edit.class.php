<?php

/*
 * Copyright (c) 2019 ООО МКС, https://www.icann.org

 * All rights reserved.
 
*/

  defined('LOGIN_CONTROL') or die('No direct script access.');
  
  require_once('components/database/Mysql.php');
  require_once('components/database/Mysql/Exception.php');
  require_once('components/database/Mysql/Statement.php');
  
  require_once(__DIR__ . '/controllers/data/edit.class/EditDownloader.class.php');
  require_once(__DIR__ . '/controllers/data/edit.class/FieldsMap.class.php');
  require_once(__DIR__ . '/controllers/data/edit.class/elements/includer.php');
  
  class EditViewMode 
  {
    const DEF = 0;
    const BASKET = 1;	  
  }
  
  class editClass 
  {
    var $host;
    var $user;
    var $password;
    var $db;
    var $table;
    var $dbHandle; 
    var $inc;   
    var $sortParam = array();
    var $keys = array();
    var $userAdmin;  
    var $uploadsPath;  
    var $uploadsPathFull;  
    var $dopGetParams = '';  
    var $modules;  
    var $view = '';

    var $serviceFields = array(
      'kp_date',
      'order_date',
      'related_*',
      'user_last_editing',
      'user_creating',
      'date_last_editing',
      'date_creating',
      'hidden',
      'version',
      'basket',
      'basket_id'
    );

    var $fieldsMap = array();
    var $filtersGetArrays_ = array();
    var $filterKeys_ = array();
    var $viewMode_ = EditViewMode::DEF;
    var $onlyRead_ = false;
      
    function __construct($opts) 
    {
      $this->userAdmin = $GLOBALS['userId'];
      
      $this->setWorkMode();
      
      $this->host = $opts['host'];
      $this->user = $opts['user'];
      $this->password = $opts['password'];
      $this->db = $opts['db'];
      $this->table = $opts['table'];
      
      $this->inc = $opts['inc'];
      
      $this->uploadsPath = $opts['uploadsPath'];
      
      $this->uploadsPathFull = $opts['uploadsPathFull'];
      
      if (isset($opts['viewMode'])) {
        $this->viewMode_ = $opts['viewMode'];	      
      }
      
      if(isset($opts['view'])) {
        $this->view = $opts['view'];
      }
            
      $this->connectDB();
      
      if (isset($_GET['sampleId']) && isset($_GET['table'])) {
        
        $this->dopGetParams = '&sampleId=' . $_GET['sampleId'] 
        . '&table=' . $_GET['table'] 
        . '&sample=' . $_GET['sample']
        . '&sampleName=' . $_GET['sampleName']
        . '&sampleMessage' . $_GET['sampleMessage'];
  
      }
      
      if (isset($_GET['search']) && $_GET['search'] != '') {
        $this->dopGetParams .= '&search=' . $_GET['search'];
      }    
      
      $this->modules = (isset($opts['modules'])) ? $opts['modules'] : '';
      
      $this->keys = array_keys($opts['fdd']);
      
      $this->initFiltersGetArrays($opts['fdd']);
      
      $this->insertFiltersParamInDopUrlParams();
      
      try {
        switch($_GET['operation']) {
          case 'edit':
            $this->showEdit($opts['fdd']);
            break;
          case 'create':
            $this->showCreate($opts['fdd']);
            break;
          case 'delete':
            $this->showEdit($opts['fdd'], 'delete');
            break;
          default: 
            $this->showTable($opts['fdd']);
            break;  
        }
      }
      catch (Exception $e) {
        echo $e->getMessage();    
      }
      
      echo '<script src="js/edit.class/edit-table.js"></script>';
      echo '<script src="js/edit.class/filters.js"></script>';
      echo '<script src="js/edit.class/groups.js"></script>';
      echo '<script src="js/edit.class/related.js"></script>';
      echo '<script src="js/edit.class/file-download.js"></script>';
            
    }

    protected function setWorkMode()
    {
      if (isset($GLOBALS['onlyReadAccess']) && !empty($GLOBALS['onlyReadAccess'])) {
        if (in_array($GLOBALS['userId'], $GLOBALS['onlyReadAccess'])) {
          $this->onlyRead_ = true;	        
        }
      }
    }
    
    protected function showCreate($fdd) 
    {
    //showCreate START
      
      if ($this->viewMode_ == EditViewMode::BASKET || $this->onlyRead_) {
        echo 'Любопытной Варваре на базаре нос оторвали!';
        exit;	      
      }
      
      $view = $this->view;
      $url = $_GET['id'];
      $dopGetParams = $this->dopGetParams;
      include('templates/edit.class/edit-table-header.php');
      
      
      if ($_POST) {
        $saveResult=$this->saveResults($fdd);
        
        echo 'Saved';
        
        echo '<script>document.location="'.$_SERVER['PHP_SELF'].'?sort='.$_GET['sort']
          .'&type='.$_GET['type'].'&page='.$_GET['page'].'&operation=edit&created=true&id='
          .$saveResult['lastId'].'&downloadResult='.$saveResult['downloadResult']
          .'&message='.$saveResult['message'].'";</script>';
      }
    
    
      echo '<form method="post" action="'.$_SERVER['REQUEST_URI'].'"  enctype="multipart/form-data" id="edit">';
      echo '<input type="hidden" name="no-check" value="1" />';
      echo '<div class="main-content-edit_table">';
      $this->printLangChange();
      
      echo '<div class="main-content-edit_table-form">';
      
      $numCols=0;      
        
      foreach($fdd as $key) {  
        
        $en=(strpos($this->keys[$numCols], '_en')) ? true : false;
        
        $key['key']=$this->keys[$numCols];
        
        $key['value']=( (isset( $_GET[$key['key']] ) )  ? $_GET[$key['key']] : '');
        
        echo '<div class="edit_table-form__item '.(($en) ? 'en' : 'ru').' '
          .(($key['type'] == 'file') ? ' edit_table-form__item-image' : '').'">
            <span class="item__name '.(($key['type'] == 'textarea' 
              || $key['type'] == 'file' || $key['type'] == 'related') ? 'item__name-textarea' : '').'">'.$key['name'].'</span>';      
        $this->createElement($key);  
        echo '</div>';
        
        
        $numCols++;
      }
      
      echo '</div>';
      echo '</div>';
      echo '</form>';
      
      $mainContentHeaderClass='main-content-header_bottom';
      include('templates/edit.class/edit-table-header.php');
    }
    
    protected function saveResults($fdd) 
    {  
      $queryData = array();
      
      $countKeys=count($this->keys);
      $resultDownload;
      
      for($i=0; $i < $countKeys; $i++) {
        if($this->keys[$i] == 'id') continue;
        
        
        if($fdd[$this->keys[$i]]['type'] == 'date') {
          $dates=explode('-', $_POST[$this->keys[$i]]);
          $times=explode(':', $_POST[$this->keys[$i].'_time']);
          $_POST[$this->keys[$i]]=mktime((int)$times[0], (int)$times[1], 0, (int)$dates[1], (int)$dates[2], (int)$dates[0]);  
        }
        
        //Related records START
        if($fdd[$this->keys[$i]]['type'] == 'related') {
          if($fdd[$this->keys[$i]]['others']['max_record'] < count($_POST[$this->keys[$i] . '-data'])) {
            continue;            
          }
          
          $relArray = array();
          $relCounter = 0;
          foreach($_POST[$this->keys[$i] . '-data'] as $data) {
            $relRecord = array();
            $relRecord['id'] = $data;
            $relRecord['type'] = (isset($_POST[$this->keys[$i] . '-type'][$relCounter])) ? $_POST[$this->keys[$i] . '-type'][$relCounter] : '';
            array_push($relArray, $relRecord);
            $relCounter++;
          }
            
          $_POST[$this->keys[$i]] = json_encode($relArray);       
        }
        //Related records END
        
        if($fdd[$this->keys[$i]]['type'] == 'file') {
          
          $_POST[$this->keys[$i]] = $_POST[$this->keys[$i].'_hash']; 
          
          if($_POST[$this->keys[$i].'_hash'] != '') {
            $imgQueryData=array(
              'copyright' => $_POST[$this->keys[$i].'_copyright'],
              'description' => $_POST[$this->keys[$i].'_caption'],
              'copyright_en' => $_POST[$this->keys[$i].'_copyright_en'],
              'description_en' => $_POST[$this->keys[$i].'_caption_en']
            );
              
            $this->dbHandle->query('UPDATE `files` SET ?As WHERE `files`.`hash` = "?s"', $imgQueryData, 
                  $_POST[$this->keys[$i].'_hash']);
          }
          
        }
        
        if($fdd[$this->keys[$i]]['type'] == 'password') {
          $_POST[$this->keys[$i]] = md5(md5($_POST[$this->keys[$i]]));
        }
        
        $queryData[$this->keys[$i]]=$_POST[$this->keys[$i]];  
      }
      
      $queryData['user_creating']=$this->userAdmin;
      $queryData['user_last_editing']=$this->userAdmin;
      $queryData['date_creating']=time();
      $queryData['date_last_editing']=time();
      
      $this->dbHandle->query('INSERT INTO `'.$this->table.'` SET ?As', $queryData);
      
      $lastId=$this->dbHandle->getLastInsertId();
      
      return array(
        'lastId' => $lastId, 
        'downloadResult' => $resultDownload['result'], 
        'message' => $resultDownload['message']
        );
      
    }
    
    protected function getPath($hash, $pathType = 'relative') 
    {  
      if($hash == '') return 0;
          
      $resultImg=$this->dbHandle->query('SELECT * FROM `files` WHERE `hash` = "?s"', $hash);
      
      $rowImg=$resultImg->fetch_assoc();
      
      $year=date('Y', $rowImg['date_creating']);
      $month=date('m', $rowImg['date_creating']);
      $day=date('d', $rowImg['date_creating']);
      
      if($pathType == 'relative') {
        return $this->uploadsPath.'/'.$year.'/'.$month.'/'.$day.'/';
      } else {
        return $this->uploadsPathFull.'/'.$year.'/'.$month.'/'.$day.'/';
      }
      
    }
    
    protected function showEdit($fdd, $action = 'edit') 
    {
      
      if ($this->viewMode_ == EditViewMode::BASKET) {
        echo 'Хакеры, хакеры! Спасайтесь!';
        exit;	      
      }
      
      if($_POST && ($action == 'edit') && !$this->onlyRead_) {
        $this->updateResults($fdd);
        
        echo '<p>Данные изменены</p>';
        if($_GET['downloadResult'] != 'success') {
          echo '<p>'.$_GET['message'].'</p>';  
        }
      }
      
      $tableDopStyle='';
      
      if($action == 'delete') {
        if($_POST && ($action == 'delete')) {
          $this->deleteResults($fdd);
        
          $this->showTable($fdd, 'Данные удалены');
          return 0;  
        }
        
        $tableDopStyle='opacity: 0.3;';
      }
      
      $view = $this->view;
      $url = $this->getValue($_GET['id'], 'url');
      $hidden = $this->getValue($_GET['id'], 'hidden');
      if(empty($url)) {
        $url = $_GET['id'];
      }
      
      $dopGetParams = $this->dopGetParams;
      $onlyRead = $this->onlyRead_;
      include('templates/edit.class/edit-table-header.php');
      
      if($_GET['created'] == true) echo 'Данные сохранены';
    
      if ($this->onlyRead_) {
        echo '<p><strong>Внимание! </strong> Эта страница доступна только в режиме чтения.</p>';	      
      }
      echo '<form method="post" action="'.$_SERVER['REQUEST_URI'].'"  enctype="multipart/form-data" id="edit">';
      
      echo '<input type="hidden" name="no-check" value="1" />';
     
      echo '<div class="main-content-edit_table" style="'.$tableDopStyle.'">';
      
      $this->printLangChange();
      
      echo '<input type="hidden" name="qwqd2892" />';//???
      echo '<div class="main-content-edit_table-form">';
      
      $numCols=0;      
        
      foreach($fdd as $key) {  
        
        $en=(strpos($this->keys[$numCols], '_en')) ? true : false;
        
        $key['key']=$this->keys[$numCols];
        
        $key['value']=$this->getValue($_GET['id'], $this->keys[$numCols]);
        
        $key['disabled']=($action == 'edit' && !$this->onlyRead_) ? $key['disabled'] : true;
        
        echo '<div class="edit_table-form__item '.(($en) ? 'en' : 'ru').' '
          .(($key['type'] == 'file') ? ' edit_table-form__item-image' : '').'">
            <span class="item__name '.(($key['type'] == 'textarea' 
              || $key['type'] == 'file' || $key['type'] == 'related') ? 'item__name-textarea' : '').'">'.$key['name'].'</span>';      
        $this->createElement($key);  
        echo '</div>';
        

        $numCols++;
      }
      
      echo '</div>';
      
      
      
      echo '</div>';    
      echo '</form>';
      
      $mainContentHeaderClass='main-content-header_bottom';
      include('templates/edit.class/edit-table-header.php');
    //showEdit END  
    }
    
    protected function deleteListResults($fdd) 
    {
      if (!empty($_POST['del_records'])) {
        foreach ($_POST['del_records'] as $recordId) {
          $this->deleteResults($fdd, $recordId);  
        }
        echo '<p>Записи удалены</p>';
      } else {
        echo '<p>Нет данных для удаления</p>';
      }
    }
    
    protected function reestablishListResults($fdd) 
    {
      if (!empty($_POST['del_records'])) {
        foreach ($_POST['del_records'] as $recordId) {
          $this->reestablishResults($fdd, $recordId);  
        }
        echo '<p>Записи восстановлены</p>';
      } else {
        echo '<p>Нет данных для восстановления</p>';
      }
    }
    
    protected function deleteResults($fdd, $recordId = 0) 
    {
      if ($recordId == 0) {
        $recordId = $_GET['id'];  
      }
    
      
      if ($this->viewMode_ != EditViewMode::BASKET) {
      
        $res = $this->dbHandle->query('SELECT * FROM `' . $this->table . '` WHERE `id` = "?i"', $recordId);
      
        if ($res->getNumRows() == 0) {
          return;
        }
      
        $row = $res->fetch_assoc();
      
        $queryData['name'] = (isset($row['title'])) ? $row['title'] : $row['name'];
        $queryData['table_from'] = $this->table;
        $queryData['date_creating'] = time();
        $queryData['user_creating'] = $this->userAdmin;
        $queryData['date_last_editing'] = time();
        $queryData['user_last_editing'] = $this->userAdmin;
        $queryData['content'] = json_encode($row);
      
        $this->dbHandle->query('INSERT INTO `basket` SET ?As', $queryData);
      }
      
      $this->dbHandle->query('DELETE FROM `'.$this->table.'` WHERE `'.$this->table.'`.`id` = ?i', $recordId);  
    }
    
    protected function reestablishResults($fdd, $recordId = 0) 
    {
      if ($recordId == 0) {
        $recordId = $_GET['id'];  
      }
      
      $res = $this->dbHandle->query('SELECT * FROM `basket` WHERE `id` = "?i"', $recordId);
      
      if ($res->getNumRows() == 0) {
        return;
      }
      
      $row = $res->fetch_assoc();
      
      $queryData = json_decode($row['content'], true);
      
      $queryData['user_last_editing'] = $this->userAdmin;
      $queryData['date_last_editing'] = time();
      $queryData['basket'] = 1;
      
      $this->dbHandle->query('INSERT INTO `' . $row['table_from'] . '` SET ?As', $queryData);
      
      
      $this->dbHandle->query('DELETE FROM `basket` WHERE `basket`.`id` = ?i', $recordId);  
    }
    
    protected function updateResults($fdd) 
    {
      $numCols=0;
    
      foreach($fdd as $key) {  
        $notUpdate=false;
        if($key['disabled'] == true) {
          continue;
          $numCols++;
        }
        
        if($key['type'] == 'date') {
          $dates=explode('-', $_POST[$this->keys[$numCols]]);
          $times=explode(':', $_POST[$this->keys[$numCols].'_time']);
          $_POST[$this->keys[$numCols]]=mktime((int)$times[0], (int)$times[1], 0, (int)$dates[1], (int)$dates[2], (int)$dates[0]);  
        }
        
        //Related records START
        if($key['type'] == 'related') {
          if($key['others']['max_record'] < count($_POST[$this->keys[$numCols] . '-data'])) {
            continue;            
          }
          
          $relArray = array();
          $relCounter = 0;
          foreach($_POST[$this->keys[$numCols] . '-data'] as $data) {
            $relRecord = array();
            $relRecord['id'] = $data;
            $relRecord['type'] = (isset($_POST[$this->keys[$numCols] . '-type'][$relCounter])) ? $_POST[$this->keys[$numCols] . '-type'][$relCounter] : '';
            array_push($relArray, $relRecord);
            $relCounter++;
          }
          
          $_POST[$this->keys[$numCols]] = json_encode($relArray); 
          
                  
        }
        //Related records END
        
        if($key['type'] == 'file') {
                    
          $resultDownload = array('result' => 'success', 'message' => '');
          $resultDownload['hashName'] = $_POST[$this->keys[$numCols].'_hash']; 
          $_POST[$this->keys[$numCols]]=$resultDownload['hashName'];          
         
          if($resultDownload['result'] != 'success' || $resultDownload['hashName'] == '' || $_POST[$this->keys[$numCols].'_hash'] != '') {
            if($_POST[$this->keys[$numCols].'_hash'] != '') {
              
              $queryData=array(
                'copyright' => $_POST[$this->keys[$numCols].'_copyright'],
                'description' => $_POST[$this->keys[$numCols].'_caption'],
                'copyright_en' => $_POST[$this->keys[$numCols].'_copyright_en'],
                'description_en' => $_POST[$this->keys[$numCols].'_caption_en']
              );
              
              $this->dbHandle->query('UPDATE `files` SET ?As WHERE `files`.`hash` = "?s"', $queryData, 
                  $_POST[$this->keys[$numCols].'_hash']);
            }
          }
        }
        
        if($key['type'] == 'password') {
          if($_POST[$this->keys[$numCols]] == '') {
            $notUpdate = true;
          } else {
            $_POST[$this->keys[$numCols]] = md5(md5($_POST[$this->keys[$numCols]]));
          }
        }
        
        
        if(!$notUpdate) {
          $this->dbHandle->query('UPDATE `'.$this->table.'` SET `'.$this->keys[$numCols].'` = "?s" 
            WHERE `'.$this->table.'`.`id` = ?i', $_POST[$this->keys[$numCols]], $_GET['id']);
        }
        
        $numCols++;
      }
          
      if($_POST['hiddenInput'] == 'true' || $_POST['hiddenInput'] == 'false') {
        $_POST['hiddenInput'] = ($_POST['hiddenInput'] == 'true') ? 1 : 0;
        
        $this->dbHandle->query('UPDATE `'.$this->table.'` SET `hidden` = "?i" 
            WHERE `'.$this->table.'`.`id` = ?i', $_POST['hiddenInput'], $_GET['id']);
      }
      
      $this->dbHandle->query('UPDATE `'.$this->table.'` SET `user_last_editing` = "?i", `date_last_editing` = "?i", `basket` = "0" 
            WHERE `'.$this->table.'`.`id` = ?i', $this->userAdmin, time(), $_GET['id']);
      
    }
    
    
    protected function printLangChange() 
    {
      echo '';
          
    }
    
    
    protected function getValue($id, $value, $table = '') 
    {  
      if($table == '') $table=$this->table;
      $result=$this->dbHandle->query('SELECT * FROM `'.$table.'` WHERE `id` = ?i', $id);
      
      if($result->getNumRows() == 1) {
        $row=$result->fetch_assoc();;
        return $row[$value];
      } else {
        return '';  
      }
    }
    
    protected function printModules() 
    {
      if(count($this->modules) > 0 && !empty($this->modules[0])) {
        foreach($this->modules as $module) {
          if(file_exists('modules/' . $module . '/output.php')) {
            include_once('modules/' . $module . '/output.php');
          } else {
            throw new Exception('Отсутствует output.php для модуля ' . $module);
          }
        }
      }
    }
    
    protected function initSortParam($fdd)
    {
      $this->sortParam = $this->fieldsMap->getDisplayFields(); 
    }
    
    protected function initFiltersGetArrays($fdd) {
      
      foreach ($this->keys as $key) {
        if ($fdd[$key]['sort']) {
          if ($fdd[$key]['type'] == 'select' && count($_GET[$key]) > 0) {
            $this->filtersGetArrays_[$key] = $_GET[$key];
            array_push($this->filterKeys_, $key);
          }
        }
      }
      
    }
    
    protected function insertFiltersParamInDopUrlParams() 
    {
      if (count($this->filterKeys_) > 0) {
        foreach ($this->filterKeys_ as $key) {
          foreach ($this->filtersGetArrays_[$key] as $elId) {
            $this->dopGetParams .= '&' . $key . '[]=' . $elId;
          }
        }
      }  
    }
    
    protected function showTable($fdd, $message = '') 
    {
    //showTable START
      $this->fieldsMap = new Base\FieldsMap($this->userAdmin, $this->table, $fdd);
      $allowFields = $this->fieldsMap->getAllowFields();
      $table = $this->table;
      
      $this->initSortParam($fdd);
      
      
      switch ($this->viewMode_) {
        case EditViewMode::BASKET:
          include('templates/edit.class/basket-table-header.php');
          break;
        default:
          $onlyRead = $this->onlyRead_;
          include('templates/edit.class/show-table-header.php');
          break;
      }
      $this->printModules();
      
      echo '<script>var idArray=[];</script>';
      
      if ($this->onlyRead_) {
        $message = '<strong>Внимание!</strong> Данный раздел доступен только для чтения.';	      
      }
      
      if($message != '') echo '<p>'.$message.'</p>';
      
      $wasDelete = false;
      
      if($_POST['delete_list'] == 'true') {
        $this->deleteListResults($fdd);
        $wasDelete = true;
      }
      
      if($_POST['reestablish_list'] == 'true') {
        $this->reestablishListResults($fdd);
        $wasDelete = true;
      }
      
      
      $data=$this->getData();
      
      $noData = false;
      
      if(!$data) {
        if($wasDelete) {
          $_GET['page'] -= 1;
          $data=$this->getData();
          if(!$data) {
            $noData = true;
          }
        } else {
          $noData = true;        
        }
      }
      
      echo '<form method="post" action="' . $_SERVER['REQUEST_URI'] . '" id="show-table-form" data-table="' . $this->table . '">
        <input type="hidden" name="delete_list" value="false" id="show-table-form-delete_status"/>';
      
      if ($this->viewMode_ == EditViewMode::BASKET) {
        echo '<input type="hidden" name="reestablish_list" value="false" id="show-table-form-reestablish_status"/>';	      
      }
      
      echo '<div class="main-content-table">';
      echo '<table>';
      echo '<tr>';
      echo '<th class="edit"><img src="img/main/settings_icon_grey.svg" class="settings" id="open-pop-up-fields" title="Настройки отображения" /></th>
                  <th class="checkbox"></th>';
      
      $sortType=($_GET['type'] == 'asc') ? 'desc' : 'asc';
                  
      echo '<th><a href="'.$_SERVER['PHP_SELF'].'?sort=id&type='
          .$sortType.'&page='.$_GET['page'].$this->dopGetParams.'">ID</a></th>';    
      
      $numCols=0;
      
      
      
      $i=0;
      
      foreach($this->sortParam as $key) {              
        $this->writeTableTh($fdd, $key, $sortType);        
        $numCols++;
        $i++;
        
      }
      
      if ($this->viewMode_ != EditViewMode::BASKET) {
        echo '<th>Группы</th>';	      
      }
      
      /*echo '<th class="delete-td">удаление</th>';*/
      echo '</tr>';
      
      if ($noData) {
        echo '</table>';
        echo 'Записи не найдены';
        return 0;  
      }
      
      while($row = $data->fetch_assoc()) {
        
        echo '<tr  class="row">';
        
        if(isset($row['url'])) {
          
          if(!empty($row['url'])) {
            $url = $row['url'];
          } else {
            $url = $row['id'];
          }
          
        } else {
          $url = $row['id'];
        }
        
        if ($this->viewMode_ != EditViewMode::BASKET) {
          echo '<td>
            <a href="/' . $this->view .'/' . $url .'" target="_blank"> <img src="img/main/preview_icon.svg" class="preview_icon" title="Просмотр" /></a>
          
            <a href="'.$_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='.$_GET['type']
              .'&page='.$_GET['page'].'&operation=edit&id='.$row['id']. $this->dopGetParams . '" title="' . (($this->onlyRead_) ? 'Посмотреть' : ' Редактировать') . '">
          
            <img src="img/main/edit_icon.svg" class="edit_icon" /></a>
          </td>';
        } else {
          echo '<td></td>';	        
        }
        
        echo '<td>';
        
        if (!$this->onlyRead_) {
          echo '   
            <script>idArray.push("'.$row['id'].'");</script>
                      <input type="checkbox" class="edit-checkbox" id="check-'.$row['id'].'" name="del_records[]" value="'.$row['id'].'" />
            <label for="check-'.$row['id'].'" class="checkbox-td"><span></span></label>';
        }
        
        echo '</td>';
        
        if ($this->viewMode_ != EditViewMode::BASKET) {	                    
          echo '<td class="id">' . $row['id'] . '</td>';
        } else {
          $recOriginalId = json_decode($row['content'], true);
          echo '<td class="id">' . $recOriginalId['id'] . '</td>';	        
        }
        
        $numCol=0;//Number field
        foreach($this->sortParam as $param) {
          $numCol++;
          if(!$param) continue;
          
          switch($fdd[$param]['type']) {
            case 'date': 
              $row[$param]=date('d-m-Y', $row[$param]);
              break;
            case 'select':
              if($fdd[$param]['values_from'] == '') {
                $row[$param]=$fdd[$param]['values'][$row[$param]];
              }
              else {
                $res=$this->getValue($row[$param], $fdd[$param]['from_name'], $fdd[$param]['values_from']);
                $row[$param]=(($res != '') ? $res : 'Не определено');
              }
              break;
              
            case 'file':
              //$row[$param]=$this->getAdditionalInfoForFiles($row[$param], $fdd[$param]);
							if (!empty($row[$param])) {
							  $downloader = new Base\EditDownloader($this->userAdmin);
							  $row[$param] = '<img src="' . $downloader->getSrcToMiniatures($row[$param], 'R350-350') . '" class="image-miniature" />';
							}
              break;
            
            default: 
              break;  
          }
          
          
          echo '<td class="'.$fdd[$param]['others']['classes-show-table'].'" contenteditable="' . (($fdd[$param]['editable'] && !$this->onlyRead_) ? 'true' : 'false') .  '" data-id="' . $row['id'] . '" data-key="' . $param . '" id="' . $row['id'] . '-' . $param . '" data-table="' . $this->table . '">'.$row[$param].'</td>';  
        }
        
        $onclickParams="sendForm('show-table-form', 'delete-show-table-exhibit', 'check-" . $row['id'] . "');";
        
        if ($this->viewMode_ != EditViewMode::BASKET) {
          echo '<td>';

          echo '</td>';
        }
        /*echo '<td class="delete-td"><a href="'.$_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='.$_GET['type']
          .'&page='.$_GET['page'].'&operation=delete&id='.$row['id'].'" title="Удалить"  onclick="' . $onclickParams . ' return false;">
          <img src="img/main/delete_icon.svg" class="delete_icon" /></a></td>';
        */  
        echo '</tr>';
      }
      
      echo '</table>';
        
      $this->insertNumbering();
      
      echo '</div>';
      echo '</form>';
      
    //showTable END
    }
    
    protected function writeFilterItem($key, $filterKey, $value)
    {
      
      $checked = (in_array($filterKey, $_GET[$key])) ? 'checked' : '';
      
      echo '
                <div class="item">
                  <input type="checkbox" id="filter-' . $key . '-' . $filterKey . '" value="' . $filterKey . '" ' . $checked . '>
                  <label for="filter-' . $key . '-' . $filterKey . '" class="checkbox-label">
                    <span></span> 
                    ' . $value . '
                  </label>
                </div>
              ';
    }
    
    protected function writeTableTh($fdd, $key, $sortType)
    {
      
      if ($fdd[$key]['sort']) {
        if ($fdd[$key]['type'] == 'select' && !$fdd[$key]['disabled_filters']) {
          echo '
            <th>
              <div class="filter ' . ((count($_GET[$key]) > 0) ? 'active' : '') . '" data-id="' . $key . '-filters">
                <a href="'.$_SERVER['PHP_SELF'].'?sort='.$key.'&type='
                  .$sortType.'&page='.$_GET['page'].$this->dopGetParams.'">'.$fdd[$key]['name'].'</a> 
                  <img src="img/main-png/arrow-bottom.png" />
              </div>
              
              <div class="filters-pop-up" id="' . $key . '-filters" data-field="' . $key . '">
                <div class="checkbox-group">
          ';
          
          if (count($fdd[$key]['values']) > 0 && isset($fdd[$key]['values'])) {
            $filterKeys = array_keys($fdd[$key]['values']);
            
            for ($i = 0; $i < count($fdd[$key]['values']); $i++) {
              $this->writeFilterItem($key, $filterKeys[$i], $fdd[$key]['values'][$filterKeys[$i]]);
            }
            
          } else {
            $values=$this->getValues($fdd[$key]['values_from']);
            
            while ($value = $values->fetch_assoc()) {
              $this->writeFilterItem($key, $value['id'], $value[$fdd[$key]['from_name']]);
            }
          }
          
          echo '  
              </div>
            </th>
          ';
          
        } else {
          echo '<th><a href="'.$_SERVER['PHP_SELF'].'?sort='.$key.'&type='
            .$sortType.'&page='.$_GET['page'].$this->dopGetParams.'">'.$fdd[$key]['name'].'</a></th>';
        }
      } else {
        echo '<th>'.$fdd[$key]['name'].'</th>';
      }
    }
    
    protected function getAdditionalInfoForFiles($hash, $params) 
    {      
      if($params['show_path']) {
        return $this->getPath($hash, 'full').$hash;  
      }
    }
    
    protected function getSQLResultForData($addSQLString = '')
    {
      $basketUserSelectionSQL = '';
      
      if ($this->viewMode_ == EditViewMode::BASKET) {
        $basketUserSelectionSQL = $this->dbHandle->prepare(' `user_creating` = "?i"', $this->userAdmin);	      
      }
      
      if(isset($_GET['sampleId']) && isset($_GET['table'])) {
        //Do sample from special id
        $resultTableId = array();
        
        try {
          $resultTable = $this->dbHandle->query('SELECT * FROM ?f WHERE `id` = "?i" AND `uploaded` = "?i"', 
            $_GET['table'], $_GET['sampleId'], 1);
          
          if($resultTable->getNumRows() == 1) {
            $rowTable = $resultTable->fetch_assoc();
            $resultTableId = json_decode($rowTable[$_GET['sample']], true);
          } else {
            throw new Exception('Запись с указанным id не найдена');
          }
          echo '<p>' . $_GET['sampleMessage'] . ' ' . $rowTable[$_GET['sampleName']] . '</p>';
        }
        catch(Exception $error) {
          echo 'Ошибка: ' . $error->getMessage();
          exit;
        }
        
        $filters = $this->addFiltersInSQL();
        
        if ($filters != '') {
          $sqlRow .= ' AND' . $this->addFiltersInSQL();
        }
        
        if ($basketUserSelectionSQL != '') {
          $basketUserSelectionSQL .= 'AND ' . $basketUserSelectionSQL;	        
        }
        
        $result=$this->dbHandle->query('SELECT * FROM `'.$this->table.'` WHERE `id` IN (?ai) ' . $sqlRow . $basketUserSelectionSQL . $addSQLString, $resultTableId);
      
      } elseif (isset($_GET['search']) && $_GET['search'] != '') {
        //Do search        
        $columns=$this->dbHandle->query('SHOW COLUMNS FROM `'.$this->table.'`');
        
        $selectString = '';
        $searchString = $_GET['search'];
        $selectArray = array(); 
         
        while ($col = $columns->fetch_assoc()){          
          if(!in_array($col['Field'], $this->serviceFields)) {
            $selectArray[] = '`' . $col['Field'] . '` LIKE "%' . $searchString . '%"';
          } 
        }
        
        for ($i = 0; $i < count($selectArray); $i++) {
          $selectString .= $selectArray[$i];
          if ($i != (count($selectArray) - 1)) {
            $selectString .= ' OR ';              
          }          
        }
        
        try {
        
        $filters = $this->addFiltersInSQL();
        
        
        if ($filters != '') {
          $sqlRow .= ' AND' . $this->addFiltersInSQL();  
        }
        
        if ($basketUserSelectionSQL != '') {
          $basketUserSelectionSQL = ' AND' . $basketUserSelectionSQL;	        
        }
        
        $result=$this->dbHandle->query('SELECT * FROM `'.$this->table.'` WHERE (' . $selectString . ') ' . $sqlRow . $basketUserSelectionSQL . $addSQLString);  
        }
        catch(Exception $error) {
          echo 'Ошибка: ' . $error->getMessage();
          exit;
        }   
         
      } else {
        //Do simple select
        
        $sqlRow = 'SELECT * FROM `'.$this->table.'`';
        
        if (count($this->filterKeys_) > 0 || $basketUserSelectionSQL != '') {
          $sqlRow .= $this->dbHandle->prepare(' WHERE');
        }
        
        $sqlRow .= $this->addFiltersInSQL();
        
        if (count($this->filterKeys_) > 0) {
          $sqlRow .= ' AND';	        
        }
        
        $sqlRow .= $basketUserSelectionSQL;
        
        $sqlRow .= $addSQLString;
        
        
        $result=$this->dbHandle->query($sqlRow);
        
      }
      
      
      if(!$result->getNumRows()) return 0;
            
      return $result;
    }
    
    protected function getData() 
    {
      $sortParam=(array_search($_GET['sort'], $this->sortParam) !== false) ? addslashes($_GET['sort']) : 'id';
      $sortType=($_GET['type'] == 'asc') ?  'asc' : 'desc';
      $pageNum=($_GET['page'] > 0) ? (int) $_GET['page'] : 1;
      $addSQLString = $this->dbHandle->prepare(' ORDER BY `'.$this->table.'`.`'.$sortParam.'` 
        '.$sortType.' LIMIT '.($this->inc*($pageNum-1)).', '.$this->inc);
      
      return $this->getSQLResultForData($addSQLString);
    }
    
    protected function addFiltersInSQL() 
    {  
      $sqlRow = '';
      
      $numAnd = count($this->filterKeys_);
      
      
      foreach ($this->filterKeys_ as $key) {
        $numAnd--;
        
        $sqlRow .= $this->dbHandle->prepare(' ?f IN (?as)', $key, $this->filtersGetArrays_[$key]);
        
        if ($numAnd > 0) {
          $sqlRow .= $this->dbHandle->prepare(' AND');
        }
      }
        
      return $sqlRow;
    }
    
    protected function insertNumbering() 
    {
      $result = $this->getSQLResultForData();
      
      $numRows=$result->getNumRows();
      
      
      $numPage=ceil($numRows/$this->inc);
      echo '<div class="main-content-page_nav">';
      
			$prevPageUrl = $_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='.$_GET['type']
            .'&page='.(($_GET['page'] > 1) ? ($_GET['page']-1) : 1). $this->dopGetParams;
			
			echo '<a href="'.$_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='.$_GET['type'].'&page=1 ' . $this->dopGetParams .'">первая</a> 
          
          <a href="'.$prevPageUrl .'"  data-url="'.$prevPageUrl .'" id="page-prev">◀</a> ';
      
      if($numPage < 10) {
        $endP=$numPage;
      } else {
        $endP= ($_GET['page'] < 6) ? 10 : ((($_GET['page'] + 4) > $numPage) ? $numPage : ($_GET['page'] + 4));
      }
      
      $startP=($_GET['page'] < 6) ? 1 : ((($_GET['page'] + 4) <= $numPage) ? ($_GET['page'] - 4) : ($endP - 8));
      
      for($i=$startP; $i <= $endP; $i++) {
        $linkClass='';
        if($_GET['page'] == $i || ($_GET['page'] == '' && $i == 1)) $linkClass=' class="active"'; 
        
        echo '<a href="'.$_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='
          .$_GET['type'].'&page='.$i . $this->dopGetParams .' " '.$linkClass .'>'.$i.'</a> ';
      }
      
			$nextPageUrl = $_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='.$_GET['type']
            .'&page='.(($_GET['page'] < $numPage - 1) ? ($_GET['page']+1) : $numPage) . $this->dopGetParams;
			
      echo '<a href="'. $nextPageUrl .'" id="page-next" data-url="' . $nextPageUrl . '">▶</a> 
          
          <a href="'.$_SERVER['PHP_SELF'].'?sort='.$_GET['sort'].'&type='
            .$_GET['type'].'&page='.$numPage . $this->dopGetParams .'">последняя</a>';
      echo '</div>';
        
    }
    
    protected function connectDB() 
    {
      $this->dbHandle = Database_Mysql::create($this->host, $this->user, $this->password)
                       ->setCharset('utf8')
                       ->setDatabaseName($this->db);
    }
    
        
    protected function getValues($table, $filter = array()) 
    {
      $filterSql = '';
      
      if (!empty($filter) && isset($filter['field']) && isset($filter['value'])) {
        $filterSql = $this->dbHandle->prepare(' WHERE ?f = "?s"', $filter['field'], $filter['value']);  
      }
      
      $result=$this->dbHandle->query('SELECT * FROM `'.$table.'`' . $filterSql);
      
      if(!$result->getNumRows()) return 0;
      
      return $result;  
    }
    
  
    protected function createElement($params) 
    {
      switch ($params['type']) {
        
        
        case 'text':
          echo '<input type="text" maxlength="'.$params['maxlen'].'" name="'
            .$params['key'].'" value="'.htmlspecialchars($params['value']).'" '
            .(($params['disabled']) ? 'disabled' : '').' class="'.$params['others']['classes-edit-table'].' ' . (($params['norepeat']) ? 'edit-check-conflict' : '') . '" data-name="' . $params['name'] . '" data-table="' . $this->table . '" data-id="' . $_GET['id'] . '" placeholder="'.$params['others']['placeholder'].'" />';
          
          break;
        
        case 'password':
          echo '<input type="password" maxlength="'.$params['maxlen'].'" name="'
            .$params['key'].'" '
            .(($params['disabled']) ? 'disabled' : '').' class="'.$params['others']['classes-edit-table'].'" id="password-edit" placeholder="Пароль" />';
            
          echo '<input type="password" maxlength="'.$params['maxlen'].'" name="'
            .$params['key'].'" '
            .(($params['disabled']) ? 'disabled' : '').' class="'.$params['others']['classes-edit-table'].' password-edit-2" id="password-edit-2" placeholder="Повторите пароль" />';
            
          break;  
          
        case 'textarea':
          echo '<textarea  maxlength="'.$params['maxlen'].'" cols="50" rows="8" name="'.$params['key'].'"'
            .(($params['disabled']) ? 'disabled' : '').' class="textarea__anons">'.$params['value'].'</textarea>';
          
          if($params['ckeditor']) echo '<script>CKEDITOR.replace("'.$params['key'].'");</script>';
          
          break;
          
          
        case 'select':
          echo '<select  maxlength="'.$params['maxlen'].'" name="'.$params['key'].'"'.(($params['disabled']) ? 'disabled' : '').'>';
          if($params['values_from'] != '') {
            //Get values for this select from database
            $values=$this->getValues($params['values_from'], $params['filter']);  
            
            echo '<option value="0" disabled>выбор</option>';
            
            while($value=$values->fetch_assoc()) {
              echo '<option value="'.$value['id'].'" '.(($value['id'] == $params['value']) ? 'selected' : '')
                .'>'.$value[$params['from_name']].'</option>';    
            }
          } else {
            for($i=0; $i < count($params['values']); $i++) {
              echo '<option value="'.$i.'" '.(($i == $params['value']) ? 'selected' : '')
                .'>'.$params['values'][$i].'</option>';  
            }
          }
          echo '</select>';
          break;
          
          
        case 'date':
          echo '<input type="date"  maxlength="'.$params['maxlen'].'" name="'.$params['key'].'" value="'.(($params['value'] == '') ? date('Y-m-d', time()) : date('Y-m-d', (int)$params['value'])).'" '.(($params['disabled']) ? 'disabled' : '').' />';
          
          if($params['show_time']) {
            echo '<span class="item__time">Время</span>
                          <input type="time" name="'.$params['key'].'_time" value="'.(($params['value'] == '') ? date('H:i', time()) : date('H:i', (int)$params['value'])).'" '.(($params['disabled']) ? 'disabled' : '').'> ';  
          }
          
          break;
          
        
        case 'related':    
         
          try {      
            $element = new Base\EditElementsRelated($this->table, $params);
            $element->printElement();
          }
          catch (Exception $error) {
            echo $error->getMessage();
          }
          
          break;
          
        case 'file':
          try {      
            $element = new Base\EditElementsFile($this->table, $_GET['id'], $params);
            $element->printElement();
          }
          catch (Exception $error) {
            echo $error->getMessage();
          }
          break;
          
          
        default:
            echo 'Неизвестный тип элемента '.$params['type'];
      }
    }
  
  }
  
?>