<?php
  $links = array(
    'news' => 'news.php',
    'slider' => 'slider.php',
    'pages' => 'pages.php',
    'file-manager' => 'file-manager.php',
    'users' => 'users.php',
    'type-news' => 'type-news.php',
    'videos' => 'videos.php',
  );

  $tablesOfActiveComponents = array(
    1 => array(
      'name' => 'Файлы',
      'table' => 'files',
      'type_control' => 1,
      'signature' => 'файла',
      'record_name' => 'name',
      'link' => '#'
    ),
    2 => array(
      'name' => 'Новости',
      'table' => 'news',
      'type_control' => 1,
      'signature' => 'новости',
      'record_name' => 'title',
      'link' => $links['news']
    ),
    3 => array(
      'name' => 'Видео',
      'table' => 'videos',
      'type_control' => 1,
      'signature' => 'видео',
      'record_name' => 'name',
    ),
    4 => array(
      'name' => 'Страницы',
      'table' => 'pages',
      'type_control' => 1,
      'signature' => 'страницы',
      'record_name' => 'title',
      'link' => $links['pages']
    ),
    5 => array(
      'name' => 'Пользователи',
      'table' => 'users',
      'type_control' => 1,
      'signature' => 'пользователя',
      'record_name' => 'login',
      'link' => $links['users']
    ),
    6 => array(
      'name' => 'Типы новостей',
      'table' => 'type_v',
      'type_control' => 1,
      'signature' => 'типы новостей',
      'record_name' => 'name',
      'link' => $links['type-news']
    ),
  );

  $defaultInstruments = array(
    1 => array(
      'name' => 'Страницы',
      'active' => true,
      'code' => 'pages',
      'access' => array(1, 2),
      'toolbar' => true                 
    ),
    2 => array(
      'name' => 'Пользователи',
      'active' => false,
      'code' => 'users',
      'access' => array(1,1),
      'toolbar' => true                  
    ),
    3 => array(
      'name' => 'Новости',
      'active' => true,
      'code' => 'news',
      'access' => array(1, 2, 4),
      'toolbar' => true,
      'main-page-conf' => array(
        'groups' => array('news'),
        'intranet' => false,
        'show-quantity' => true,
        'child' => array(
          0 => array('name' => 'Новости', 'url' => '/admin/events.php?type_v[]=1')
        )
      )
    ),
    4 => array(
      'name' => 'Видео',
      'active' => true,
      'code' => 'videos',
      'access' => array(1, 2),
      'toolbar' => true
    ),
    5 => array(
      'name' => 'Типы новостей',
      'code' => 'type-news',
      'table' => 'type_v',
      'access' => array(1, 2),
      'main-page-conf' => array(
        'groups' => array('news'),
        'intranet' => false,
        'settings' => true
      )
    ),
  );
?>