<?php
  define('LOGIN_CONTROL', 1);
  
  require_once(__DIR__ . '/../controllers/users/User.class.php');
  $user = new User();
  
  $req = array('success' => false, 'message' => '');
  
  try {      
    $req['message'] = $user->resetPassword($_POST['login']);
    $req['success'] = true;
  }
  catch (Exception $error) {
    $req['message'] = $error->getMessage();
  }
  
  echo json_encode($req);
  
?>