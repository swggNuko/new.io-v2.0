<?php
  define('LOGIN_CONTROL', 1);
  
  error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
  require_once('components/config.php');
  
  $page=$_SERVER['PHP_SELF'];
  
  include_once( __DIR__ . '/controllers/users/User.class.php' );
  $user = new User();
  
  if ($_POST && isset($_POST['login']) && isset($_POST['password']) && $_POST['no-check'] != 1) {
    
    $hash = $user->logInControl($_POST['login'], $_POST['password']);
    
    if ($hash != '0') {
      setcookie('access', $hash, 0, '/');
      header('Location: /admin' . $user->getRecentMovement());
      exit;
    } else {
      header('Location: index.php?error=1&login=' . $_POST['login']);
      exit;
    }
  }
  
  if (isset($_COOKIE['access'])) {
    
    if (!$user->checkUser($_COOKIE['access'])) {
      setcookie('access', '', time() - 60, '/');
      header('Location: index.php?error=2');
      exit;
    } elseif ($page == $entryFormPage) {
      
      header('Location: main.php');
      exit;
    }
    
  } else {
    if($page != $entryFormPage) {
      header('Location: index.php?error=1');
      exit;
    }
  }
  
  if (isset($_GET['exit'])) {
    setcookie('access', '', time() - 60, '/');
    header('Location: index.php');
    exit;
  }
  
  if (isset($GLOBALS['access'])) {
    
    $userRights = $user->getCurrentRights();
    
    if (!in_array($userRights, $GLOBALS['access'])) {
      header('Location: main.php');
      exit;
    }
    
  }
  
  $GLOBALS['userId'] = $user->getCurrentId();
  
  if (!empty($GLOBALS['userId'])) {
    $GLOBALS['userRights'] = $user->getCurrentRights();
  
    $user->setRecentMovement();
  }
    
?>