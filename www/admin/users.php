<?php
  $pageTitle='Пользователи';
	$GLOBALS['access'] = array(1);
  include_once('templates/header.php');
  
?>

<?php
  include_once('components/config.php');
  $opts['table'] = 'users';
  
  $opts['key'] = 'id';
  
  $opts['key_type'] = 'int';
  
  $opts['inc'] = 30;
  
  $opts['fdd']['surname'] = array(
    'name'     => 'Фамилия',
    'type'     => 'text',
    'maxlen'   => 255,
    'sort'     => true,
    'editable' => true,
		'display-in-table' => true
  );
  
  $opts['fdd']['name'] = array(
    'name'     => 'Имя',
    'type'     => 'text',
    'maxlen'   => 255,
    'sort'     => true,
    'editable' => true,
		'display-in-table' => true
  );
  
  $opts['fdd']['patronymic'] = array(
    'name'     => 'Отчество',
    'type'     => 'text',
    'maxlen'   => 255,
    'sort'     => true,
    'editable' => true,
		'display-in-table' => true
  );
  
  $opts['fdd']['post'] = array(
    'name'     => 'Должность',
    'type'     => 'text',
    'maxlen'   => 255,
    'sort'     => true,
    'editable' => true,
		'display-in-table' => true
  );
  
  $opts['fdd']['rights'] = array(
     'name'     => 'Права',
     'sort'     => true,
		 'display-in-table' => true,
     'type'     => 'select',
     'values' => array(
          0 => 'Не выбрано',
          1 => 'Администратор',
          2 => 'Редактор',
          3 => 'Хранитель фонда',
          4 => 'Сотрудник',
          5 => 'Экскурсовод'
        )
    );
    
  $opts['fdd']['login'] = array(
    'name'     => 'Логин',
    'type'     => 'text',
    'maxlen'   => 255,
    'sort'     => true,
		'norepeat' => true,
		'display-in-table' => true
  );
  
  $opts['fdd']['password'] = array(
    'name'     => 'Пароль',
    'type'     => 'password',
    'maxlen'   => 255
  );
  
  $opts['fdd']['e_mail'] = array(
    'name'     => 'E-mail',
    'type'     => 'text',
    'maxlen'   => 255,
    'sort'     => true,
    'editable' => true,
		'display-in-table' => true
  );

  
    
    
    
  include_once('components/edit.class.php');
  new editClass($opts);
?>
  
<?php
  include_once('templates/footer.php');
?>
