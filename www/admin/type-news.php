<?php
  $pageTitle='Тип новости';
  $GLOBALS['access'] = array(1, 2);//Rights which have an assecc to this page
  include_once('templates/header.php');
?>

  <?php
    // MySQL host name, user name, password, database, and table
    include_once('components/config.php');
    $opts['table'] = 'type_v';
    
    // Name of field which is the unique key
    $opts['key'] = 'id';
    
    // Type of key field (int/real/string/date etc.)
    $opts['key_type'] = 'int';
    
    // Number of entries per page
    $opts['inc'] = 15;
    
    
    $opts['fdd']['name'] = array(
        'name'     => 'Название типа',
        'type'     => 'text',
        'maxlen'   => 255,
        'sort'     => true,
        'editable' => true,				        'display-in-table' => true,
        'others'    => array(
          'classes-edit-table' => 'input__work-title'
        )
    );

    
    
    include_once('components/edit.class.php');
    new editClass($opts);
  ?>
  
<?php
  include_once('templates/footer.php');
?>
