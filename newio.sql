-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Апр 25 2019 г., 22:36
-- Версия сервера: 10.1.34-MariaDB
-- Версия PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `newio`
--

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `protected` tinyint(1) NOT NULL,
  `copyright` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `copyright_en` varchar(255) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `user_creating` int(10) UNSIGNED NOT NULL,
  `user_last_editing` int(10) UNSIGNED NOT NULL,
  `date_last_editing` int(10) UNSIGNED NOT NULL,
  `date_creating` int(10) UNSIGNED NOT NULL,
  `basket` tinyint(1) NOT NULL,
  `basket_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `name`, `hash`, `type`, `protected`, `copyright`, `description`, `copyright_en`, `description_en`, `user_creating`, `user_last_editing`, `date_last_editing`, `date_creating`, `basket`, `basket_id`) VALUES
(1, 'IMG_9636.JPG', '8490186366f3cbc19c193dda7a1be447.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1546781651, 1546781636, 0, 0),
(2, 'fTmbHzP8eJM.jpg', '655f4af01ec2e615a0e00d5635b70136.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1546781656, 1546781656, 0, 0),
(3, 'IMG_5241.JPG', '76ba36e8586800ba490c45c716cf17c1.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1546781661, 1546781661, 0, 0),
(4, '2n22eEoklZ4.jpg', 'b18f4f773234f783281f9a79de8164b3.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1546781664, 1546781664, 0, 0),
(5, '08010735.114255.2065.jpeg', 'ddb526bc719aa93abe64cda95513275d.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1547298796, 1547298598, 0, 0),
(6, '0VbvEeGvcoY.jpg', '18408ce6b0d905a8c8c138323bd7a713.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1547298723, 1547298723, 0, 0),
(7, 'Снимок.PNG', 'b21c2748b8b8d149318838de496a6cb2.jpg', '.jpg', 0, '123', '123', '', '', 1, 1, 1555690998, 1555690998, 0, 0),
(8, 'beautiful-calm-clouds-206359.jpg', '28a2aa040112ed953b0867657132c903.jpg', '.jpg', 0, 'wer', 'wer', '', '', 1, 1, 1555840198, 1555840198, 0, 0),
(9, 'beautiful-bloom-blossom-906150.jpg', '35b65d47b72984210bd680a2e5e87d81.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1555840583, 1555840583, 0, 0),
(10, 'tree-736885_960_720.jpg', '19cb5827afaad73742afda1273e5da84.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1555840646, 1555840646, 0, 0),
(11, 'singapore-at-night.jpg', '9f59543373291c23cf725674cb098171.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556040534, 1556040534, 0, 0),
(12, 'singapore-at-night.jpg', 'c2d700803a79807a754a7913ca8428ae.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556040580, 1556040580, 0, 0),
(13, 'singapore-at-night.jpg', '90f600761ceb72d5aac324517926cd4f.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556040793, 1556040793, 0, 0),
(14, 'beautiful-bloom-blossom-906150.jpg', '88a407a8ff1ef4b3b60c5c96c141f915.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556040873, 1556040873, 0, 0),
(15, 'singapore-at-night.jpg', 'fbe47580bd4d68fae2c9b41ce30712fd.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556040949, 1556040949, 0, 0),
(16, 'Karta_Razviazka_Escape_of_Tarkov_Edit.jpg', '3b423bb487d224f6de53b4c4b0598c4c.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556131741, 1556131741, 0, 0),
(17, 'beautiful-bloom-blossom-906150.jpg', '7763f7aef4de2ccf3e2d2ce29bade88f.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556131811, 1556131811, 0, 0),
(18, 'singapore-at-night.jpg', 'a31053df668d00313bb958492fbb8bce.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556131849, 1556131849, 0, 0),
(19, 'tree-736885_960_720.jpg', 'e1429ececb197012f9b4d52d4fd1761f.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556223529, 1556223529, 0, 0),
(20, 'tree-736885_960_720.jpg', 'd659fb8717c72e233a608ccc0ce3b24f.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556223617, 1556223617, 0, 0),
(21, 'beautiful-calm-clouds-206359.jpg', '791f57bf48001d17e1f4116d3e841564.jpg', '.jpg', 0, '', '', '', '', 1, 1, 1556223662, 1556223662, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `main_site_menu`
--

CREATE TABLE `main_site_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `link_name` varchar(255) NOT NULL,
  `link_name_en` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `basket_id` int(11) NOT NULL,
  `basket` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `post` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` int(11) NOT NULL,
  `text` text NOT NULL,
  `anons` text NOT NULL,
  `images` text NOT NULL,
  `related_materials` text NOT NULL,
  `date_last_editing` int(11) UNSIGNED NOT NULL,
  `date_creating` int(11) UNSIGNED NOT NULL,
  `user_creating` int(11) UNSIGNED NOT NULL,
  `user_last_editing` int(11) UNSIGNED NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `type_v` int(11) NOT NULL,
  `basket` int(11) NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `post`, `title`, `date`, `text`, `anons`, `images`, `related_materials`, `date_last_editing`, `date_creating`, `user_creating`, `user_last_editing`, `hidden`, `type_v`, `basket`, `views`) VALUES
(16, '', 'Technology Needs To Be Regulated', 1556056800, '<p>Apple CEO Tim Cook has called for more government regulation on the technology industry in order to protect privacy in an interview at the TIME 100 Summit in New York.</p>\r\n\r\n<p>“We all have to be intellectually honest, and we have to admit that what we’re doing isn’t working,” said Cook, in an interview with former TIME Editor in Chief Nancy Gibbs. “Technology needs to be regulated. There are now too many examples where the no rails have resulted in a great damage to society.”</p>\r\n\r\n<p>Cook, who took over from Steve Jobs as Apple’s CEO in 2011, is one of the most prominent voices in Silicon Valley calling for the government to step in to his industry in order to protect user rights and privacy.</p>\r\n\r\n<p>In the interview, Cook suggested that U.S. regulators could look to Europe’s passage of the General Data Protection Regulation (GDPR) in 2018. “GDPR isn’t ideal,” said Cook. “But GDPR is a step in the right direction.”</p>\r\n\r\n<p>In light of recent data breaches and foreign election influence through social media, Cook’s view is that the tech industry has no other responsible option but to accept more government oversight, a position he outlined in a recent TIME Ideas piece.</p>\r\n\r\n<p>“I’m hopeful,” Cook said at the Summit. “We are advocating strongly for regulation — I do not see another path.”</p>\r\n\r\n<p>Cook also explained Apple’s stance on transparency and money in politics. “We focus on policies, not politics,” Cook said. “Apple doesn’t have a PAC…I refuse to have one because it shouldn’t exist.”</p>\r\n\r\n<p>The CEO also spoke on Apple’s positions on other issues, like immigration and education, as well as the company’s new emphasis on health-related technology, like the newest Apple Watch, which includes a built-in EKG.</p>\r\n\r\n<p>“I do think that there will be the day that we will look back and say, ‘Apple’s greatest contribution to mankind was in healthcare’.”</p>\r\n\r\n<p>Cook also explained the way that Apple thinks about the relationship between people and the devices the company creates.</p>\r\n\r\n<p>Apple doesn’t want to keep people glued to the phones in their pockets, which is why they have developed tools to help users keep track of the amount of time they spend on their phones, Cook said.</p>\r\n\r\n<p>“Apple never wanted to maximize user time,” Cook said. “We’ve never been about that. We’re not motivated to do that from a business point of view, and we’re certainly not motivated from a values point of view.”</p>\r\n\r\n<p>“If you’re looking at a phone more than someone’s eyes, you’re doing the wrong thing,” Cook said.</p>\r\n\r\n<p>Speaking on those issues, Cook returned to his own perspective on corporate responsibility. He says corporate leaders should do what they think is right, instead of trying to avoid controversy.</p>\r\n\r\n<p>“I try not to get wrapped up in a pretzel about who we upset,” Cook said. “At the end of the day we’ll be judged more on ‘did we stand up for what we believed in,’ not necessarily, ‘do they agree with it.&#39;”</p>\r\n', 'Apple CEO Tim Cook Says No Oversight Has Led To Great Damage To Society', '3b423bb487d224f6de53b4c4b0598c4c.jpg', '', 1556131747, 1556131747, 1, 1, 0, 2, 0, 0),
(8, '1', 'Usu ei mutat causae mnesarchum. Consectetuer delicatissimi per ne. ', 1555797600, '<p>Usu debet option voluptaria ut. Dolor laboramus has no, erroribus constituto nec at. Nam te omnium laboramus necessitatibus, porro laoreet ex pro. Sit singulis invenire sapientem in, lorem lucilius repudiare est ex. Et etiam erant accommodare usu. Sint vituperatoribus ne vix, at brute viris habemus mei, posse veniam splendide mei eu.</p>\r\n\r\n<p>Est ne vidit labores consulatu, ut mel lorem dissentiet adversarium. Ad pro nisl probo populo. Case posse falli qui in, nec ignota adipisci instructior no. Diam mollis eum ea, mea no alia purto abhorreant, at qui diam quando elaboraret. An latine vocibus duo, sea ea eirmod interpretaris. Ut vis harum nonumy sadipscing.</p>\r\n\r\n<p>Dolorem expetendis efficiendi mea an, meis dolorem vituperatoribus usu ad, eu vis ipsum nihil vocent. Aperiri luptatum nam te. Te etiam corpora sed, sit eu prodesset appellantur. Erant percipit vim ea, malis necessitatibus at pro. At vim errem qualisque, pri natum tractatos ea, duis justo mucius ea vix.</p>\r\n\r\n<p>Pro at iuvaret singulis consetetur, nulla eirmod usu an. Sed ea veniam deserunt. Pri nominavi phaedrum deserunt ei, qui rebum debitis rationibus cu, est regione eligendi in. Phaedrum petentium efficiendi quo no. Quo modo erroribus adolescens ex, sed ei ipsum tantas honestatis, qui cu suas numquam dissentiet. Est ad nobis eruditi, in vix liber semper, eu dolor everti utroque per.</p>\r\n\r\n<p>Dico illud dolores ex sed, per te ubique altera constituto. Nec aeterno hendrerit cu, sanctus vivendo invenire id pri, his consul corrumpit et. Vis ei graece suavitate. Ut has unum utroque insolens. Cu pro voluptua scaevola, choro contentiones intellegebat an eam, quo ex error tamquam.<br />\r\n </p>\r\n', 'No nisl placerat contentiones sit, vim at solet detracto. Ea sit liber expetenda forensibus, nam ea enim ferri. Eu dicat malorum his, modus audire constituam eu vis. Pro malorum verterem te.', '28a2aa040112ed953b0867657132c903.jpg', '', 1555840206, 1555840206, 1, 1, 0, 1, 0, 0),
(9, '', 'Lorem ipsum dolor sit amet, no mei ponderum dignissim.', 1555797600, '<p>Vis id mundi veritus. Ut sed ipsum viderer. Partem liberavisse philosophia ut vix, cum et senserit scribentur. An sumo agam neglegentur qui, veri splendide est no, tantas utroque ius id.</p>\r\n\r\n<p>Ei nonumes nostrum mei, vim meis accusamus argumentum ne. Vim enim graeci placerat ei, eos fabulas offendit ut, et mei aliquam mnesarchum. Vel aperiri antiopam mnesarchum cu. Putant insolens perpetua ea mei, animal tractatos ut eos.</p>\r\n\r\n<p>Est melius admodum suscipit ex. Pri reque omnium insolens ei, no pri movet voluptua. Ne has quem veniam. Vis nostrum salutandi ea, ei duo purto atqui.</p>\r\n\r\n<p>No alii officiis vel, pri et dicta tempor. Posse honestatis his et, nostrud utroque verterem nam in. Atomorum necessitatibus vix ut, ea vis dicta iudico, no error elitr vel. Ex qui efficiantur concludaturque. His sint principes intellegat ad, veri semper signiferumque id cum. Detracto deseruisse ne nam. Fierent appellantur quo ex.</p>\r\n\r\n<p>Has sale fabulas dolorum cu, ullum appetere in cum, altera admodum detraxit ei cum. Ne pri vidit constituam. Cetero deserunt facilisis an est. Facilisi reformidans quo et, aliquid torquatos ei nec.</p>\r\n\r\n<p>Quo libris perpetua accommodare an. Audiam pericula te his. Eam ei nulla interesset, vim ne etiam dicta falli. Nisl eros pertinax mel ea.</p>\r\n', 'Duo antiopam senserit ei, delenit facilisi disputando in vis, feugiat tibique et ius. Enim definiebas eu usu, ea eum iisque delenit. Cum ex mundi congue, graeci apeirian adipisci ius ut, nec magna nihil theophrastus id. Eos no aliquam bonorum voluptua, offendit praesent cum ut. Nec an ponderum delicatissimi, homero voluptaria adversarium vim ex. Partem detraxit an nam, has et agam intellegebat, in justo lorem vocent qui.', '35b65d47b72984210bd680a2e5e87d81.jpg', '', 1555840587, 1555840587, 1, 1, 0, 1, 0, 0),
(10, '', 'Lorem ipsum dolor sit amet, no vim partem iisque.', 1555797600, '<p>Ut volutpat postulant consequat est, ne est quot ridens recteque, at his putant veritus erroribus. Vis in quis everti, dolore prompta consetetur est eu, ad eum simul perpetua. Vix ea euismod inermis definitionem, pri at erroribus repudiare, cum aliquid qualisque constituam te. Mel eu quot legendos, ridens adipisci no duo. An pro commodo aperiam dolorem, pri justo albucius in. Ius fierent intellegam ad, vix cu reque delectus, partem fuisset facilisi ei ius. Decore aliquam ocurreret sed et, mei habeo nulla tractatos ex, qualisque dignissim persequeris at nam.</p>\r\n\r\n<p>Vel ea docendi referrentur, ei nec alterum aliquando voluptatum, iuvaret percipit iudicabit cu per. Ea veritus definitionem quo, in agam omittam ponderum vim. Eu tollit indoctum instructior has, nam et stet possim dignissim. Et interesset liberavisse vel, in saepe alienum splendide nec. Sale accusam eos ut. Eum no duis prompta, vel ad malis numquam mnesarchum.</p>\r\n', 'Dolore appellantur deterruisset vix id, brute argumentum usu cu. Vim te iriure evertitur, assum placerat eam at. At vim eirmod gubergren complectitur, in mazim sonet sed. Nec ei fastidii suscipit imperdiet, te nec tale suscipit. Te pri choro alienum mediocritatem, id eam doming accommodare. Atqui rationibus has et.', '19cb5827afaad73742afda1273e5da84.jpg', '', 1555962884, 1555840648, 1, 1, 0, 1, 0, 0),
(11, '', 'Technology Needs To Be Regulated', 1555970400, '<p>Apple CEO Tim Cook has called for more government regulation on the technology industry in order to protect privacy in an interview at the TIME 100 Summit in New York.</p>\r\n\r\n<p>“We all have to be intellectually honest, and we have to admit that what we’re doing isn’t working,” said Cook, in an interview with former TIME Editor in Chief Nancy Gibbs. “Technology needs to be regulated. There are now too many examples where the no rails have resulted in a great damage to society.”</p>\r\n\r\n<p>Cook, who took over from Steve Jobs as Apple’s CEO in 2011, is one of the most prominent voices in Silicon Valley calling for the government to step in to his industry in order to protect user rights and privacy.</p>\r\n\r\n<p>In the interview, Cook suggested that U.S. regulators could look to Europe’s passage of the General Data Protection Regulation (GDPR) in 2018. “GDPR isn’t ideal,” said Cook. “But GDPR is a step in the right direction.”</p>\r\n\r\n<p>In light of recent data breaches and foreign election influence through social media, Cook’s view is that the tech industry has no other responsible option but to accept more government oversight, a position he outlined in a recent TIME Ideas piece.</p>\r\n\r\n<p>“I’m hopeful,” Cook said at the Summit. “We are advocating strongly for regulation — I do not see another path.”</p>\r\n\r\n<p>The CEO also spoke on Apple’s positions on other issues, like immigration and education, as well as the company’s new emphasis on health-related technology, like the newest Apple Watch, which includes a built-in EKG.</p>\r\n\r\n<p>“I do think that there will be the day that we will look back and say, ‘Apple’s greatest contribution to mankind was in healthcare’.”</p>\r\n\r\n<p>Cook also explained the way that Apple thinks about the relationship between people and the devices the company creates.</p>\r\n\r\n<p>Apple doesn’t want to keep people glued to the phones in their pockets, which is why they have developed tools to help users keep track of the amount of time they spend on their phones, Cook said.</p>\r\n\r\n<p>“Apple never wanted to maximize user time,” Cook said. “We’ve never been about that. We’re not motivated to do that from a business point of view, and we’re certainly not motivated from a values point of view.”</p>\r\n\r\n<p>“If you’re looking at a phone more than someone’s eyes, you’re doing the wrong thing,” Cook said.</p>\r\n\r\n<p>Speaking on those issues, Cook returned to his own perspective on corporate responsibility. He says corporate leaders should do what they think is right, instead of trying to avoid controversy.</p>\r\n\r\n<p>“I try not to get wrapped up in a pretzel about who we upset,” Cook said. “At the end of the day we’ll be judged more on ‘did we stand up for what we believed in,’ not necessarily, ‘do they agree with it.&#39;”</p>\r\n\r\n<p>See TIME&#39;s full list of the 100 Most Influential People of 2019</p>\r\n', ' Apple CEO Tim Cook Says No Oversight Has Led To Great Damage To Society', '9f59543373291c23cf725674cb098171.jpg', '', 1556040537, 1556040537, 1, 1, 0, 2, 0, 0),
(12, '', 'Finding Value In Silicom, A Growing, Niche Technology Business', 1555970400, '<p>Israel-based Silicom is a niche producer of customized network interface cards, which has been an excellent business for the company for many years.  Revenues have been growing at an 18% compound annual growth rate over the past ten years, while profit margins have been attractive because customized network interface cards are a small, niche business which does not attract a lot of competitors. Meanwhile, due to the loss of a large contract in early 2018, its share price declined by over 50%, even though the long-term fundamentals across the rest of its business remain attractive. Since then, Silicom&#39;s share price has consolidated in the $30s, which makes it a stock worth looking at.</p>\r\n\r\n<p>Silicom’s History</p>\r\n\r\n<p>Silicom was incorporated in Israel and began operations in 1987. In the 1990s, Silicom specialized in the development and manufacturing of networking hardware for resellers of broadband internet bandwidth. With the collapse of the dot-com bubble, Silicom found itself with an attractive set of technologies and no customers. As the company sought to reinvent itself, management realized that cybersecurity companies, which were mostly software developers, would greatly benefit if they could replace generic networking cards in their solutions with sophisticated and customized products.</p>\r\n\r\n<p>Silicom managed to return to growth in FY2003 and profitability in FY2005 by pursuing this business niche. The company eventually expanded beyond serving just cybersecurity customers and has not experienced a money-losing quarter since 2005.</p>\r\n\r\n<p>Silicom’s Products and Customers</p>\r\n\r\n<p>You probably have an integrated or a pluggable network interface card (NIC) in your home computer. The simplest NICs are commoditized products can be purchased for as little as $10. Silicom’s NIC products, in contrast, are highly customized and differentiated. Silicom designs customized NICs which often cost hundreds or even thousands of dollars per unit for a relatively small number of highly-sophisticated cloud/enterprise-server Original Equipment Manufacturers (OEMs). These server OEMs require advanced data compression, encryption, time-stamping, switching, routing and fail-safe features that are of little use to retail customers. Moreover, by offloading specific networking-related processes to a smart networking card, an OEM can save substantial amounts of CPU’s bandwidth for other tasks.</p>\r\n\r\n<p>In the chart below, you can see the breakdown of Silicom’s revenues by end markets based on FY 2017 figures. All of these are large, fast-growing markets. Its customer list includes Symantec, Cisco, Western Digital, Lenovo, Riverbed, and Check Point.</p>\r\n\r\n<p>Unfortunately, there are no market size and growth data for customized NICs within the high-end segments that Silicom serves, primarily because of the market’s small size. However, it is likely that these market segments are much larger than Silicom’s current revenue base and are growing at a rapid pace. Indeed, Silicom’s customer base has been steadily increasing for many years and is characterized by extraordinary loyalty; the company claims that they have never lost a single customer, although they have lost specific product design RFPs.  Moreover, according to management, Silicom is retained for next-generation products in 95%+ of the cases.</p>\r\n\r\n<p>Source: Company Reports</p>\r\n\r\n<p>Pekin Hardy Strauss Wealth Management</p>\r\n\r\n<p>Silicom’s Revenue and Profit Growth Trends</p>\r\n\r\n<p>As a result of increased demand and Silicom’s ability to retain key customers, the company has grown revenues at an 18% compound annual growth rate over the past ten years. In March 2018, the contract for the largest design win in Silicom’s history was canceled because the customer involved had given up on its own system design.  The announcement of this contract cancellation coincided with a steep decline in Silicom’s share price to a level which seems both inexpensive and unwarranted.  Despite this major contract going away, Silicom&#39;s top-line increased by 6% in FY 2018.</p>\r\n', 'Israel-based Silicom is a niche producer of customized network interface cards', 'c2d700803a79807a754a7913ca8428ae.jpg', '', 1556040731, 1556040581, 1, 1, 0, 2, 0, 0),
(13, '', 'AI, machine-learning', 1555970400, '<p>AI, machine-learning and other technology-based advances fail to recognize that executive recruiting is more art than science.</p>\r\n\r\n<p>April 23, 2019 8 min read</p>\r\n\r\n<p>Opinions expressed by Entrepreneur contributors are their own.</p>\r\n\r\n<p>In 2017, an artificial intelligence (AI)-based software company called Textio raised $20 million and grew its client base by 200 percent, all in the name of transforming the way companies write job descriptions. The announcement of this software, which had been designed to boost diversity and inclusivity by using AI-generated gender-neutral language, was exciting. But it was hardly an isolated event.</p>\r\n\r\n<p>Related: How Integrating AI Into Recruitment Can Benefit Companies Facing a Labor Crisis</p>\r\n\r\n<p>In fact, Textio is one of many technology disruptions currently shaping the future of recruiting.</p>\r\n\r\n<p>Yet those disruptions aren&#39;t all good, because, despite the promise big data offers, AI, machine-learning and other technology-based advances ultimately fail to recognize the reality that executive recruiting is more art than science.</p>\r\n\r\n<p>Anyone -- even a computer -- can scan a candidate’s resume for the basic skills needed to get a specifuc job done. What’s much harder is understanding how a company’s intangible culture has an impact on hiring, retention and the bottom line.</p>\r\n\r\n<p>Here&#39;s what I&#39;ve learned as an executive recruiter that helps me find the best employee fit when it comes to matching a candidate to a company&#39;s culture.</p>\r\n\r\n<p>\"Don&#39;t choose your club; choose your owner.\"</p>\r\n\r\n<p>As part of my executive recruiting business, I advise candidates looking for a senior role in team sports. In that context, I like to say, “Don’t choose your club, choose your owner.”</p>\r\n\r\n<p>By this, I mean that a company’s flashy name or reputation matters far less than the company&#39;s ability to establish trust and offer a strong alignment with key stakeholders’ core values. This mantra applies to recruiting across industries -- not just sports -- yet it’s typically overlooked.</p>\r\n\r\n<p>In my personal experience, companies often underestimate the connection between articulating their distinct culture and identifying candidates who will integrate seamlessly into that culture.</p>\r\n\r\n<p>Related: How to &#39;Hire Smarter&#39; Using Talent-Acquisition Technology</p>\r\n\r\n<p>But making that connection is crucial. It can mean the difference between hiring someone whose tenure is short, expensive and disruptive -- and finding a team member who&#39;ll contribute positively to the culture and take the organization to new heights.  </p>\r\n\r\n<p>Here are the top considerations for evaluating whether your next candidate will fit into your company’s culture.</p>\r\n\r\n<p>Understand the costs of culture and candidate misalignment.</p>\r\n\r\n<p>When newly hired executives leave after a relatively short period of time, the reason is rarely that they lacked the technical skills to deliver on the job. More often, it’s because they struggled to form relationships within the company or lacked cultural compatibility.</p>\r\n\r\n<p>And those new hires aren&#39;t the only ones losing out. When new executives leave suddenly, there can be fallout for the company: A sudden departure can disrupt morale. Turnover due to poor culture fit can also have a high cost: A 2017 study by the Center for Executive Succession at the University of South Carolina’s Darla Moore School of Business found that failure rates in the c-suite succession situations it examined ranged from 10 to 50 percent.</p>\r\n\r\n<p>The researchers further found that the direct cost of those failed internal promotions typically hovered between $2 million and $5 million. The cost of externally generated failures, meanwhile, was a staggering $20 million-plus.</p>\r\n\r\n<p>Then there was, and is, the internal cost in terms of staff: Even at companies where senior leaders want to improve company culture, employees are not always aware that this is a priority.</p>\r\n\r\n<p>While 71 percent of senior leaders, in a 2018 global survey by PwC, said that actively establishing company culture was an important priority, only 48 percent of employees believed their company culture had been carefully cultivated by senior leadership.</p>\r\n\r\n<p>This kind of divide can impact employee retention, driving away workers who sense a gap between what an organization claims to care about and how it lives those values.</p>\r\n\r\n<p>This scenario is particularly true for millennials (born between 1981 and 1996), who value company culture more than previous generations do. A 2016 study by Fidelity Investments found that millennials would take an average pay cut of $7,600 in return for “improved \"quality of work life\" (career development, purposeful work, work/life balance and company culture).”</p>\r\n\r\n<p>Next to enter the workforce is Generation Z (born roughly between 1996 and 2012). Companies also need to adapt their recruiting styles to attract this newest generation of workers -- on track to be the most diverse and best-educated generation to date.</p>\r\n\r\n<p>Given a choice, people would rather work for a company where daily actions align with company values, rather than a company that expresses platitudes without taking action. In this regard, millennials and Gen-Zers represent a critical executive pipeline that company leaders can’t afford to ignore.</p>\r\n\r\n<p>Good recruiters do a deep dive into a company&#39;s culture.</p>\r\n\r\n<p>Before compiling a shortlist of potential candidates, recruiters need to intimately understand the client’s values and goals. Without this insight, recruiters risk suggesting candidates who look great on paper but ultimately fail because of a cultural mismatch.</p>\r\n\r\n<p>True core values are elemental to the way a business is run and embedded into actions and messaging at all levels. Bland references to teamwork, integrity or authenticity might sound lofty but still fail to connect with a concrete business strategy in many organizations.</p>\r\n\r\n<p>Instead, company values should be specific and vivid enough to guide everything from hiring decisions to how a customer service representative answers the phone.</p>\r\n\r\n<p>Often, recruiters need to prompt companies to define their core values. If yours is the company doing the defining, don&#39;t underestimate the power of simply being able to cite examples of what daily life is like at your company for the role you&#39;re hiring for.</p>\r\n', 'Don\'t Let Technology Make Recruiting A Lost Art -- Here\'s Why.', '90f600761ceb72d5aac324517926cd4f.jpg', '', 1556040795, 1556040795, 1, 1, 0, 1, 0, 0),
(14, '', 'World’s Worst Passport', 1555970400, '<p>Baderkhan travels with an Iraq passport, which means he can travel to only 30 countries without a visa. In spite of this, Baderkhan travels frequently and hopes to change the perception of his country’s passport.</p>\r\n\r\n<p>Drew Binsky, this video’s creator, wrote that Iraq has the least powerful passport in the world. It ranked 176 out of 176 on the Global Passport Index.</p>\r\n\r\n<p>Baderkhan, from Iraqi Kurdistan, aspires to shatter the stereotype about the Iraq passport and to inspire people to travel and see the world.</p>\r\n\r\n<p>Blend In Abroad: International Travel Safety Tips</p>\r\n\r\n<p>Plane ticket booked! It&#39;s time to plan and pack for that dream trip abroad. Use these travel tips to stay safe during international trave</p>\r\n', 'What It’s Like To Travel With The ‘World’s Worst Passport’', '88a407a8ff1ef4b3b60c5c96c141f915.jpg', '', 1556040876, 1556040876, 1, 1, 0, 3, 0, 0),
(15, '', 'Elon Musk Slams', 1555970400, '<p>Elon Musk did not mince words about his thoughts on lidar sensor technology, which many companies rely on to give their self-driving cars an understanding of the road and what&#39;s on it.</p>\r\n\r\n<p>\"Lidar is a fool&#39;s errand,\" Musk said onstage during Tesla&#39;s \"Autonomy Day\" event, where it detailed its latest advancements in full self-driving-car technology.</p>\r\n\r\n<p>\"Anyone relying on lidar is doomed,\" he said. \"Expensive sensors that are unnecessary. It&#39;s like having a whole bunch of expensive appendices. Like one appendix is bad, well how about a whole bunch of them? That&#39;s ridiculous. You&#39;ll see.\"</p>\r\n\r\n<p>Read more: Tesla unveils the &#39;best chip in the world&#39; for self-driving cars at its autonomy day event</p>\r\n\r\n<p>Musk&#39;s comment takes direct aim at other autonomous-car companies, like Alphabet&#39;s self-driving arm, Waymo, which relies on honeycomb-looking lidar sensors.</p>\r\n\r\n<p>When asked about how companies will use lidar in the future, Musk said: \"They&#39;re all going to dump lidar. That&#39;s my prediction. Mark my words.\"</p>\r\n\r\n<p>Musk said he doesn&#39;t hate lidar as much as it sounded. He pointed to an example at SpaceX where he personally spearheaded a project to create lidar sensors to help navigate to the space station.</p>\r\n\r\n<p>In cars, though, Musk said the sensors simply do not make sense.</p>\r\n\r\n<p>\"It&#39;s expensive and unnecessary. You have expensive hardware that&#39;s worthless on the car,\" Musk said.</p>\r\n\r\n<p>The competition between Tesla and other autonomous-car companies like Waymo had already been heating up before Musk&#39;s jabs on Monday, given Tesla&#39;s recent hints about entering the robo-taxi market. Waymo has already begun rolling out its autonomous taxi services in Arizona, starting to charge customers last December.</p>\r\n\r\n<p>Musk, however, appeared to be incredibly confident on Monday about the self-driving hardware his team had created.</p>\r\n\r\n<p>\"It seems improbable. How could we at Tesla, who has never designed a chip before ... design the best chip in the world?\" Musk said. \"But that is what, objectively, has occurred. And not best by a small margin, but best by a huge margin.\"</p>\r\n\r\n<p>Later in the discussion, Musk made it clear what he considers true self-driving technology — and what he doesn&#39;t.</p>\r\n\r\n<p>\"If you have a geo-fenced area, you don&#39;t have real self-driving,\" he said.</p>\r\n', 'Elon Musk Slams Rivals\' Self-driving-car Tech, Says \'anyone Relying On Lidar Is Doomed\'', 'fbe47580bd4d68fae2c9b41ce30712fd.jpg', '', 1556130684, 1556040952, 1, 1, 0, 1, 0, 0),
(17, '', 'Align Technology Expected To Earn 84 Cents A Share', 1556056800, '<p>Align Technology, Inc. (ALGN - Get Report) is expected to report quarterly earnings of 84 cents a share on sales of $531.2 million after the market closes Wednesday, based on a FactSet survey of 14 analysts.</p>\r\n\r\n<p>In the same period a year ago the posted earnings of $1.17 a share on sales of $436.9 million.</p>\r\n\r\n<p>The stock has risen 30.9% since the company last reported earnings on Jan. 29. The company offered quarterly EPS guidance of 78 cents to 84 cents at the time of its last report.</p>\r\n\r\n<p>Quarterly estimates have fallen 1.8 cents a share in the past month.</p>\r\n\r\n<p>Align Technology is currently trading at a price-to-forward-earnings ratio of 52 based on the 12-month estimates of 16 analysts surveyed by FactSet.</p>\r\n\r\n<p>Jim Cramer and the AAP team are watching the health care equipment & supplies sector. To find out more about what stocks they like in the industry click here now to get more from Action Alerts PLUS.</p>\r\n\r\n<p>Introducing TheStreet Courses: Financial titans Jim Cramer and Robert Powell are bringing their market savvy and investing strategies to you. Learn how to create tax-efficient income, avoid top mistakes, reduce risk and more. With our courses, you will have the tools and knowledge needed to achieve your financial goals. Learn more about TheStreet Courses on investing and personal finance here. </p>\r\n', 'Align Technology Expected To Earn 84 Cents A Share', '7763f7aef4de2ccf3e2d2ce29bade88f.jpg', '', 1556131814, 1556131814, 1, 1, 0, 1, 0, 0),
(18, '', 'Technology Causing Americans To Sit More Than Ever', 1556056800, '<p>America&#39;s couch potatoes are becoming ever more deeply rooted, and computers are the reason why.</p>\r\n\r\n<p>The amount of time people spend sitting around has increased in recent years, driven largely by more leisure time spent with a computer, federal survey data shows.</p>\r\n\r\n<p>Total daily sitting time increased about an hour a day for teenagers and adults between 2007 and 2016, the researchers found. Teens spend an average 8.2 hours a day plopped down, and adults about 6.4 hours.</p>\r\n\r\n<p>Television isn&#39;t behind this increase, said senior researcher Yin Cao. She is an assistant professor with the division of public health sciences at Washington University in St. Louis.</p>\r\n\r\n<p>\"For TV watching time, we found that in most age groups the trend has been stable over the past 15 years,\" Cao said.</p>\r\n\r\n<p>Instead, people are sitting around more because they&#39;re busy clicking away on computers, the results show.</p>\r\n\r\n<p>\"When we looked into computer use outside school or work, we saw a dramatic increase for all age groups,\" Cao explained.</p>\r\n\r\n<p>Among children, 56 percent spent an hour or more on a computer outside school or work in 2016, compared to 43 percent in 2001, the study found. Among teens, the proportion rose to 57 percent from 53 percent, while half of adults logged an hour or more of leisure time on a computer in 2016, up from 29 percent.</p>\r\n\r\n<p>All of this sedentary behavior has serious implications for people&#39;s health.</p>\r\n\r\n<p>Excess time spent sitting has been associated with an increased risk of obesity, heart disease, cancer, diabetes and premature death, the study authors noted.</p>\r\n\r\n<p>And don&#39;t think a half-hour spent trundling on your gym&#39;s treadmill or elliptical is going to offset the time you&#39;ve invested in \"Game of Thrones\" and Facebook.</p>\r\n\r\n<p>Exercise eliminates the excess risk associated with prolonged sitting only among highly active people -- for example, those who get in 10 to 11 hours a week of brisk walking, the researchers said.</p>\r\n\r\n<p>\"The detrimental effects from prolonged sitting can be offset by physical activity, but you need to have very vigorous activity to get the benefits,\" Cao said. \"For most people in the U.S., we are not really at that level of vigorous physical activity.\"</p>\r\n\r\n<p>For the study, Cao&#39;s team analyzed responses from more than 51,000 participants in the U.S. National Health and Nutrition Examination Survey between 2001 and 2016. This series of surveys by the U.S. Centers for Disease Control and Prevention tracks health trends among Americans.</p>\r\n\r\n<p>The investigators found that most Americans spend at least two hours a day sitting and watching TV or videos.</p>\r\n\r\n<p>That includes 62 percent of 5- to 11-year-olds; 59 percent of 12- to 19-year-olds; 65 percent of 20- to 64-year-olds; and 84 percent of seniors. These percentages remained stable over the 15 years of the study.</p>\r\n', 'Technology Causing Americans To Sit More Than Ever', 'a31053df668d00313bb958492fbb8bce.jpg', '', 1556131851, 1556131851, 1, 1, 0, 2, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `content_right` text NOT NULL,
  `content_footer` text NOT NULL,
  `combined` tinyint(1) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `keywords_en` varchar(255) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `content_en` text NOT NULL,
  `date_creating` int(11) UNSIGNED NOT NULL,
  `date_last_editing` int(10) UNSIGNED NOT NULL,
  `user_creating` int(10) UNSIGNED NOT NULL,
  `user_last_editing` int(10) UNSIGNED NOT NULL,
  `basket` tinyint(1) NOT NULL,
  `basket_id` int(10) UNSIGNED NOT NULL,
  `modul` tinyint(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `parent_page_id` int(11) NOT NULL,
  `content_right_en` text NOT NULL,
  `content_footer_en` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `personal_instruments`
--

CREATE TABLE `personal_instruments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) NOT NULL,
  `instruments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `personal_instruments`
--

INSERT INTO `personal_instruments` (`id`, `user`, `instruments`) VALUES
(13, 1, '{\"1\":{\"name\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"active\":true,\"code\":\"pages\",\"access\":[1,2],\"toolbar\":true},\"2\":{\"name\":\"\\u041f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u0438\",\"active\":false,\"code\":\"users\",\"access\":[1,1],\"toolbar\":true},\"3\":{\"name\":\"\\u041d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0438\",\"active\":true,\"code\":\"news\",\"access\":[1,2,4],\"toolbar\":true,\"main-page-conf\":{\"groups\":[\"news\"],\"intranet\":false,\"show-quantity\":true,\"child\":[{\"name\":\"\\u041d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0438\",\"url\":\"\\/admin\\/events.php?type_v[]=1\"}]}},\"4\":{\"name\":\"\\u0422\\u0438\\u043f\\u044b \\u043d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0435\\u0439\",\"code\":\"type-news\",\"table\":\"type_v\",\"access\":[1,2],\"main-page-conf\":{\"groups\":[\"news\"],\"intranet\":false,\"settings\":true}}}');

-- --------------------------------------------------------

--
-- Структура таблицы `recent_movements`
--

CREATE TABLE `recent_movements` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `section` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `recent_movements`
--

INSERT INTO `recent_movements` (`id`, `user_id`, `section`) VALUES
(1, 1, '/main.php'),
(2, 0, '/main.php'),
(3, 6, '/main.php'),
(4, 7, '/main.php'),
(5, 4, '/main.php'),
(6, 5, '/main.php'),
(7, 3, '/main.php');

-- --------------------------------------------------------

--
-- Структура таблицы `type_groups`
--

CREATE TABLE `type_groups` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `type_v`
--

CREATE TABLE `type_v` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `hidden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_v`
--

INSERT INTO `type_v` (`id`, `name`, `user_creating`, `user_last_editing`, `date_creating`, `date_last_editing`, `hidden`) VALUES
(1, 'IT', 1, 1, 1555960243, 1555960243, 0),
(2, 'Technology', 1, 1, 1555960540, 1555960540, 0),
(3, 'Treveling', 1, 1, 1556040849, 1556040849, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `e_mail` varchar(255) NOT NULL,
  `rights` tinyint(2) NOT NULL,
  `post` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  `post_en` varchar(255) NOT NULL,
  `surname_en` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `patronymic_en` varchar(255) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `site_disabled_edit` tinyint(1) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `basket` tinyint(1) NOT NULL,
  `basket_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `e_mail`, `rights`, `post`, `surname`, `name`, `patronymic`, `post_en`, `surname_en`, `name_en`, `patronymic_en`, `notify`, `hash`, `site_disabled_edit`, `date_last_editing`, `date_creating`, `user_creating`, `user_last_editing`, `basket`, `basket_id`) VALUES
(1, 'admin', '1f32aa4c9a1d2ea010adcf2348166a04', '', 1, 'ВВВ', 'Admin', '', '', '', '', '', '', 1, 'e73a6e623dd2a9f3ad9057e06dd141af', 0, 1556139923, 0, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `type_v` int(11) NOT NULL,
  `anons` text NOT NULL,
  `images` text NOT NULL,
  `video` text NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `videos`
--

INSERT INTO `videos` (`id`, `title`, `type_v`, `anons`, `images`, `video`, `date_creating`, `user_creating`, `user_last_editing`, `date_last_editing`, `hidden`, `date`) VALUES
(2, 'RRR', 2, 'asdasdasdasdasd', '791f57bf48001d17e1f4116d3e841564.jpg', 'https://www.youtube.com/watch?v=g14_1SW8QQM', 1556223664, 1, 1, 1556223664, 0, 1556143200);

-- --------------------------------------------------------

--
-- Структура таблицы `view_fields`
--

CREATE TABLE `view_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `fields` text NOT NULL,
  `fields_width` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `view_fields`
--

INSERT INTO `view_fields` (`id`, `user_id`, `table_name`, `fields`, `fields_width`) VALUES
(1, 1, 'exhibits', '[\"name\",\"creation_time\",\"material_and_technique\",\"inv_number\",\"kp_number\",\"dimensions\",\"fund\",\"hide_short_description\"]', '[{\"field\":\"name\",\"width\":\"132.8\"},{\"field\":\"creation_time\",\"width\":\"132.8\"},{\"field\":\"material_and_technique\",\"width\":\"132.8\"},{\"field\":\"inv_number\",\"width\":\"132.8\"},{\"field\":\"kp_number\",\"width\":\"132.8\"},{\"field\":\"dimensions\",\"width\":\"134.4\"},{\"field\":\"fund\",\"width\":\"130.4\"},{\"field\":\"hide_short_description\",\"width\":\"132.8\"}]'),
(2, 1, 'groups', '[\"name\",\"url\",\"type_g\"]', '[{\"field\":\"name\",\"width\":\"392\"},{\"field\":\"url\",\"width\":\"378\"},{\"field\":\"type_g\",\"width\":\"385\"}]'),
(3, 1, 'x_is_docs_estimates_items_of_expenditure', '[\"hide\",\"name\"]', '[{\"field\":\"hide\",\"width\":null},{\"field\":\"name\",\"width\":null}]'),
(4, 1, 'news', '[\"title\",\"type_v\",\"date\",\"post\"]', '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_site_menu`
--
ALTER TABLE `main_site_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `personal_instruments`
--
ALTER TABLE `personal_instruments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `recent_movements`
--
ALTER TABLE `recent_movements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `type_groups`
--
ALTER TABLE `type_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `type_v`
--
ALTER TABLE `type_v`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `view_fields`
--
ALTER TABLE `view_fields`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `main_site_menu`
--
ALTER TABLE `main_site_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `personal_instruments`
--
ALTER TABLE `personal_instruments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `recent_movements`
--
ALTER TABLE `recent_movements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `type_groups`
--
ALTER TABLE `type_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `type_v`
--
ALTER TABLE `type_v`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `view_fields`
--
ALTER TABLE `view_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
