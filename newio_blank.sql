-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Апр 25 2019 г., 22:39
-- Версия сервера: 10.1.34-MariaDB
-- Версия PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `newio`
--

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `protected` tinyint(1) NOT NULL,
  `copyright` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `copyright_en` varchar(255) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `user_creating` int(10) UNSIGNED NOT NULL,
  `user_last_editing` int(10) UNSIGNED NOT NULL,
  `date_last_editing` int(10) UNSIGNED NOT NULL,
  `date_creating` int(10) UNSIGNED NOT NULL,
  `basket` tinyint(1) NOT NULL,
  `basket_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `main_site_menu`
--

CREATE TABLE `main_site_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `link_name` varchar(255) NOT NULL,
  `link_name_en` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `basket_id` int(11) NOT NULL,
  `basket` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `post` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` int(11) NOT NULL,
  `text` text NOT NULL,
  `anons` text NOT NULL,
  `images` text NOT NULL,
  `related_materials` text NOT NULL,
  `date_last_editing` int(11) UNSIGNED NOT NULL,
  `date_creating` int(11) UNSIGNED NOT NULL,
  `user_creating` int(11) UNSIGNED NOT NULL,
  `user_last_editing` int(11) UNSIGNED NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `type_v` int(11) NOT NULL,
  `basket` int(11) NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `content_right` text NOT NULL,
  `content_footer` text NOT NULL,
  `combined` tinyint(1) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `keywords_en` varchar(255) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `content_en` text NOT NULL,
  `date_creating` int(11) UNSIGNED NOT NULL,
  `date_last_editing` int(10) UNSIGNED NOT NULL,
  `user_creating` int(10) UNSIGNED NOT NULL,
  `user_last_editing` int(10) UNSIGNED NOT NULL,
  `basket` tinyint(1) NOT NULL,
  `basket_id` int(10) UNSIGNED NOT NULL,
  `modul` tinyint(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `parent_page_id` int(11) NOT NULL,
  `content_right_en` text NOT NULL,
  `content_footer_en` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `personal_instruments`
--

CREATE TABLE `personal_instruments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) NOT NULL,
  `instruments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `recent_movements`
--

CREATE TABLE `recent_movements` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `section` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `type_groups`
--

CREATE TABLE `type_groups` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `type_v`
--

CREATE TABLE `type_v` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `hidden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `e_mail` varchar(255) NOT NULL,
  `rights` tinyint(2) NOT NULL,
  `post` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  `post_en` varchar(255) NOT NULL,
  `surname_en` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `patronymic_en` varchar(255) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `site_disabled_edit` tinyint(1) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `basket` tinyint(1) NOT NULL,
  `basket_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `type_v` int(11) NOT NULL,
  `anons` text NOT NULL,
  `images` text NOT NULL,
  `video` text NOT NULL,
  `date_creating` int(11) NOT NULL,
  `user_creating` int(11) NOT NULL,
  `user_last_editing` int(11) NOT NULL,
  `date_last_editing` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `view_fields`
--

CREATE TABLE `view_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `fields` text NOT NULL,
  `fields_width` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_site_menu`
--
ALTER TABLE `main_site_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `personal_instruments`
--
ALTER TABLE `personal_instruments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `recent_movements`
--
ALTER TABLE `recent_movements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `type_groups`
--
ALTER TABLE `type_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `type_v`
--
ALTER TABLE `type_v`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `view_fields`
--
ALTER TABLE `view_fields`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `main_site_menu`
--
ALTER TABLE `main_site_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `personal_instruments`
--
ALTER TABLE `personal_instruments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `recent_movements`
--
ALTER TABLE `recent_movements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `type_groups`
--
ALTER TABLE `type_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `type_v`
--
ALTER TABLE `type_v`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `view_fields`
--
ALTER TABLE `view_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
